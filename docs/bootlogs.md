# Bootlogs

Note: At the time of writing (2021-06), we have enabled debug-logging for
the [ARM Trusted Firmware BL2 and BL3](/software/bootprocess/) by default
to assist in the troubleshooting of any RAM related issues. We may reduce
the verbosity in the future.

## Firmware v0.8.5 + OpenWrt 21.02 Snapshot (Kernel 5.10)

```
INFO:    RCW BOOT SRC is QSPI
INFO:    RCW BOOT SRC is QSPI
WARNING: Using I2C3 for DDR SPD
INFO:    platform clock 700000000
INFO:    DDR PLL 2100000000
INFO:    time base 11 ms
INFO:    Parse DIMM SPD(s)
INFO:    Controller 0
INFO:    DIMM 0
INFO:    addr 0x51
WARNING: Using I2C3 for DDR SPD
WARNING: tx_byte nodev 85
WARNING: Using I2C3 for DDR SPD
WARNING: Using I2C3 for DDR SPD
WARNING: tx_byte nodev 85
WARNING: Using I2C3 for DDR SPD
INFO:    checksum 0x6cc5df74
INFO:    n_ranks 2
INFO:    rank_density 0x200000000
INFO:    capacity 0x400000000
INFO:    die density 0x5
INFO:    primary_sdram_width 64
INFO:    ec_sdram_width 0
INFO:    device_width 8
INFO:    package_3ds 0
INFO:    rc 0x21 (Raw card B1)
INFO:    B1 SPD DQ mapping error fixed
INFO:    rdimm 0
INFO:    mirrored_dimm 1
INFO:    n_row_addr 16
INFO:    n_col_addr 10
INFO:    bank_addr_bits 0
INFO:    bank_group_bits 2
INFO:    edc_config 0
INFO:    burst_lengths_bitmask 0xc
INFO:    tckmin_x_ps 833
INFO:    tckmax_ps 1600
INFO:    caslat_x 0x3ffc00
INFO:    taa_ps 13750
INFO:    trcd_ps 13750
INFO:    trp_ps 13750
INFO:    tras_ps 32000
INFO:    trc_ps 45750
INFO:    trfc1_ps 350000
INFO:    trfc2_ps 260000
INFO:    trfc4_ps 160000
INFO:    tfaw_ps 21000
INFO:    trrds_ps 3300
INFO:    trrdl_ps 4901
INFO:    tccdl_ps 5000
INFO:    trfc_slr_ps 0
INFO:    twr_ps 15000
INFO:    refresh_rate_ps 7800000
INFO:    dq_mapping 0x2b
INFO:    dq_mapping 0x16
INFO:    dq_mapping 0x2c
INFO:    dq_mapping 0xb
INFO:    dq_mapping 0x2c
INFO:    dq_mapping 0x16
INFO:    dq_mapping 0x2c
INFO:    dq_mapping 0xc
INFO:    dq_mapping 0x0
INFO:    dq_mapping 0x0
INFO:    dq_mapping 0x2c
INFO:    dq_mapping 0xc
INFO:    dq_mapping 0x15
INFO:    dq_mapping 0x36
INFO:    dq_mapping 0x2b
INFO:    dq_mapping 0x15
INFO:    dq_mapping 0xb
INFO:    dq_mapping 0x36
INFO:    dq_mapping_ors 1
INFO:    DIMM 1
INFO:    done with controller 0
INFO:    cal cs
INFO:    cs_in_use = 3
INFO:    cs_on_dimm[0] = 3
NOTICE:  UDIMM CT16G4SFD824A.C16F
INFO:    Time after parsing SPD 568 ms
INFO:    Synthesize configurations
INFO:    cs 0
INFO:         odt_rd_cfg 0x0
INFO:         odt_wr_cfg 0x5
INFO:         odt_rtt_norm 0x3
INFO:         odt_rtt_wr 0x0
INFO:         auto_precharge 0
INFO:    cs 1
INFO:         odt_rd_cfg 0x0
INFO:         odt_wr_cfg 0x0
INFO:         odt_rtt_norm 0x0
INFO:         odt_rtt_wr 0x0
INFO:         auto_precharge 0
INFO:    cs 2
INFO:         odt_rd_cfg 0x0
INFO:         odt_wr_cfg 0x0
INFO:         odt_rtt_norm 0x0
INFO:         odt_rtt_wr 0x0
INFO:         auto_precharge 0
INFO:    cs 3
INFO:         odt_rd_cfg 0x0
INFO:         odt_wr_cfg 0x0
INFO:         odt_rtt_norm 0x0
INFO:         odt_rtt_wr 0x0
INFO:         auto_precharge 0
INFO:    ctlr_init_ecc 0
INFO:    x4_en 0
INFO:    ap_en 0
INFO:    ctlr_intlv 0
INFO:    ctlr_intlv_mode 0
INFO:    ba_intlv 0x40
INFO:    data_bus_used 0
INFO:    otf_burst_chop_en 1
INFO:    burst_length 0x6
INFO:    dbw_cap_shift 0
INFO:    Assign binding addresses
INFO:    ctlr_intlv 0
INFO:    rank density 0x200000000
INFO:    CS 0
INFO:        base_addr 0x0
INFO:        size 0x400000000
INFO:    CS 1
INFO:        base_addr 0x0
INFO:        size 0x400000000
INFO:    base 0x0
INFO:    Total mem by assignment is 0x400000000
INFO:    Calculate controller registers
INFO:    Skip CL mask for this speed 0x4000
INFO:    Skip caslat 0x4000
INFO:    cs_in_use = 0x3
INFO:    cs0
INFO:       _config = 0x80050422
INFO:    cs[0].bnds = 0x3ff
INFO:    cs_in_use = 0x3
INFO:    cs1
INFO:       _config = 0x80000422
INFO:    cs[1].bnds = 0x3ff
INFO:    sdram_cfg[0] = 0xc5004000
INFO:    sdram_cfg[1] = 0x401141
INFO:    sdram_cfg[2] = 0x0
INFO:    timing_cfg[0] = 0xd1770018
INFO:    timing_cfg[1] = 0xf2fc8245
INFO:    timing_cfg[2] = 0x594197
INFO:    timing_cfg[3] = 0x2161100
INFO:    timing_cfg[4] = 0x220002
INFO:    timing_cfg[5] = 0x5401400
INFO:    timing_cfg[6] = 0x0
INFO:    timing_cfg[7] = 0x26600000
INFO:    timing_cfg[8] = 0x5446a00
INFO:    timing_cfg[9] = 0x0
INFO:    dq_map[0] = 0xad6b0bb0
INFO:    dq_map[1] = 0x5ac32c30
INFO:    dq_map[2] = 0x576ad52c
INFO:    dq_map[3] = 0xd8000001
INFO:    sdram_mode[0] = 0x3010631
INFO:    sdram_mode[1] = 0x100200
INFO:    sdram_mode[9] = 0x8400000
INFO:    sdram_mode[8] = 0x500
INFO:    sdram_mode[2] = 0x10631
INFO:    sdram_mode[3] = 0x100200
INFO:    sdram_mode[10] = 0x400
INFO:    sdram_mode[11] = 0x8400000
INFO:    sdram_mode[4] = 0x10631
INFO:    sdram_mode[5] = 0x100200
INFO:    sdram_mode[12] = 0x400
INFO:    sdram_mode[13] = 0x8400000
INFO:    sdram_mode[6] = 0x10631
INFO:    sdram_mode[7] = 0x100200
INFO:    sdram_mode[14] = 0x400
INFO:    sdram_mode[15] = 0x8400000
INFO:    eor = 0x40000000
INFO:    interval = 0x1ffe07ff
INFO:    zq_cntl = 0x8a090705
INFO:    ddr_sr_cntr = 0x0
INFO:    clk_cntl = 0x2400000
INFO:    cdr[0] = 0x80080000
INFO:    cdr[1] = 0xc0
INFO:    wrlvl_cntl[0] = 0x8675f605
INFO:    wrlvl_cntl[1] = 0x8040a0c
INFO:    wrlvl_cntl[2] = 0x130f1014
INFO:    debug[28] = 0x7b
INFO:    Time before programming controller 836 ms
INFO:    Program controller registers
INFO:    Reading debug[9] as 0x2d003200
INFO:    Reading debug[10] as 0x29003500
INFO:    Reading debug[11] as 0x3a004800
INFO:    Reading debug[12] as 0x3f004300
INFO:    cpo_min 0x29
INFO:    cpo_max 0x48
INFO:    debug[28] 0x6a007b
WARNING: Warning: A009942 requires setting cpo_sample to 0x5f
INFO:    *0x1080000 = 0x3ff
INFO:    *0x1080008 = 0x3ff
INFO:    *0x1080080 = 0x80050422
INFO:    *0x1080084 = 0x80000422
INFO:    *0x1080100 = 0x2161100
INFO:    *0x1080104 = 0xd1770018
INFO:    *0x1080108 = 0xf2fc8245
INFO:    *0x108010c = 0x594197
INFO:    *0x1080110 = 0xc5004000
INFO:    *0x1080114 = 0x401141
INFO:    *0x1080118 = 0x3010631
INFO:    *0x108011c = 0x100200
INFO:    *0x1080120 = 0x1600086b
INFO:    *0x1080124 = 0x1ffe07ff
INFO:    *0x1080128 = 0xdeadbeef
INFO:    *0x1080130 = 0x2400000
INFO:    *0x1080160 = 0x220002
INFO:    *0x1080164 = 0x5401400
INFO:    *0x108016c = 0x26600000
INFO:    *0x1080170 = 0x8a090705
INFO:    *0x1080174 = 0xc675f605
INFO:    *0x1080190 = 0x8040a0c
INFO:    *0x1080194 = 0x130f1014
INFO:    *0x1080200 = 0x10631
INFO:    *0x1080204 = 0x100200
INFO:    *0x1080208 = 0x10631
INFO:    *0x108020c = 0x100200
INFO:    *0x1080210 = 0x10631
INFO:    *0x1080214 = 0x100200
INFO:    *0x1080220 = 0x500
INFO:    *0x1080224 = 0x8400000
INFO:    *0x1080228 = 0x400
INFO:    *0x108022c = 0x8400000
INFO:    *0x1080230 = 0x400
INFO:    *0x1080234 = 0x8400000
INFO:    *0x1080238 = 0x400
INFO:    *0x108023c = 0x8400000
INFO:    *0x1080250 = 0x5446a00
INFO:    *0x1080270 = 0xffff
INFO:    *0x1080280 = 0xdeedeeee
INFO:    *0x1080284 = 0xeededd21
INFO:    *0x1080288 = 0x21121111
INFO:    *0x108028c = 0x112122de
INFO:    *0x1080290 = 0x1
INFO:    *0x1080400 = 0xad6b0bb0
INFO:    *0x1080404 = 0x5ac32c30
INFO:    *0x1080408 = 0x576ad52c
INFO:    *0x108040c = 0xd8000001
INFO:    *0x1080b20 = 0x8080
INFO:    *0x1080b24 = 0x80000000
INFO:    *0x1080b28 = 0x80080000
INFO:    *0x1080b2c = 0xc0
INFO:    *0x1080bf8 = 0x20502
INFO:    *0x1080bfc = 0x100
INFO:    *0x1080c00 = 0x40000000
INFO:    *0x1080f04 = 0x2
INFO:    *0x1080f08 = 0x9
INFO:    *0x1080f0c = 0x14000c20
INFO:    *0x1080f24 = 0x2d003200
INFO:    *0x1080f28 = 0x29003500
INFO:    *0x1080f2c = 0x3a004800
INFO:    *0x1080f30 = 0x3f004300
INFO:    *0x1080f34 = 0x7000
INFO:    *0x1080f48 = 0x1
INFO:    *0x1080f4c = 0x94000000
INFO:    *0x1080f50 = 0xc001100
INFO:    *0x1080f54 = 0xa001500
INFO:    *0x1080f58 = 0x1a002700
INFO:    *0x1080f5c = 0x1f002200
INFO:    *0x1080f60 = 0x28000000
INFO:    *0x1080f64 = 0x9000
INFO:    *0x1080f68 = 0x20
INFO:    *0x1080f70 = 0x6a007b
INFO:    *0x1080f94 = 0x80000000
INFO:    *0x1080f9c = 0x2c002a00
INFO:    *0x1080fa0 = 0x2c002a00
INFO:    *0x1080fa4 = 0x2b002800
INFO:    *0x1080fa8 = 0x2b002900
INFO:    *0x1080fb0 = 0x3
INFO:    *0x1080fb4 = 0x1f1d1d1f
INFO:    *0x1080fb8 = 0x1d1e1e1d
INFO:    *0x1080fbc = 0x1e1c1b1e
INFO:    *0x1080fc0 = 0x1c1d1d1c
INFO:    *0x1080fc4 = 0x1e1e1b20
INFO:    *0x1080fc8 = 0x1e201f1f
INFO:    *0x1080fcc = 0x1e1c1c1e
INFO:    *0x1080fd0 = 0x1d1c1d1c
INFO:    *0x1080fd4 = 0x1e1e1c1e
INFO:    *0x1080fd8 = 0x1e1f1f1d
INFO:    *0x1080fdc = 0x1e1b1a1c
INFO:    *0x1080fe0 = 0x1c1c1c19
INFO:    *0x1080fe4 = 0x1e1e1c1e
INFO:    *0x1080fe8 = 0x1d1f1d1c
INFO:    *0x1080fec = 0x1e1d1b1d
INFO:    *0x1080ff0 = 0x1b1d1d1c
INFO:    *0x1080ff4 = 0x1f1f1f1f
INFO:    *0x1080ff8 = 0x1f1f1f1f
INFO:    *0x1080ffc = 0x1f000000

NOTICE:  16 GB DDR4, 64-bit, CL=15, ECC off, CS0+CS1
INFO:    Time used by DDR driver 1159 ms
NOTICE:  BL2: v1.5(debug):8bf4598
NOTICE:  BL2: Built : 04:11:44, Jun 22 2021
INFO:    Configuring TrustZone Controller
INFO:    Value of region base = ffe00000
INFO:    Value of region base = 1ffe00000
INFO:    Value of region base = fbe00000
INFO:    Value of region base = 8480000000
INFO:    BL2: Doing platform setup
INFO:    BL2: Loading image id 3
INFO:    Loading image id=3 at address 0xfbe00000
INFO:    Image id=3 loaded: 0xfbe00000 - 0xfbe0c16c
INFO:    BL2: Loading image id 5
INFO:    Loading image id=5 at address 0x82000000
INFO:    Image id=5 loaded: 0x82000000 - 0x8210b046
NOTICE:  BL2: Booting BL31
INFO:    Entry point address = 0xfbe00000
NOTICE:  BL31: v1.5(debug):8bf4598
NOTICE:  BL31: Built : 04:11:44, Jun 22 2021
NOTICE:  Welcome to LS1088 BL31 Phase
INFO:    GICv3 without legacy support detected. ARM GICV3 driver initialized in EL3
INFO:    BL31: Initializing runtime services
WARNING: BL31: cortex_a53: CPU workaround for 835769 was missing!
WARNING: BL31: cortex_a53: CPU workaround for 843419 was missing!
INFO:    BL31: cortex_a53: CPU workaround for 855873 was applied
INFO:    BL31: Preparing for EL3 exit to normal world
INFO:    Entry point address = 0x82000000
INFO:    SPSR = 0x3c9


U-Boot 2020.07-rc1-gb47b96d4 (Jun 22 2021 - 04:09:02 +0000)

SoC:  LS1088AE Rev1.0 (0x87030010)
Clock Configuration:
       CPU0(A53):1600 MHz  CPU1(A53):1600 MHz  CPU2(A53):1600 MHz
       CPU3(A53):1600 MHz  CPU4(A53):1600 MHz  CPU5(A53):1600 MHz
       CPU6(A53):1600 MHz  CPU7(A53):1600 MHz
       Bus:      700  MHz  DDR:      2100 MT/s
Reset Configuration Word (RCW):
       00000000: 4000541c 00000040 00000000 00000000
       00000010: 00000000 000a0000 00300000 00000000
       00000020: 010011a0 00002580 00000000 00000000
       00000030: 013fe60b 00000000 00002c03 00000000
       00000040: 00000000 00000000 00000000 00000000
       00000050: 00000000 00000000 00000000 00000000
       00000060: 00000000 00000000 00000089 000009e7
       00000070: 44110000 0d007755
DRAM:  15.9 GiB
DDR    15.9 GiB (DDR4, 64-bit, CL=15, ECC off)
       DDR Chip-Select Interleaving Mode: CS0+CS1
        15.4 GiB available for userspace

Using SERDES1 Protocol: 29 (0x1d)
Using SERDES2 Protocol: 20 (0x14)
PCIe0: pcie@3400000 Root Complex: no link
PCIe1: pcie@3500000 Root Complex: x1 gen2
PCIe2: pcie@3600000 Root Complex: x2 gen3
Retimer: (retimer on, resetting...) DS110DF410 found
OK
USB Hub:    No PCI card found in Key B
OK
Fan controller: OK
MMC:   FSL_SDHC: 0
Loading Environment from SPI Flash... SF: Detected en25s64 with page size 256 Bytes, erase size 64 KiB, total 8 MiB
OK
uC:    MACADDR: 00:0A:FA:24:2D:95
DPMAC7: 00:0A:FA:24:2D:95
DPMAC8: 00:0A:FA:24:2D:96
DPMAC9: 00:0A:FA:24:2D:97
DPMAC10: 00:0A:FA:24:2D:98
DPMAC3: 00:0A:FA:24:2D:99
DPMAC4: 00:0A:FA:24:2D:9A
DPMAC5: 00:0A:FA:24:2D:9B
DPMAC6: 00:0A:FA:24:2D:9C
DPMAC1: 00:0A:FA:24:2D:9D
DPMAC2: 00:0A:FA:24:2D:9E
In:    serial
Out:   serial
Err:   serial
Model: Traverse Ten64
Board: 1064-0201C, boot from QSPI
Net:   eth0: DPMAC1@xgmii, eth1: DPMAC2@xgmii, eth2: DPMAC3@qsgmii, eth3: DPMAC4@qsgmii, eth4: DPMAC5@qsgmii, eth5: DPMAC6@qsgmii, eth6: DPMAC7@qsgmii [PRIME], eth7: DPMAC8@qsgmii, eth8: DPMAC9@qsgmii, eth9: DPMAC10@qsgmii
SF: Detected en25s64 with page size 256 Bytes, erase size 64 KiB, total 8 MiB
device 0 offset 0x300000, size 0x200000
SF: 2097152 bytes @ 0x300000 Read: OK
device 0 offset 0x5c0000, size 0x40000
SF: 262144 bytes @ 0x5c0000 Read: OK
crc32+
fsl-mc: Booting Management Complex ... SUCCESS
fsl-mc: Management Complex booted (version: 10.20.4, boot status: 0x1)
Hit any key to stop autoboot:  0

  *** U-Boot Boot Menu ***

     Normal boot
     Built-in recovery
     Boot from NVMe
     Boot from USB
     Boot from SD
     Boot OpenWrt from NAND
     Boot from network
     Boot from SATA
     U-Boot console


  Press UP/DOWN to move, ENTER to select

Device 0: Vendor: 0x144d Rev: GXA7301Q Prod: S674NE0NC02476
            Type: Hard Disk
            Capacity: 244198.3 MB = 238.4 GB (500118192 x 512)
... is now current device
Scanning nvme 0:1...
** Unable to read file / **
Found EFI removable media binary efi/boot/bootaa64.efi
device 0 offset 0x580000, size 0x40000
SF: 262144 bytes @ 0x580000 Read: OK
device 0 offset 0x600000, size 0x40000
SF: 262144 bytes @ 0x600000 Read: OK
TEN64 ft_board_setup start, blob 0000000090000000
INFO:    RNG Desc SUCCESS with status 0
INFO:    result 87f423ffa1aeec08
TEN64 ft_board_setup end
Card did not respond to voltage select!
mmc_init: -95, time 19
Scanning disk esdhc@2140000.blk...
Disk esdhc@2140000.blk not ready
Scanning disk nvme#0.blk#0...
Found 3 disks
TEN64 ft_board_setup start, blob 0000000087f00000
TEN64 ft_board_setup already run, not doing anything
BootOrder not defined
EFI boot manager: Cannot load any image
675840 bytes read in 2 ms (322.3 MiB/s)
TEN64 ft_board_setup start, blob 0000000087ee7000
TEN64 ft_board_setup already run, not doing anything
Welcome to GRUB!

DPMAC7@qsgmii Waiting for PHY auto negotiation to complete......... TIMEOUT !
DPMAC7@qsgmii: Could not initialize
DPMAC8@qsgmii Waiting for PHY auto negotiation to complete......... TIMEOUT !
DPMAC8@qsgmii: Could not initialize
DPMAC9@qsgmii Waiting for PHY auto negotiation to complete......... TIMEOUT !
DPMAC9@qsgmii: Could not initialize
DPMAC10@qsgmii Waiting for PHY auto negotiation to complete......... TIMEOUT !
DPMAC10@qsgmii: Could not initialize
error: serial port `com0' isn't found.
error: terminal `serial' isn't found.
error: terminal `serial' isn't found.

                             GNU GRUB  version 2.04

 ┌────────────────────────────────────────────────────────────────────────────┐
 │*OpenWrt                                                                    │
 │ OpenWrt (failsafe)                                                         │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 └────────────────────────────────────────────────────────────────────────────┘

      Use the ▲ and ▼ keys to select which entry is highlighted.
      Press enter to boot the selected OS, `e' to edit the commands
      before booting or `c' for a command-line. ESC to return previous
      menu.
   The highlighted entry will be executed automatically in 0s.
  Booting `OpenWrt'

EFI stub: Booting Linux Kernel...
EFI stub: ERROR: Could not determine UEFI Secure Boot status.
EFI stub: Using DTB from configuration table
EFI stub: Exiting boot services and installing virtual address map...
fsl-mc: Deploying data path layout ... SUCCESS
[    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
[    0.000000] Linux version 5.10.43 (build@runner-isgbxbcc-project-15763379-concurrent-0) (aarch64-openwrt-linux-musl-gcc (OpenWrt GCC 8.4.0 r0-ab0210e) 8.4.0, GNU ld (GNU Binutils) 2.34) #0 SMP Thu Jun 17 07:42:32 2021
[    0.000000] Machine model: Traverse Ten64
[    0.000000] efi: EFI v2.80 by Das U-Boot
[    0.000000] efi: RTPROP=0x83dfff8040 SMBIOS=0xf69c6000 MEMRESERVE=0x837a6b4040
[    0.000000] earlycon: ns16550a0 at MMIO 0x00000000021c0500 (options '115200n8')
[    0.000000] printk: bootconsole [ns16550a0] enabled
[    0.000000] NUMA: No NUMA configuration found
[    0.000000] NUMA: Faking a node at [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000] NUMA: NODE_DATA [mem 0x83de0e3100-0x83de0e4fff]
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000080000000-0x00000000ffffffff]
[    0.000000]   DMA32    empty
[    0.000000]   Normal   [mem 0x0000000100000000-0x00000083dfffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000080000000-0x00000000f69c5fff]
[    0.000000]   node   0: [mem 0x00000000f69c6000-0x00000000f69c6fff]
[    0.000000]   node   0: [mem 0x00000000f69c7000-0x00000000fbcbffff]
[    0.000000]   node   0: [mem 0x00000000fbcc0000-0x00000000fbccffff]
[    0.000000]   node   0: [mem 0x00000000fbcd0000-0x00000000fbdfffff]
[    0.000000]   node   0: [mem 0x0000008080000000-0x00000083dff42fff]
[    0.000000]   node   0: [mem 0x00000083dff43000-0x00000083dff44fff]
[    0.000000]   node   0: [mem 0x00000083dff45000-0x00000083dffebfff]
[    0.000000]   node   0: [mem 0x00000083dffec000-0x00000083dfff0fff]
[    0.000000]   node   0: [mem 0x00000083dfff1000-0x00000083dfff3fff]
[    0.000000]   node   0: [mem 0x00000083dfff4000-0x00000083dfff4fff]
[    0.000000]   node   0: [mem 0x00000083dfff5000-0x00000083dfff7fff]
[    0.000000]   node   0: [mem 0x00000083dfff8000-0x00000083dfffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000]   DMA zone: 512 pages in unavailable ranges
[    0.000000] cma: Reserved 16 MiB at 0x00000000fac00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] psci: SMC Calling Convention v1.1
[    0.000000] percpu: Embedded 21 pages/cpu s45528 r8192 d32296 u86016
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: detected: GIC system register CPU interface
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 3982848
[    0.000000] Policy zone: Normal
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/vmlinux root=PARTUUID=28621705-ff1f-2e8b-8410-a3e504886302 rootwait earlycon noinitrd
[    0.000000] Dentry cache hash table entries: 2097152 (order: 12, 16777216 bytes, linear)
[    0.000000] Inode-cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] software IO TLB: mapped [mem 0x00000000f6c00000-0x00000000fac00000] (64MB)
[    0.000000] Memory: 15770924K/16185344K available (8192K kernel code, 968K rwdata, 2212K rodata, 896K init, 362K bss, 398036K reserved, 16384K cma-reserved)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=8, Nodes=1
[    0.000000] rcu: Hierarchical RCU implementation.
[    0.000000] rcu:     CONFIG_RCU_FANOUT set to non-default value of 32.
[    0.000000] rcu:     RCU restricting CPUs from NR_CPUS=256 to nr_cpu_ids=8.
[    0.000000]  Tracing variant of Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=8
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GICv3: GIC: Using split EOI/Deactivate mode
[    0.000000] GICv3: 256 SPIs implemented
[    0.000000] GICv3: 0 Extended SPIs implemented
[    0.000000] GICv3: Distributor has no Range Selector support
[    0.000000] GICv3: 16 PPIs implemented
[    0.000000] GICv3: CPU0: found redistributor 0 region 0:0x0000000006100000
[    0.000000] ITS [mem 0x06020000-0x0603ffff]
[    0.000000] ITS@0x0000000006020000: allocated 65536 Devices @8080080000 (flat, esz 8, psz 64K, shr 0)
[    0.000000] ITS: using cache flushing for cmd queue
[    0.000000] GICv3: using LPI property table @0x0000008080050000
[    0.000000] GIC: using cache flushing for LPI property table
[    0.000000] GICv3: CPU0: using allocated LPI pending table @0x0000008080060000
[    0.000000] random: get_random_bytes called from start_kernel+0x350/0x54c with crng_init=0
[    0.000000] arch_timer: cp15 timer(s) running at 25.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x5c409fb33, max_idle_ns: 440795203156 ns
[    0.000002] sched_clock: 56 bits at 25MHz, resolution 39ns, wraps every 4398046511103ns
[    0.008251] Calibrating delay loop (skipped), value calculated using timer frequency.. 50.00 BogoMIPS (lpj=100000)
[    0.018668] pid_max: default: 32768 minimum: 301
[    0.023409] Mount-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.031062] Mountpoint-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.039915] rcu: Hierarchical SRCU implementation.
[    0.044816] dyndbg: Ignore empty _ddebug table in a CONFIG_DYNAMIC_DEBUG_CORE build
[    0.052539] Platform MSI: gic-its@6020000 domain created
[    0.057924] PCI/MSI: /interrupt-controller@6000000/gic-its@6020000 domain created
[    0.065491] fsl-mc MSI: gic-its@6020000 domain created
[    0.070687] Remapping and enabling EFI services.
[    0.075511] smp: Bringing up secondary CPUs ...
[    0.080296] Detected VIPT I-cache on CPU1
[    0.080315] GICv3: CPU1: found redistributor 1 region 0:0x0000000006120000
[    0.080337] GICv3: CPU1: using allocated LPI pending table @0x0000008080070000
[    0.080360] CPU1: Booted secondary processor 0x0000000001 [0x410fd034]
[    0.080614] Detected VIPT I-cache on CPU2
[    0.080627] GICv3: CPU2: found redistributor 2 region 0:0x0000000006140000
[    0.080648] GICv3: CPU2: using allocated LPI pending table @0x0000008080100000
[    0.080666] CPU2: Booted secondary processor 0x0000000002 [0x410fd034]
[    0.080914] Detected VIPT I-cache on CPU3
[    0.080926] GICv3: CPU3: found redistributor 3 region 0:0x0000000006160000
[    0.080946] GICv3: CPU3: using allocated LPI pending table @0x0000008080110000
[    0.080964] CPU3: Booted secondary processor 0x0000000003 [0x410fd034]
[    0.081217] Detected VIPT I-cache on CPU4
[    0.081237] GICv3: CPU4: found redistributor 100 region 0:0x0000000006180000
[    0.081267] GICv3: CPU4: using allocated LPI pending table @0x0000008080120000
[    0.081296] CPU4: Booted secondary processor 0x0000000100 [0x410fd034]
[    0.081580] Detected VIPT I-cache on CPU5
[    0.081595] GICv3: CPU5: found redistributor 101 region 0:0x00000000061a0000
[    0.081616] GICv3: CPU5: using allocated LPI pending table @0x0000008080130000
[    0.081634] CPU5: Booted secondary processor 0x0000000101 [0x410fd034]
[    0.081896] Detected VIPT I-cache on CPU6
[    0.081911] GICv3: CPU6: found redistributor 102 region 0:0x00000000061c0000
[    0.081930] GICv3: CPU6: using allocated LPI pending table @0x0000008080140000
[    0.081949] CPU6: Booted secondary processor 0x0000000102 [0x410fd034]
[    0.082206] Detected VIPT I-cache on CPU7
[    0.082221] GICv3: CPU7: found redistributor 103 region 0:0x00000000061e0000
[    0.082244] GICv3: CPU7: using allocated LPI pending table @0x0000008080150000
[    0.082263] CPU7: Booted secondary processor 0x0000000103 [0x410fd034]
[    0.082312] smp: Brought up 1 node, 8 CPUs
[    0.260455] SMP: Total of 8 processors activated.
[    0.265184] CPU features: detected: 32-bit EL0 Support
[    0.270349] CPU features: detected: CRC32 instructions
[    0.275514] CPU features: detected: 32-bit EL1 Support
[    0.280716] CPU features: emulated: Privileged Access Never (PAN) using TTBR0_EL1 switching
[    0.289119] CPU: All CPU(s) started at EL2
[    0.293253] alternatives: patching kernel code
[    0.300647] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.310457] futex hash table entries: 2048 (order: 5, 131072 bytes, linear)
[    0.317916] pinctrl core: initialized pinctrl subsystem
[    0.323343] SMBIOS 3.0 present.
[    0.326502] DMI: traverse ten64/ten64, BIOS 2020.07-rc1-gb47b96d4 06/22/2021
[    0.333707] NET: Registered protocol family 16
[    0.339086] DMA: preallocated 2048 KiB GFP_KERNEL pool for atomic allocations
[    0.346706] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA pool for atomic allocations
[    0.355201] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.363759] thermal_sys: Registered thermal governor 'step_wise'
[    0.364022] cpuidle: using governor ladder
[    0.374253] ASID allocator initialised with 65536 entries
[    0.379744] Serial: AMBA PL011 UART driver
[    0.390945] Machine: Traverse Ten64
[    0.394459] SoC family: QorIQ LS1088A
[    0.398135] SoC ID: svr:0x87030010, Revision: 1.0
[    0.404261] fsl_mc_bus 80c000000.fsl-mc: MC firmware version: 10.20.4
[    0.413297] fsl_mc_dprc dprc.1: DMA mask not set
[    0.422728] cryptd: max_cpu_qlen set to 1000
[    0.427320] ACPI: Interpreter disabled.
[    0.431222] iommu: Default domain type: Passthrough
[    0.436350] SCSI subsystem initialized
[    0.440334] usbcore: registered new interface driver usbfs
[    0.445880] usbcore: registered new interface driver hub
[    0.451242] usbcore: registered new device driver usb
[    0.456482] imx-i2c 2000000.i2c: can't get pinctrl, bus recovery not supported
[    0.464688] i2c i2c-0: IMX I2C adapter registered
[    0.469520] imx-i2c 2020000.i2c: can't get pinctrl, bus recovery not supported
[    0.476872] i2c i2c-1: IMX I2C adapter registered
[    0.481676] imx-i2c 2030000.i2c: can't get pinctrl, bus recovery not supported
[    0.489041] i2c i2c-2: IMX I2C adapter registered
[    0.494421] clocksource: Switched to clocksource arch_sys_counter
[    0.500692] pnp: PnP ACPI: disabled
[    0.504506] NET: Registered protocol family 2
[    0.509206] IP idents hash table entries: 262144 (order: 9, 2097152 bytes, linear)
[    0.520682] tcp_listen_portaddr_hash hash table entries: 8192 (order: 5, 131072 bytes, linear)
[    0.529460] TCP established hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    0.538194] TCP bind hash table entries: 65536 (order: 8, 1048576 bytes, linear)
[    0.546211] TCP: Hash tables configured (established 131072 bind 65536)
[    0.552932] UDP hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    0.559963] UDP-Lite hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    0.567524] NET: Registered protocol family 1
[    0.571918] PCI: CLS 0 bytes, default 64
[    0.576226] kvm [1]: IPA Size Limit: 40 bits
[    0.580842] kvm [1]: vgic-v2@c0e0000
[    0.584434] kvm [1]: GIC system register CPU interface enabled
[    0.590393] kvm [1]: vgic interrupt IRQ9
[    0.594474] kvm [1]: Hyp mode initialized successfully
[    0.601322] workingset: timestamp_bits=44 max_order=22 bucket_order=0
[    0.610319] squashfs: version 4.0 (2009/01/31) Phillip Lougher
[    0.616188] jffs2: version 2.2 (NAND) (SUMMARY) (LZMA) (RTIME) (CMODE_PRIORITY) (c) 2001-2006 Red Hat, Inc.
[    0.626994] layerscape-pcie 3400000.pcie: host bridge /soc/pcie@3400000 ranges:
[    0.634364] layerscape-pcie 3400000.pcie:       IO 0x2000010000..0x200001ffff -> 0x0000000000
[    0.642953] layerscape-pcie 3400000.pcie:      MEM 0x2040000000..0x207fffffff -> 0x0040000000
[    0.651625] layerscape-pcie 3400000.pcie: PCI host bridge to bus 0000:00
[    0.658371] pci_bus 0000:00: root bus resource [bus 00-ff]
[    0.663889] pci_bus 0000:00: root bus resource [io  0x0000-0xffff]
[    0.670107] pci_bus 0000:00: root bus resource [mem 0x2040000000-0x207fffffff] (bus address [0x40000000-0x7fffffff])
[    0.680713] pci 0000:00:00.0: [1957:80c0] type 01 class 0x060400
[    0.686800] pci 0000:00:00.0: supports D1 D2
[    0.691092] pci 0000:00:00.0: PME# supported from D0 D1 D2 D3hot
[    0.698830] pci 0000:00:00.0: PCI bridge to [bus 01-ff]
[    0.704098] pci 0000:00:00.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  256
[    0.712728] layerscape-pcie 3500000.pcie: host bridge /soc/pcie@3500000 ranges:
[    0.720094] layerscape-pcie 3500000.pcie:       IO 0x2800010000..0x280001ffff -> 0x0000000000
[    0.728679] layerscape-pcie 3500000.pcie:      MEM 0x2840000000..0x287fffffff -> 0x0040000000
[    0.737334] layerscape-pcie 3500000.pcie: PCI host bridge to bus 0001:00
[    0.744080] pci_bus 0001:00: root bus resource [bus 00-ff]
[    0.749600] pci_bus 0001:00: root bus resource [io  0x10000-0x1ffff] (bus address [0x0000-0xffff])
[    0.758617] pci_bus 0001:00: root bus resource [mem 0x2840000000-0x287fffffff] (bus address [0x40000000-0x7fffffff])
[    0.769217] pci 0001:00:00.0: [1957:80c0] type 01 class 0x060400
[    0.775292] pci 0001:00:00.0: supports D1 D2
[    0.779587] pci 0001:00:00.0: PME# supported from D0 D1 D2 D3hot
[    0.786587] pci 0001:01:00.0: [12d8:b304] type 01 class 0x060400
[    0.792999] pci 0001:01:00.0: supports D1 D2
[    0.797293] pci 0001:01:00.0: PME# supported from D0 D1 D2 D3hot D3cold
[    0.804037] pci 0001:01:00.0: 4.000 Gb/s available PCIe bandwidth, limited by 5.0 GT/s PCIe x1 link at 0001:00:00.0 (capable of 8.000 Gb/s with 5.0 GT/s PCIe x2 link)
[    0.831425] pci 0001:02:01.0: [12d8:b304] type 01 class 0x060400
[    0.837847] pci 0001:02:01.0: supports D1 D2
[    0.842141] pci 0001:02:01.0: PME# supported from D0 D1 D2 D3hot D3cold
[    0.849044] pci 0001:02:02.0: [12d8:b304] type 01 class 0x060400
[    0.855463] pci 0001:02:02.0: supports D1 D2
[    0.859757] pci 0001:02:02.0: PME# supported from D0 D1 D2 D3hot D3cold
[    0.869194] pci 0001:02:01.0: PCI bridge to [bus 03]
[    0.874218] pci 0001:02:02.0: PCI bridge to [bus 04]
[    0.879240] pci 0001:01:00.0: PCI bridge to [bus 02-04]
[    0.884523] pci 0001:00:00.0: PCI bridge to [bus 01-ff]
[    0.889784] pci 0001:00:00.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  256
[    0.898310] pci 0001:01:00.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  128
[    0.906836] pci 0001:02:01.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  128
[    0.915362] pci 0001:02:02.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  128
[    0.924044] layerscape-pcie 3600000.pcie: host bridge /soc/pcie@3600000 ranges:
[    0.931410] layerscape-pcie 3600000.pcie:       IO 0x3000010000..0x300001ffff -> 0x0000000000
[    0.939996] layerscape-pcie 3600000.pcie:      MEM 0x3040000000..0x307fffffff -> 0x0040000000
[    0.948648] layerscape-pcie 3600000.pcie: PCI host bridge to bus 0002:00
[    0.955393] pci_bus 0002:00: root bus resource [bus 00-ff]
[    0.960911] pci_bus 0002:00: root bus resource [io  0x20000-0x2ffff] (bus address [0x0000-0xffff])
[    0.969930] pci_bus 0002:00: root bus resource [mem 0x3040000000-0x307fffffff] (bus address [0x40000000-0x7fffffff])
[    0.980527] pci 0002:00:00.0: [1957:80c0] type 01 class 0x060400
[    0.986600] pci 0002:00:00.0: supports D1 D2
[    0.990893] pci 0002:00:00.0: PME# supported from D0 D1 D2 D3hot
[    0.997878] pci 0002:01:00.0: [144d:a80a] type 00 class 0x010802
[    1.003976] pci 0002:01:00.0: reg 0x10: [mem 0x3040000000-0x3040003fff 64bit]
[    1.011577] pci 0002:01:00.0: 15.752 Gb/s available PCIe bandwidth, limited by 8.0 GT/s PCIe x2 link at 0002:00:00.0 (capable of 63.012 Gb/s with 16.0 GT/s PCIe x4 link)
[    1.027665] pci 0002:00:00.0: BAR 8: assigned [mem 0x3040000000-0x30400fffff]
[    1.034847] pci 0002:01:00.0: BAR 0: assigned [mem 0x3040000000-0x3040003fff 64bit]
[    1.042574] pci 0002:00:00.0: PCI bridge to [bus 01-ff]
[    1.047829] pci 0002:00:00.0:   bridge window [mem 0x3040000000-0x30400fffff]
[    1.055014] pci 0002:00:00.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  256
[    1.063538] pci 0002:01:00.0: Max Payload Size set to  256/ 256 (was  128), Max Read Rq  256
[    1.073028] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    1.079763] printk: console [ttyS0] disabled
[    1.084091] 21c0500.serial: ttyS0 at MMIO 0x21c0500 (irq = 16, base_baud = 21875000) is a 16550A
[    1.092948] printk: console [ttyS0] enabled
[    1.092948] printk: console [ttyS0] enabled
[    1.101336] printk: bootconsole [ns16550a0] disabled
[    1.101336] printk: bootconsole [ns16550a0] disabled
[    1.111461] 21c0600.serial: ttyS1 at MMIO 0x21c0600 (irq = 16, base_baud = 21875000) is a 16550A
[    1.120801] arm-smmu 5000000.iommu: probing hardware configuration...
[    1.127241] arm-smmu 5000000.iommu: SMMUv2 with:
[    1.131856] arm-smmu 5000000.iommu:  stage 1 translation
[    1.137164] arm-smmu 5000000.iommu:  stage 2 translation
[    1.142473] arm-smmu 5000000.iommu:  nested translation
[    1.147695] arm-smmu 5000000.iommu:  stream matching with 128 register groups
[    1.154831] arm-smmu 5000000.iommu:  64 context banks (0 stage-2 only)
[    1.161359] arm-smmu 5000000.iommu:  Supported page sizes: 0x61311000
[    1.167798] arm-smmu 5000000.iommu:  Stage-1: 48-bit VA -> 48-bit IPA
[    1.174236] arm-smmu 5000000.iommu:  Stage-2: 48-bit IPA -> 48-bit PA
[    1.181221] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    1.187802] nvme 0002:01:00.0: Adding to iommu group 0
[    1.193065] nvme nvme0: pci function 0002:01:00.0
[    1.198595] libphy: Fixed MDIO Bus: probed
[    1.203352] libphy: Freescale XGMAC MDIO Bus: probed
[    1.210543] libphy: Freescale XGMAC MDIO Bus: probed
[    1.210678] nvme nvme0: Shutdown timeout set to 10 seconds
[    1.215892] libphy: Freescale XGMAC MDIO Bus: probed
[    1.224624] nvme nvme0: 8/0/0 default/read/poll queues
[    1.226344] libphy: Freescale XGMAC MDIO Bus: probed
[    1.233447] Alternate GPT is invalid, using primary GPT.
[    1.237015] libphy: Freescale XGMAC MDIO Bus: probed
[    1.241424]  nvme0n1: p1 p2 p128
[    1.248046] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[    1.255084] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 1
[    1.262826] xhci-hcd xhci-hcd.0.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[    1.272245] xhci-hcd xhci-hcd.0.auto: irq 22, io mem 0x03100000
[    1.278498] hub 1-0:1.0: USB hub found
[    1.282254] hub 1-0:1.0: 1 port detected
[    1.286331] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[    1.291821] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 2
[    1.299479] xhci-hcd xhci-hcd.0.auto: Host supports USB 3.0 SuperSpeed
[    1.306035] usb usb2: We don't know the algorithms for LPM for this host, disabling LPM.
[    1.314338] hub 2-0:1.0: USB hub found
[    1.318097] hub 2-0:1.0: 1 port detected
[    1.322220] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[    1.327715] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 3
[    1.335444] xhci-hcd xhci-hcd.1.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[    1.344858] xhci-hcd xhci-hcd.1.auto: irq 23, io mem 0x03110000
[    1.351040] hub 3-0:1.0: USB hub found
[    1.354805] hub 3-0:1.0: 1 port detected
[    1.358865] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[    1.364355] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 4
[    1.372014] xhci-hcd xhci-hcd.1.auto: Host supports USB 3.0 SuperSpeed
[    1.378565] usb usb4: We don't know the algorithms for LPM for this host, disabling LPM.
[    1.386863] hub 4-0:1.0: USB hub found
[    1.390622] hub 4-0:1.0: 1 port detected
[    1.394775] usbcore: registered new interface driver usb-storage
[    1.400830] i2c /dev entries driver
[    1.410641] sp805-wdt c000000.wdt: registration successful
[    1.416207] sp805-wdt c010000.wdt: registration successful
[    1.421777] sp805-wdt c020000.wdt: registration successful
[    1.427341] sp805-wdt c030000.wdt: registration successful
[    1.432907] sp805-wdt c100000.wdt: registration successful
[    1.438472] sp805-wdt c110000.wdt: registration successful
[    1.444035] sp805-wdt c120000.wdt: registration successful
[    1.449602] sp805-wdt c130000.wdt: registration successful
[    1.455219] sdhci: Secure Digital Host Controller Interface driver
[    1.461396] sdhci: Copyright(c) Pierre Ossman
[    1.465748] sdhci-pltfm: SDHCI platform and OF driver helper
[    1.471879] NET: Registered protocol family 10
[    1.476709] Segment Routing with IPv6
[    1.480400] NET: Registered protocol family 17
[    1.484856] bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
[    1.494428] mmc0: SDHCI controller on 2140000.esdhc [2140000.esdhc] using ADMA 64-bit
[    1.497811] 8021q: 802.1Q VLAN Support v1.8
[    1.509926] registered taskstats version 1
[    1.515667] fsl_mc_dprc dprc.1: Adding to iommu group 1
[    1.601980] fsl_mc_allocator dpbp.9: Adding to iommu group 1
[    1.607745] fsl_mc_allocator dpbp.8: Adding to iommu group 1
[    1.613504] fsl_mc_allocator dpbp.7: Adding to iommu group 1
[    1.619258] fsl_mc_allocator dpbp.6: Adding to iommu group 1
[    1.625016] fsl_mc_allocator dpbp.5: Adding to iommu group 1
[    1.626424] usb 3-1: new high-speed USB device number 2 using xhci-hcd
[    1.630770] fsl_mc_allocator dpbp.4: Adding to iommu group 1
[    1.642960] fsl_mc_allocator dpbp.3: Adding to iommu group 1
[    1.648716] fsl_mc_allocator dpbp.2: Adding to iommu group 1
[    1.654476] fsl_mc_allocator dpbp.1: Adding to iommu group 1
[    1.660241] fsl_mc_allocator dpbp.0: Adding to iommu group 1
[    1.667007] fsl_mc_allocator dpmcp.34: Adding to iommu group 1
[    1.673441] fsl_mc_allocator dpmcp.33: Adding to iommu group 1
[    1.679872] fsl_mc_allocator dpmcp.32: Adding to iommu group 1
[    1.686304] fsl_mc_allocator dpmcp.31: Adding to iommu group 1
[    1.692734] fsl_mc_allocator dpmcp.30: Adding to iommu group 1
[    1.699169] fsl_mc_allocator dpmcp.29: Adding to iommu group 1
[    1.705604] fsl_mc_allocator dpmcp.28: Adding to iommu group 1
[    1.712034] fsl_mc_allocator dpmcp.27: Adding to iommu group 1
[    1.718471] fsl_mc_allocator dpmcp.26: Adding to iommu group 1
[    1.724903] fsl_mc_allocator dpmcp.25: Adding to iommu group 1
[    1.731337] fsl_mc_allocator dpmcp.24: Adding to iommu group 1
[    1.737781] fsl_mc_allocator dpmcp.23: Adding to iommu group 1
[    1.744219] fsl_mc_allocator dpmcp.22: Adding to iommu group 1
[    1.750657] fsl_mc_allocator dpmcp.21: Adding to iommu group 1
[    1.757088] fsl_mc_allocator dpmcp.20: Adding to iommu group 1
[    1.763522] fsl_mc_allocator dpmcp.19: Adding to iommu group 1
[    1.769957] fsl_mc_allocator dpmcp.18: Adding to iommu group 1
[    1.776391] fsl_mc_allocator dpmcp.17: Adding to iommu group 1
[    1.782828] fsl_mc_allocator dpmcp.16: Adding to iommu group 1
[    1.789265] fsl_mc_allocator dpmcp.15: Adding to iommu group 1
[    1.795702] fsl_mc_allocator dpmcp.14: Adding to iommu group 1
[    1.802137] fsl_mc_allocator dpmcp.13: Adding to iommu group 1
[    1.808577] fsl_mc_allocator dpmcp.12: Adding to iommu group 1
[    1.815015] fsl_mc_allocator dpmcp.11: Adding to iommu group 1
[    1.821452] fsl_mc_allocator dpmcp.10: Adding to iommu group 1
[    1.826640] hub 3-1:1.0: USB hub found
[    1.827886] fsl_mc_allocator dpmcp.9: Adding to iommu group 1
[    1.831057] hub 3-1:1.0: 4 ports detected
[    1.837371] fsl_mc_allocator dpmcp.8: Adding to iommu group 1
[    1.847117] fsl_mc_allocator dpmcp.7: Adding to iommu group 1
[    1.853465] fsl_mc_allocator dpmcp.6: Adding to iommu group 1
[    1.859814] fsl_mc_allocator dpmcp.5: Adding to iommu group 1
[    1.866162] fsl_mc_allocator dpmcp.4: Adding to iommu group 1
[    1.872514] fsl_mc_allocator dpmcp.3: Adding to iommu group 1
[    1.878867] fsl_mc_allocator dpmcp.2: Adding to iommu group 1
[    1.885219] fsl_mc_allocator dpmcp.1: Adding to iommu group 1
[    1.891086] fsl_mc_allocator dpcon.72: Adding to iommu group 1
[    1.897027] fsl_mc_allocator dpcon.71: Adding to iommu group 1
[    1.902969] fsl_mc_allocator dpcon.70: Adding to iommu group 1
[    1.908904] fsl_mc_allocator dpcon.69: Adding to iommu group 1
[    1.914854] fsl_mc_allocator dpcon.68: Adding to iommu group 1
[    1.918449] usb 4-1: new SuperSpeed Gen 1 USB device number 2 using xhci-hcd
[    1.920790] fsl_mc_allocator dpcon.67: Adding to iommu group 1
[    1.933661] fsl_mc_allocator dpcon.66: Adding to iommu group 1
[    1.939599] fsl_mc_allocator dpcon.65: Adding to iommu group 1
[    1.945539] fsl_mc_allocator dpcon.64: Adding to iommu group 1
[    1.951482] fsl_mc_allocator dpcon.63: Adding to iommu group 1
[    1.957421] fsl_mc_allocator dpcon.62: Adding to iommu group 1
[    1.963358] fsl_mc_allocator dpcon.61: Adding to iommu group 1
[    1.969295] fsl_mc_allocator dpcon.60: Adding to iommu group 1
[    1.970635] hub 4-1:1.0: USB hub found
[    1.975233] fsl_mc_allocator dpcon.59: Adding to iommu group 1
[    1.978898] hub 4-1:1.0: 4 ports detected
[    1.984805] fsl_mc_allocator dpcon.58: Adding to iommu group 1
[    1.994641] fsl_mc_allocator dpcon.57: Adding to iommu group 1
[    2.000582] fsl_mc_allocator dpcon.56: Adding to iommu group 1
[    2.006524] fsl_mc_allocator dpcon.55: Adding to iommu group 1
[    2.012464] fsl_mc_allocator dpcon.54: Adding to iommu group 1
[    2.018403] fsl_mc_allocator dpcon.53: Adding to iommu group 1
[    2.024348] fsl_mc_allocator dpcon.52: Adding to iommu group 1
[    2.030286] fsl_mc_allocator dpcon.51: Adding to iommu group 1
[    2.036226] fsl_mc_allocator dpcon.50: Adding to iommu group 1
[    2.042166] fsl_mc_allocator dpcon.49: Adding to iommu group 1
[    2.048106] fsl_mc_allocator dpcon.48: Adding to iommu group 1
[    2.054049] fsl_mc_allocator dpcon.47: Adding to iommu group 1
[    2.059988] fsl_mc_allocator dpcon.46: Adding to iommu group 1
[    2.065929] fsl_mc_allocator dpcon.45: Adding to iommu group 1
[    2.071868] fsl_mc_allocator dpcon.44: Adding to iommu group 1
[    2.077810] fsl_mc_allocator dpcon.43: Adding to iommu group 1
[    2.083751] fsl_mc_allocator dpcon.42: Adding to iommu group 1
[    2.089695] fsl_mc_allocator dpcon.41: Adding to iommu group 1
[    2.095634] fsl_mc_allocator dpcon.40: Adding to iommu group 1
[    2.101576] fsl_mc_allocator dpcon.39: Adding to iommu group 1
[    2.107517] fsl_mc_allocator dpcon.38: Adding to iommu group 1
[    2.113467] fsl_mc_allocator dpcon.37: Adding to iommu group 1
[    2.119411] fsl_mc_allocator dpcon.36: Adding to iommu group 1
[    2.125352] fsl_mc_allocator dpcon.35: Adding to iommu group 1
[    2.131297] fsl_mc_allocator dpcon.34: Adding to iommu group 1
[    2.137238] fsl_mc_allocator dpcon.33: Adding to iommu group 1
[    2.143182] fsl_mc_allocator dpcon.32: Adding to iommu group 1
[    2.149127] fsl_mc_allocator dpcon.31: Adding to iommu group 1
[    2.155075] fsl_mc_allocator dpcon.30: Adding to iommu group 1
[    2.161015] fsl_mc_allocator dpcon.29: Adding to iommu group 1
[    2.166964] fsl_mc_allocator dpcon.28: Adding to iommu group 1
[    2.172908] fsl_mc_allocator dpcon.27: Adding to iommu group 1
[    2.178858] fsl_mc_allocator dpcon.26: Adding to iommu group 1
[    2.184804] fsl_mc_allocator dpcon.25: Adding to iommu group 1
[    2.190747] fsl_mc_allocator dpcon.24: Adding to iommu group 1
[    2.196694] fsl_mc_allocator dpcon.23: Adding to iommu group 1
[    2.202638] fsl_mc_allocator dpcon.22: Adding to iommu group 1
[    2.208583] fsl_mc_allocator dpcon.21: Adding to iommu group 1
[    2.214533] fsl_mc_allocator dpcon.20: Adding to iommu group 1
[    2.220479] fsl_mc_allocator dpcon.19: Adding to iommu group 1
[    2.226431] fsl_mc_allocator dpcon.18: Adding to iommu group 1
[    2.232374] fsl_mc_allocator dpcon.17: Adding to iommu group 1
[    2.238320] fsl_mc_allocator dpcon.16: Adding to iommu group 1
[    2.244266] fsl_mc_allocator dpcon.15: Adding to iommu group 1
[    2.250215] fsl_mc_allocator dpcon.14: Adding to iommu group 1
[    2.256157] fsl_mc_allocator dpcon.13: Adding to iommu group 1
[    2.262103] fsl_mc_allocator dpcon.12: Adding to iommu group 1
[    2.268047] fsl_mc_allocator dpcon.11: Adding to iommu group 1
[    2.273995] fsl_mc_allocator dpcon.10: Adding to iommu group 1
[    2.279946] fsl_mc_allocator dpcon.9: Adding to iommu group 1
[    2.285804] fsl_mc_allocator dpcon.8: Adding to iommu group 1
[    2.291665] fsl_mc_allocator dpcon.7: Adding to iommu group 1
[    2.297522] fsl_mc_allocator dpcon.6: Adding to iommu group 1
[    2.303385] fsl_mc_allocator dpcon.5: Adding to iommu group 1
[    2.309245] fsl_mc_allocator dpcon.4: Adding to iommu group 1
[    2.315107] fsl_mc_allocator dpcon.3: Adding to iommu group 1
[    2.320968] fsl_mc_allocator dpcon.2: Adding to iommu group 1
[    2.326830] fsl_mc_allocator dpcon.1: Adding to iommu group 1
[    2.332695] fsl_mc_allocator dpcon.0: Adding to iommu group 1
[    2.338563] fsl_dpaa2_eth dpni.9: Adding to iommu group 1
[    2.383371] fsl_dpaa2_eth dpni.8: Adding to iommu group 1
[    2.428137] fsl_dpaa2_eth dpni.7: Adding to iommu group 1
[    2.472895] fsl_dpaa2_eth dpni.6: Adding to iommu group 1
[    2.517647] fsl_dpaa2_eth dpni.5: Adding to iommu group 1
[    2.562386] fsl_dpaa2_eth dpni.4: Adding to iommu group 1
[    2.607137] fsl_dpaa2_eth dpni.3: Adding to iommu group 1
[    2.651899] fsl_dpaa2_eth dpni.2: Adding to iommu group 1
[    2.696673] fsl_dpaa2_eth dpni.1: Adding to iommu group 1
[    2.741424] fsl_dpaa2_eth dpni.0: Adding to iommu group 1
[    2.787700] fsl_mc_dpio dpio.8: Adding to iommu group 1
[    2.796503] fsl_mc_dpio dpio.8: probed
[    2.801910] fsl_mc_dpio dpio.7: Adding to iommu group 1
[    2.810753] fsl_mc_dpio dpio.7: probed
[    2.816138] fsl_mc_dpio dpio.6: Adding to iommu group 1
[    2.824973] fsl_mc_dpio dpio.6: probed
[    2.830353] fsl_mc_dpio dpio.5: Adding to iommu group 1
[    2.838950] fsl_mc_dpio dpio.5: probed
[    2.844331] fsl_mc_dpio dpio.4: Adding to iommu group 1
[    2.852968] fsl_mc_dpio dpio.4: probed
[    2.858347] fsl_mc_dpio dpio.3: Adding to iommu group 1
[    2.866951] fsl_mc_dpio dpio.3: probed
[    2.872330] fsl_mc_dpio dpio.2: Adding to iommu group 1
[    2.880967] fsl_mc_dpio dpio.2: probed
[    2.886347] fsl_mc_dpio dpio.1: Adding to iommu group 1
[    2.894952] fsl_mc_dpio dpio.1: probed
[    2.902308] fsl_mc_dprc dprc.1: DPRC device bound to driver
[    2.907986] pcieport 0000:00:00.0: Adding to iommu group 2
[    2.913597] pcieport 0000:00:00.0: PME: Signaling with IRQ 371
[    2.919596] pcieport 0001:00:00.0: Adding to iommu group 3
[    2.925170] pcieport 0001:00:00.0: PME: Signaling with IRQ 372
[    2.931144] pcieport 0001:01:00.0: Adding to iommu group 3
[    2.936991] pcieport 0001:02:01.0: Adding to iommu group 3
[    2.943176] pcieport 0001:02:02.0: Adding to iommu group 3
[    2.949360] pcieport 0002:00:00.0: Adding to iommu group 4
[    2.954916] pcieport 0002:00:00.0: PME: Signaling with IRQ 105
[    3.001572] fsl_dpaa2_eth dpni.9 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1c] driver [Generic PHY] (irq=POLL)
[    3.013769] fsl_dpaa2_eth dpni.9: Probed interface eth0
[    3.059715] fsl_dpaa2_eth dpni.8 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1d] driver [Generic PHY] (irq=POLL)
[    3.071848] fsl_dpaa2_eth dpni.8: Probed interface eth1
[    3.118262] fsl_dpaa2_eth dpni.7 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1e] driver [Generic PHY] (irq=POLL)
[    3.130373] fsl_dpaa2_eth dpni.7: Probed interface eth2
[    3.176529] fsl_dpaa2_eth dpni.6 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1f] driver [Generic PHY] (irq=POLL)
[    3.188651] fsl_dpaa2_eth dpni.6: Probed interface eth3
[    3.234567] fsl_dpaa2_eth dpni.5 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0c] driver [Generic PHY] (irq=POLL)
[    3.246686] fsl_dpaa2_eth dpni.5: Probed interface eth4
[    3.292603] fsl_dpaa2_eth dpni.4 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0d] driver [Generic PHY] (irq=POLL)
[    3.304717] fsl_dpaa2_eth dpni.4: Probed interface eth5
[    3.350646] fsl_dpaa2_eth dpni.3 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0e] driver [Generic PHY] (irq=POLL)
[    3.362766] fsl_dpaa2_eth dpni.3: Probed interface eth6
[    3.408672] fsl_dpaa2_eth dpni.2 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0f] driver [Generic PHY] (irq=POLL)
[    3.420797] fsl_dpaa2_eth dpni.2: Probed interface eth7
[    3.466897] fsl_dpaa2_eth dpni.1: Probed interface eth8
[    3.512981] fsl_dpaa2_eth dpni.0: Probed interface eth9
[    3.526673] EXT4-fs (nvme0n1p2): mounted filesystem without journal. Opts: (null)
[    3.534174] VFS: Mounted root (ext4 filesystem) readonly on device 259:2.
[    3.541146] Freeing unused kernel memory: 896K
[    3.590463] Run /sbin/init as init process
[    3.600421] init: Console is alive
[    3.603885] init: - watchdog -
[    3.621396] random: fast init done
[    3.632381] kmodloader: loading kernel modules from /etc/modules-boot.d/*
[    3.644473] Button Hotplug driver version 0.4.1
[    3.660516] tpm tpm0: starting up the TPM manually
[    3.750288] rtc-rx8025 1-0032: Type RX-8035
[    3.756968] rtc-rx8025 1-0032: registered as rtc0
[    3.763441] rtc-rx8025 1-0032: setting system clock to 2021-06-22T06:49:13 UTC (1624344553)
[    3.773385] input: buttons as /devices/platform/buttons/input/input0
[    3.780564] usbcore: registered new interface driver uas
[    3.785954] kmodloader: done loading kernel modules from /etc/modules-boot.d/*
[    3.798266] init: - preinit -
[    3.847146] random: jshn: uninitialized urandom read (4 bytes read)
[    3.857015] random: jshn: uninitialized urandom read (4 bytes read)
Press the [f] key and hit [enter] to enter failsafe mode
Press the [1], [2], [3] or [4] key and hit [enter] to select the debug level
[    6.890207] mount_root: mounting /dev/root
[    6.898975] EXT4-fs (nvme0n1p2): re-mounted. Opts: (null)
[    6.904428] mount_root: loading kmods from internal overlay
[    6.919257] kmodloader: loading kernel modules from //etc/modules-boot.d/*
[    6.926791] kmodloader: done loading kernel modules from //etc/modules-boot.d/*
[    6.944674] block: attempting to load /etc/config/fstab
[    6.950218] block: extroot: not configured
[    6.979679] random: procd: uninitialized urandom read (4 bytes read)
[    7.001875] urandom-seed: Seeding with /etc/urandom.seed
[    7.011452] procd: - early -
[    7.014350] procd: - watchdog -
[    7.533370] procd: - watchdog -
[    7.536634] procd: - ubus -
[    7.542452] random: ubusd: uninitialized urandom read (4 bytes read)
[    7.589396] random: ubusd: uninitialized urandom read (4 bytes read)
[    7.596617] procd: - init -
Please press Enter to activate this console.
[    7.664039] urngd: v1.0.2 started.
[    7.669289] kmodloader: loading kernel modules from /etc/modules.d/*
[    7.679538] caam 8000000.crypto: device ID = 0x0a13030000000000 (Era 8)
[    7.686165] caam 8000000.crypto: job rings = 3, qi = 1
[    7.686779] random: crng init done
[    7.694708] random: 5 urandom warning(s) missed due to ratelimiting
[    7.707902] caam algorithms registered in /proc/crypto
[    7.713863] caam 8000000.crypto: caam pkc algorithms registered in /proc/crypto
[    7.721174] caam 8000000.crypto: registering rng-caam
[    7.729174] dpaa2_caam dpseci.0: Adding to iommu group 1
[    7.736151] dpaa2_caam dpseci.0: dpseci v5.3
[    7.752945] fsl_mc_dprc dprc.1 (unnamed net_device) (uninitialized): netif_napi_add() called with weight 512
[    7.771599] dpaa2_caam dpseci.0: algorithms registered in /proc/crypto
[    7.778894] dpaa2_caam dpseci.0: hash algorithms registered in /proc/crypto
[    7.820506] tun: Universal TUN/TAP device driver, 1.6
[    7.833604] i2c i2c-2: Added multiplexed i2c bus 3
[    7.838586] i2c i2c-2: Added multiplexed i2c bus 4
[    7.843388] pca954x 2-0070: registered 2 multiplexed busses for I2C mux pca9540
[    7.851582] pca953x 0-0076: using no AI
[    7.858196] gpio-381 (admin_led_lower): hogged as output/low
[    7.868657] emc2301 0-002f: EMC2301 detected
[    7.872938] emc2301 0-002f: Have 1 fans configured in DT
[    7.878251] emc2301 0-002f: Fan 0 Cooling step is 312 RPM, minimum 3000, max 5500 RPM
[    7.888134] emc2301 0-002f: registering a cooling device
[    7.896777] GACT probability on
[    7.900222] Mirror/redirect action on
[    7.905940] u32 classifier
[    7.908649]     input device check on
[    7.912307]     Actions configured
[    7.921954] usbcore: registered new interface driver cdc_wdm
[    7.928008] Loading modules backported from Linux version v5.10.42-0-g65859eca4dff
[    7.935578] Backport generated by backports.git v5.10.42-1-0-gbee5c545
[    7.947935] libphy: SFP I2C Bus: probed
[    7.953272] sfp dpmac2_sfp: Host maximum power 2.0W
[    7.961142] libphy: SFP I2C Bus: probed
[    7.966445] sfp dpmac1_sfp: Host maximum power 2.0W
[    7.975666] usbcore: registered new device driver usbip-host
[    7.983656] usbcore: registered new interface driver usbserial_generic
[    7.990216] usbserial: USB Serial support registered for generic
[    7.996898] vhci_hcd vhci_hcd.0: USB/IP Virtual Host Controller
[    8.002823] vhci_hcd vhci_hcd.0: new USB bus registered, assigned bus number 5
[    8.010061] vhci_hcd: created sysfs vhci_hcd.0
[    8.014753] hub 5-0:1.0: USB hub found
[    8.018519] hub 5-0:1.0: 8 ports detected
[    8.022941] vhci_hcd vhci_hcd.0: USB/IP Virtual Host Controller
[    8.028877] vhci_hcd vhci_hcd.0: new USB bus registered, assigned bus number 6
[    8.036168] usb usb6: We don't know the algorithms for LPM for this host, disabling LPM.
[    8.044557] hub 6-0:1.0: USB hub found
[    8.048336] hub 6-0:1.0: 8 ports detected
[    8.057283] xt_time: kernel timezone is -0000
[    8.062552] usbcore: registered new interface driver asix
[    8.068716] usbcore: registered new interface driver cdc_ether
[    8.075630] usbcore: registered new interface driver cdc_ncm
[    8.089606] usbcore: registered new interface driver ftdi_sio
[    8.095389] usbserial: USB Serial support registered for FTDI USB Serial Device
[    8.107454] Intel(R) Wireless WiFi driver for Linux
[    8.133842] PPP generic driver version 2.4.2
[    8.138682] NET: Registered protocol family 24
[    8.143764] usbcore: registered new interface driver qmi_wwan
[    8.150168] usbcore: registered new interface driver rndis_host
[    8.157931] wireguard: WireGuard 1.0.0 loaded. See www.wireguard.com for information.
[    8.165771] wireguard: Copyright (C) 2015-2019 Jason A. Donenfeld <Jason@zx2c4.com>. All Rights Reserved.
[    8.192200] usbcore: registered new interface driver cdc_mbim
[    8.205951] usbcore: registered new interface driver option
[    8.211571] usbserial: USB Serial support registered for GSM modem (1-port)
[    8.220091] usbcore: registered new interface driver qcserial
[    8.225868] usbserial: USB Serial support registered for Qualcomm USB modem
[    8.236633] kmodloader: done loading kernel modules from /etc/modules.d/*
[   10.277520] fsl_dpaa2_eth dpni.9 eth0: configuring for inband/qsgmii link mode
[   10.296185] br-lan: port 1(eth0) entered blocking state
[   10.301442] br-lan: port 1(eth0) entered disabled state
[   10.308855] device eth0 entered promiscuous mode
[   10.319603] fsl_dpaa2_eth dpni.8 eth1: configuring for inband/qsgmii link mode



BusyBox v1.33.1 (2021-06-17 07:42:32 UTC) built-in shell (ash)

  _______                     ________        __
 |       |.-----.-----.-----.|  |  |  |.----.|  |_
 |   -   ||  _  |  -__|     ||  |  |  ||   _||   _|
 |_______||   __|_____|__|__||________||__|  |____|
          |__| W I R E L E S S   F R E E D O M
 -----------------------------------------------------
 OpenWrt 21.02-SNAPSHOT, r0-ab0210e
 -----------------------------------------------------
=== WARNING! =====================================
There is no root password defined on this device!
Use the "passwd" command to set up a new password
in order to prevent unauthorized SSH logins.
--------------------------------------------------
root@OpenWrt:/#

```

## Fimware v0.8.5 + Debian-sid (debian kernel)

```
  *** U-Boot Boot Menu ***

     Normal boot
     Built-in recovery
     Boot from NVMe
     Boot from USB
     Boot from SD
     Boot OpenWrt from NAND
     Boot from network
     Boot from SATA
     U-Boot console


  Press UP/DOWN to move, ENTER to select

Device 0: Vendor: 0x144d Rev: GXA7301Q Prod: S674NE0NC02476
            Type: Hard Disk
            Capacity: 244198.3 MB = 238.4 GB (500118192 x 512)
... is now current device
Scanning nvme 0:f...
** Unable to read file / **
Found EFI removable media binary efi/boot/bootaa64.efi
device 0 offset 0x580000, size 0x40000
SF: 262144 bytes @ 0x580000 Read: OK
device 0 offset 0x600000, size 0x40000
SF: 262144 bytes @ 0x600000 Read: OK
TEN64 ft_board_setup start, blob 0000000090000000
INFO:    RNG Desc SUCCESS with status 0
INFO:    result ccbc63a647e08f72
TEN64 ft_board_setup end
Card did not respond to voltage select!
mmc_init: -95, time 19
Scanning disk esdhc@2140000.blk...
Disk esdhc@2140000.blk not ready
Scanning disk nvme#0.blk#0...
** Unrecognized filesystem type **
Found 4 disks
TEN64 ft_board_setup start, blob 0000000087f00000
TEN64 ft_board_setup already run, not doing anything
BootOrder not defined
EFI boot manager: Cannot load any image
147456 bytes read in 1 ms (140.6 MiB/s)
TEN64 ft_board_setup start, blob 0000000087ee7000
TEN64 ft_board_setup already run, not doing anything
Welcome to GRUB!

error: no suitable video mode found.

                           GNU GRUB  version 2.04-18

 ┌────────────────────────────────────────────────────────────────────────────┐
 │*Debian GNU/Linux                                                           │
 │ Advanced options for Debian GNU/Linux                                      │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 └────────────────────────────────────────────────────────────────────────────┘

      Use the ▲ and ▼ keys to select which entry is highlighted.
      Press enter to boot the selected OS, `e' to edit the commands
      before booting or `c' for a command-line.
   The highlighted entry will be executed automatically in 0s.
  Booting `Debian GNU/Linux'

Loading Linux 5.10.0-7-arm64 ...
Loading initial ramdisk ...
EFI stub: Booting Linux Kernel...
EFI stub: EFI_RNG_PROTOCOL unavailable, KASLR will be disabled
EFI stub: ERROR: Could not determine UEFI Secure Boot status.
EFI stub: Using DTB from configuration table
EFI stub: Exiting boot services and installing virtual address map...
EFI stub: ERROR: Could not determine UEFI Secure Boot status.
fsl-mc: Deploying data path layout ... SUCCESS
[    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
[    0.000000] Linux version 5.10.0-7-arm64 (debian-kernel@lists.debian.org) (gcc-10 (Debian 10.2.1-6) 10.2.1 20210110, GNU ld (GNU Binutils for Debian) 2.35.2) #1 SMP Debian 5.10.40-1 (2021-05-28)
[    0.000000] Machine model: Traverse Ten64
[    0.000000] efi: EFI v2.80 by Das U-Boot
[    0.000000] efi: RTPROP=0x83dfff7040 SMBIOS=0xf69c6000 MEMRESERVE=0x8378ad4040
[    0.000000] secureboot: Secure boot could not be determined (mode 1)
[    0.000000] earlycon: ns16550a0 at MMIO 0x00000000021c0500 (options '115200n8')
[    0.000000] printk: bootconsole [ns16550a0] enabled
[    0.000000] NUMA: No NUMA configuration found
[    0.000000] NUMA: Faking a node at [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000] NUMA: NODE_DATA [mem 0x83de0e3b00-0x83de0e5fff]
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000080000000-0x00000000ffffffff]
[    0.000000]   DMA32    empty
[    0.000000]   Normal   [mem 0x0000000100000000-0x00000083dfffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000080000000-0x00000000f69c5fff]
[    0.000000]   node   0: [mem 0x00000000f69c6000-0x00000000f69c6fff]
[    0.000000]   node   0: [mem 0x00000000f69c7000-0x00000000fbcbffff]
[    0.000000]   node   0: [mem 0x00000000fbcc0000-0x00000000fbccffff]
[    0.000000]   node   0: [mem 0x00000000fbcd0000-0x00000000fbdfffff]
[    0.000000]   node   0: [mem 0x0000008080000000-0x00000083dffc1fff]
[    0.000000]   node   0: [mem 0x00000083dffc2000-0x00000083dffc3fff]
[    0.000000]   node   0: [mem 0x00000083dffc4000-0x00000083dffeafff]
[    0.000000]   node   0: [mem 0x00000083dffeb000-0x00000083dffeffff]
[    0.000000]   node   0: [mem 0x00000083dfff0000-0x00000083dfff2fff]
[    0.000000]   node   0: [mem 0x00000083dfff3000-0x00000083dfff3fff]
[    0.000000]   node   0: [mem 0x00000083dfff4000-0x00000083dfff6fff]
[    0.000000]   node   0: [mem 0x00000083dfff7000-0x00000083dfffffff]
[    0.000000] Initmem setup node 0 [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000] cma: Reserved 64 MiB at 0x00000000f7c00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] psci: SMC Calling Convention v1.1
[    0.000000] percpu: Embedded 33 pages/cpu s95192 r8192 d31784 u135168
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: detected: ARM erratum 845719
[    0.000000] CPU features: detected: GIC system register CPU interface
[    0.000000] CPU features: kernel page table isolation forced ON by KASLR
[    0.000000] CPU features: detected: Kernel page table isolation (KPTI)
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 3982848
[    0.000000] Policy zone: Normal
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/vmlinuz-5.10.0-7-arm64 root=/dev/nvme0n1p1 ro console=ttyS0,115200 earlycon arm-smmu.disable_bypass=0 net.ifnames=0
[    0.000000] Dentry cache hash table entries: 2097152 (order: 12, 16777216 bytes, linear)
[    0.000000] Inode-cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:on, heap free:off
[    0.000000] software IO TLB: mapped [mem 0x00000000f0f3c000-0x00000000f4f3c000] (64MB)
[    0.000000] Memory: 2919828K/16185344K available (11776K kernel code, 2436K rwdata, 7008K rodata, 5440K init, 598K bss, 440248K reserved, 65536K cma-reserved)
[    0.000000] random: get_random_u64 called from __kmem_cache_create+0x38/0x560 with crng_init=0
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=8, Nodes=1
[    0.000000] ftrace: allocating 38529 entries in 151 pages
[    0.000000] ftrace: allocated 151 pages with 5 groups
[    0.000000] rcu: Hierarchical RCU implementation.
[    0.000000] rcu:     RCU restricting CPUs from NR_CPUS=256 to nr_cpu_ids=8.
[    0.000000]  Rude variant of Tasks RCU enabled.
[    0.000000]  Tracing variant of Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 25 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=8
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GICv3: GIC: Using split EOI/Deactivate mode
[    0.000000] GICv3: 256 SPIs implemented
[    0.000000] GICv3: 0 Extended SPIs implemented
[    0.000000] GICv3: Distributor has no Range Selector support
[    0.000000] GICv3: 16 PPIs implemented
[    0.000000] GICv3: CPU0: found redistributor 0 region 0:0x0000000006100000
[    0.000000] ITS [mem 0x06020000-0x0603ffff]
[    0.000000] ITS@0x0000000006020000: allocated 65536 Devices @8080200000 (flat, esz 8, psz 64K, shr 0)
[    0.000000] ITS: using cache flushing for cmd queue
[    0.000000] GICv3: using LPI property table @0x0000008080190000
[    0.000000] GIC: using cache flushing for LPI property table
[    0.000000] GICv3: CPU0: using allocated LPI pending table @0x00000080801a0000
[    0.000000] arch_timer: cp15 timer(s) running at 25.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x5c409fb33, max_idle_ns: 440795203156 ns
[    0.000002] sched_clock: 56 bits at 25MHz, resolution 39ns, wraps every 4398046511103ns
[    0.008329] Console: colour dummy device 80x25
[    0.012861] Calibrating delay loop (skipped), value calculated using timer frequency.. 50.00 BogoMIPS (lpj=100000)
[    0.023282] pid_max: default: 32768 minimum: 301
[    0.028012] LSM: Security Framework initializing
[    0.032671] Yama: disabled by default; enable with sysctl kernel.yama.*
[    0.039383] AppArmor: AppArmor initialized
[    0.043504] TOMOYO Linux initialized
[    0.047167] Mount-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.054820] Mountpoint-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.064065] rcu: Hierarchical SRCU implementation.
[    0.069450] Platform MSI: gic-its@6020000 domain created
[    0.074863] PCI/MSI: /interrupt-controller@6000000/gic-its@6020000 domain created
[    0.082436] fsl-mc MSI: gic-its@6020000 domain created
[    0.088003] Remapping and enabling EFI services.
[    0.092985] smp: Bringing up secondary CPUs ...
[    0.097896] Detected VIPT I-cache on CPU1
[    0.097917] GICv3: CPU1: found redistributor 1 region 0:0x0000000006120000
[    0.097947] GICv3: CPU1: using allocated LPI pending table @0x00000080801b0000
[    0.097980] CPU1: Booted secondary processor 0x0000000001 [0x410fd034]
[    0.098374] Detected VIPT I-cache on CPU2
[    0.098387] GICv3: CPU2: found redistributor 2 region 0:0x0000000006140000
[    0.098414] GICv3: CPU2: using allocated LPI pending table @0x00000080801c0000
[    0.098436] CPU2: Booted secondary processor 0x0000000002 [0x410fd034]
[    0.098817] Detected VIPT I-cache on CPU3
[    0.098830] GICv3: CPU3: found redistributor 3 region 0:0x0000000006160000
[    0.098858] GICv3: CPU3: using allocated LPI pending table @0x00000080801d0000
[    0.098877] CPU3: Booted secondary processor 0x0000000003 [0x410fd034]
[    0.099256] Detected VIPT I-cache on CPU4
[    0.099276] GICv3: CPU4: found redistributor 100 region 0:0x0000000006180000
[    0.099315] GICv3: CPU4: using allocated LPI pending table @0x00000080801e0000
[    0.099347] CPU4: Booted secondary processor 0x0000000100 [0x410fd034]
[    0.099774] Detected VIPT I-cache on CPU5
[    0.099788] GICv3: CPU5: found redistributor 101 region 0:0x00000000061a0000
[    0.099819] GICv3: CPU5: using allocated LPI pending table @0x00000080801f0000
[    0.099839] CPU5: Booted secondary processor 0x0000000101 [0x410fd034]
[    0.100232] Detected VIPT I-cache on CPU6
[    0.100247] GICv3: CPU6: found redistributor 102 region 0:0x00000000061c0000
[    0.100276] GICv3: CPU6: using allocated LPI pending table @0x0000008080280000
[    0.100296] CPU6: Booted secondary processor 0x0000000102 [0x410fd034]
[    0.100699] Detected VIPT I-cache on CPU7
[    0.100714] GICv3: CPU7: found redistributor 103 region 0:0x00000000061e0000
[    0.100744] GICv3: CPU7: using allocated LPI pending table @0x0000008080290000
[    0.100764] CPU7: Booted secondary processor 0x0000000103 [0x410fd034]
[    0.100831] smp: Brought up 1 node, 8 CPUs
[    0.279058] SMP: Total of 8 processors activated.
[    0.283789] CPU features: detected: 32-bit EL0 Support
[    0.288959] CPU features: detected: CRC32 instructions
[    0.294127] CPU features: detected: 32-bit EL1 Support
[    0.307565] CPU: All CPU(s) started at EL2
[    0.311723] alternatives: patching kernel code
[    0.890698] node 0 deferred pages initialised in 572ms
[    0.896660] devtmpfs: initialized
[    0.903710] Registered cp15_barrier emulation handler
[    0.908802] Registered setend emulation handler
[    0.913362] KASLR enabled
[    0.916110] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.925940] futex hash table entries: 2048 (order: 5, 131072 bytes, linear)
[    0.934705] pinctrl core: initialized pinctrl subsystem
[    0.940428] SMBIOS 3.0 present.
[    0.943590] DMI: traverse ten64/ten64, BIOS 2020.07-rc1-gb47b96d4 06/22/2021
[    0.951043] NET: Registered protocol family 16
[    0.956667] DMA: preallocated 2048 KiB GFP_KERNEL pool for atomic allocations
[    0.964295] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA pool for atomic allocations
[    0.972908] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.980982] audit: initializing netlink subsys (disabled)
[    0.986762] audit: type=2000 audit(0.832:1): state=initialized audit_enabled=0 res=1
[    0.987365] thermal_sys: Registered thermal governor 'fair_share'
[    0.994561] thermal_sys: Registered thermal governor 'bang_bang'
[    1.000693] thermal_sys: Registered thermal governor 'step_wise'
[    1.006736] thermal_sys: Registered thermal governor 'user_space'
[    1.012779] thermal_sys: Registered thermal governor 'power_allocator'
[    1.019115] cpuidle: using governor ladder
[    1.029812] cpuidle: using governor menu
[    1.033864] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    1.040812] ASID allocator initialised with 32768 entries
[    1.046484] Serial: AMBA PL011 UART driver
[    1.058882] Machine: Traverse Ten64
[    1.062385] SoC family: QorIQ LS1088A
[    1.066069] SoC ID: svr:0x87030010, Revision: 1.0
[    1.072826] fsl_mc_bus 80c000000.fsl-mc: MC firmware version: 10.20.4
[    1.081876] fsl_mc_dprc dprc.1: DMA mask not set
[    1.090569] HugeTLB registered 1.00 GiB page size, pre-allocated 0 pages
[    1.097322] HugeTLB registered 32.0 MiB page size, pre-allocated 0 pages
[    1.104066] HugeTLB registered 2.00 MiB page size, pre-allocated 0 pages
[    1.110810] HugeTLB registered 64.0 KiB page size, pre-allocated 0 pages
[    1.537875] ACPI: Interpreter disabled.
[    1.541880] iommu: Default domain type: Translated
[    1.546896] vgaarb: loaded
[    1.549834] EDAC MC: Ver: 3.0.0
[    1.553786] NetLabel: Initializing
[    1.557220] NetLabel:  domain hash size = 128
[    1.561600] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    1.567335] NetLabel:  unlabeled traffic allowed by default
[    1.573137] clocksource: Switched to clocksource arch_sys_counter
[    1.622936] VFS: Disk quotas dquot_6.6.0
[    1.626924] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.634299] AppArmor: AppArmor Filesystem Enabled
[    1.639155] pnp: PnP ACPI: disabled
[    1.647216] NET: Registered protocol family 2
[    1.651919] IP idents hash table entries: 262144 (order: 9, 2097152 bytes, linear)
[    1.663820] tcp_listen_portaddr_hash hash table entries: 8192 (order: 5, 131072 bytes, linear)
[    1.672767] TCP established hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    1.681636] TCP bind hash table entries: 65536 (order: 8, 1048576 bytes, linear)
[    1.689659] TCP: Hash tables configured (established 131072 bind 65536)
[    1.696449] UDP hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    1.703523] UDP-Lite hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    1.711103] NET: Registered protocol family 1
[    1.715503] NET: Registered protocol family 44
[    1.719977] PCI: CLS 0 bytes, default 64
[    1.724064] Trying to unpack rootfs image as initramfs...
[    2.618876] Freeing initrd memory: 27172K
[    2.623600] hw perfevents: enabled with armv8_pmuv3 PMU driver, 7 counters available
[    2.631587] kvm [1]: IPA Size Limit: 40 bits
[    2.636786] kvm [1]: vgic-v2@c0e0000
[    2.640392] kvm [1]: GIC system register CPU interface enabled
[    2.646385] kvm [1]: vgic interrupt IRQ9
[    2.650488] kvm [1]: Hyp mode initialized successfully
[    2.656747] Initialise system trusted keyrings
[    2.661245] Key type blacklist registered
[    2.665400] workingset: timestamp_bits=42 max_order=22 bucket_order=0
[    2.675541] zbud: loaded
[    2.678518] integrity: Platform Keyring initialized
[    2.683431] Key type asymmetric registered
[    2.687554] Asymmetric key parser 'x509' registered
[    2.692478] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 249)
[    2.700018] io scheduler mq-deadline registered
[    2.706824] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4
[    2.713934] layerscape-pcie 3400000.pcie: host bridge /soc/pcie@3400000 ranges:
[    2.721307] layerscape-pcie 3400000.pcie:       IO 0x2000010000..0x200001ffff -> 0x0000000000
[    2.729897] layerscape-pcie 3400000.pcie:      MEM 0x2040000000..0x207fffffff -> 0x0040000000
[    2.738596] layerscape-pcie 3400000.pcie: PCI host bridge to bus 0000:00
[    2.745345] pci_bus 0000:00: root bus resource [bus 00-ff]
[    2.750867] pci_bus 0000:00: root bus resource [io  0x0000-0xffff]
[    2.757088] pci_bus 0000:00: root bus resource [mem 0x2040000000-0x207fffffff] (bus address [0x40000000-0x7fffffff])
[    2.767699] pci 0000:00:00.0: [1957:80c0] type 01 class 0x060400
[    2.773795] pci 0000:00:00.0: supports D1 D2
[    2.778091] pci 0000:00:00.0: PME# supported from D0 D1 D2 D3hot
[    2.785832] pci 0000:00:00.0: PCI bridge to [bus 01-ff]
[    2.791264] layerscape-pcie 3500000.pcie: host bridge /soc/pcie@3500000 ranges:
[    2.798660] layerscape-pcie 3500000.pcie:       IO 0x2800010000..0x280001ffff -> 0x0000000000
[    2.807264] layerscape-pcie 3500000.pcie:      MEM 0x2840000000..0x287fffffff -> 0x0040000000
[    2.815943] layerscape-pcie 3500000.pcie: PCI host bridge to bus 0001:00
[    2.822691] pci_bus 0001:00: root bus resource [bus 00-ff]
[    2.828214] pci_bus 0001:00: root bus resource [io  0x10000-0x1ffff] (bus address [0x0000-0xffff])
[    2.837236] pci_bus 0001:00: root bus resource [mem 0x2840000000-0x287fffffff] (bus address [0x40000000-0x7fffffff])
[    2.847843] pci 0001:00:00.0: [1957:80c0] type 01 class 0x060400
[    2.853924] pci 0001:00:00.0: supports D1 D2
[    2.858222] pci 0001:00:00.0: PME# supported from D0 D1 D2 D3hot
[    2.865230] pci 0001:01:00.0: [12d8:b304] type 01 class 0x060400
[    2.871654] pci 0001:01:00.0: supports D1 D2
[    2.875950] pci 0001:01:00.0: PME# supported from D0 D1 D2 D3hot D3cold
[    2.882787] pci 0001:01:00.0: PTM enabled (root), unknown granularity
[    2.889329] pci 0001:01:00.0: 4.000 Gb/s available PCIe bandwidth, limited by 5.0 GT/s PCIe x1 link at 0001:00:00.0 (capable of 8.000 Gb/s with 5.0 GT/s PCIe x2 link)
[    2.914128] pci 0001:02:01.0: [12d8:b304] type 01 class 0x060400
[    2.920560] pci 0001:02:01.0: supports D1 D2
[    2.924858] pci 0001:02:01.0: PME# supported from D0 D1 D2 D3hot D3cold
[    2.931913] pci 0001:02:02.0: [12d8:b304] type 01 class 0x060400
[    2.938344] pci 0001:02:02.0: supports D1 D2
[    2.942641] pci 0001:02:02.0: PME# supported from D0 D1 D2 D3hot D3cold
[    2.952140] pci 0001:02:01.0: PCI bridge to [bus 03]
[    2.957169] pci 0001:02:02.0: PCI bridge to [bus 04]
[    2.962195] pci 0001:01:00.0: PCI bridge to [bus 02-04]
[    2.967482] pci 0001:00:00.0: PCI bridge to [bus 01-ff]
[    2.973019] layerscape-pcie 3600000.pcie: host bridge /soc/pcie@3600000 ranges:
[    2.980405] layerscape-pcie 3600000.pcie:       IO 0x3000010000..0x300001ffff -> 0x0000000000
[    2.989007] layerscape-pcie 3600000.pcie:      MEM 0x3040000000..0x307fffffff -> 0x0040000000
[    2.997684] layerscape-pcie 3600000.pcie: PCI host bridge to bus 0002:00
[    3.004433] pci_bus 0002:00: root bus resource [bus 00-ff]
[    3.009954] pci_bus 0002:00: root bus resource [io  0x20000-0x2ffff] (bus address [0x0000-0xffff])
[    3.018990] pci_bus 0002:00: root bus resource [mem 0x3040000000-0x307fffffff] (bus address [0x40000000-0x7fffffff])
[    3.029597] pci 0002:00:00.0: [1957:80c0] type 01 class 0x060400
[    3.035676] pci 0002:00:00.0: supports D1 D2
[    3.039973] pci 0002:00:00.0: PME# supported from D0 D1 D2 D3hot
[    3.046968] pci 0002:01:00.0: [144d:a80a] type 00 class 0x010802
[    3.053070] pci 0002:01:00.0: reg 0x10: [mem 0x3040000000-0x3040003fff 64bit]
[    3.060827] pci 0002:01:00.0: 15.752 Gb/s available PCIe bandwidth, limited by 8.0 GT/s PCIe x2 link at 0002:00:00.0 (capable of 63.012 Gb/s with 16.0 GT/s PCIe x4 link)
[    3.076897] pci 0002:00:00.0: BAR 14: assigned [mem 0x3040000000-0x30400fffff]
[    3.084172] pci 0002:01:00.0: BAR 0: assigned [mem 0x3040000000-0x3040003fff 64bit]
[    3.091904] pci 0002:00:00.0: PCI bridge to [bus 01-ff]
[    3.097164] pci 0002:00:00.0:   bridge window [mem 0x3040000000-0x30400fffff]
[    3.107533] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    3.114992] printk: console [ttyS0] disabled
[    3.119335] 21c0500.serial: ttyS0 at MMIO 0x21c0500 (irq = 16, base_baud = 21875000) is a 16550A
[    3.128216] printk: console [ttyS0] enabled
[    3.128216] printk: console [ttyS0] enabled
[    3.136611] printk: bootconsole [ns16550a0] disabled
[    3.136611] printk: bootconsole [ns16550a0] disabled
[    3.147052] 21c0600.serial: ttyS1 at MMIO 0x21c0600 (irq = 16, base_baud = 21875000) is a 16550A
[    3.156118] Serial: AMBA driver
[    3.159494] msm_serial: driver initialized
[    3.164314] arm-smmu 5000000.iommu: probing hardware configuration...
[    3.170757] arm-smmu 5000000.iommu: SMMUv2 with:
[    3.175372] arm-smmu 5000000.iommu:  stage 1 translation
[    3.180682] arm-smmu 5000000.iommu:  stage 2 translation
[    3.185991] arm-smmu 5000000.iommu:  nested translation
[    3.191217] arm-smmu 5000000.iommu:  stream matching with 128 register groups
[    3.198354] arm-smmu 5000000.iommu:  64 context banks (0 stage-2 only)
[    3.204882] arm-smmu 5000000.iommu:  Supported page sizes: 0x61311000
[    3.211322] arm-smmu 5000000.iommu:  Stage-1: 48-bit VA -> 48-bit IPA
[    3.217761] arm-smmu 5000000.iommu:  Stage-2: 48-bit IPA -> 48-bit PA
[    3.224814] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    3.231305] mousedev: PS/2 mouse device common for all mice
[    3.237804] ledtrig-cpu: registered to indicate activity on CPUs
[    3.244806] NET: Registered protocol family 10
[    3.270786] Segment Routing with IPv6
[    3.274530] mip6: Mobile IPv6
[    3.277504] NET: Registered protocol family 17
[    3.282025] mpls_gso: MPLS GSO support
[    3.286069] registered taskstats version 1
[    3.290183] Loading compiled-in X.509 certificates
[    3.390198] Loaded X.509 cert 'Debian Secure Boot CA: 6ccece7e4c6c0d1f6149f3dd27dfcc5cbb419ea1'
[    3.398933] Loaded X.509 cert 'Debian Secure Boot Signer 2021 - linux: 4b6ef5abca669825178e052c84667ccbc0531f8c'
[    3.409207] zswap: loaded using pool lzo/zbud
[    3.413829] Key type ._fscrypt registered
[    3.417839] Key type .fscrypt registered
[    3.421760] Key type fscrypt-provisioning registered
[    3.426824] AppArmor: AppArmor sha1 policy hashing enabled
[    3.434796] fsl_mc_dprc dprc.1: Adding to iommu group 0
[    3.520612] fsl_mc_allocator dpbp.9: Adding to iommu group 0
[    3.526393] fsl_mc_allocator dpbp.8: Adding to iommu group 0
[    3.532171] fsl_mc_allocator dpbp.7: Adding to iommu group 0
[    3.537942] fsl_mc_allocator dpbp.6: Adding to iommu group 0
[    3.543718] fsl_mc_allocator dpbp.5: Adding to iommu group 0
[    3.549494] fsl_mc_allocator dpbp.4: Adding to iommu group 0
[    3.555284] fsl_mc_allocator dpbp.3: Adding to iommu group 0
[    3.561054] fsl_mc_allocator dpbp.2: Adding to iommu group 0
[    3.566830] fsl_mc_allocator dpbp.1: Adding to iommu group 0
[    3.572602] fsl_mc_allocator dpbp.0: Adding to iommu group 0
[    3.579385] fsl_mc_allocator dpmcp.34: Adding to iommu group 0
[    3.585834] fsl_mc_allocator dpmcp.33: Adding to iommu group 0
[    3.592291] fsl_mc_allocator dpmcp.32: Adding to iommu group 0
[    3.598741] fsl_mc_allocator dpmcp.31: Adding to iommu group 0
[    3.605197] fsl_mc_allocator dpmcp.30: Adding to iommu group 0
[    3.611656] fsl_mc_allocator dpmcp.29: Adding to iommu group 0
[    3.618106] fsl_mc_allocator dpmcp.28: Adding to iommu group 0
[    3.624557] fsl_mc_allocator dpmcp.27: Adding to iommu group 0
[    3.631012] fsl_mc_allocator dpmcp.26: Adding to iommu group 0
[    3.637471] fsl_mc_allocator dpmcp.25: Adding to iommu group 0
[    3.643923] fsl_mc_allocator dpmcp.24: Adding to iommu group 0
[    3.650372] fsl_mc_allocator dpmcp.23: Adding to iommu group 0
[    3.656825] fsl_mc_allocator dpmcp.22: Adding to iommu group 0
[    3.663286] fsl_mc_allocator dpmcp.21: Adding to iommu group 0
[    3.669747] fsl_mc_allocator dpmcp.20: Adding to iommu group 0
[    3.676198] fsl_mc_allocator dpmcp.19: Adding to iommu group 0
[    3.682650] fsl_mc_allocator dpmcp.18: Adding to iommu group 0
[    3.689098] fsl_mc_allocator dpmcp.17: Adding to iommu group 0
[    3.695550] fsl_mc_allocator dpmcp.16: Adding to iommu group 0
[    3.702002] fsl_mc_allocator dpmcp.15: Adding to iommu group 0
[    3.708451] fsl_mc_allocator dpmcp.14: Adding to iommu group 0
[    3.714916] fsl_mc_allocator dpmcp.13: Adding to iommu group 0
[    3.721371] fsl_mc_allocator dpmcp.12: Adding to iommu group 0
[    3.727824] fsl_mc_allocator dpmcp.11: Adding to iommu group 0
[    3.734279] fsl_mc_allocator dpmcp.10: Adding to iommu group 0
[    3.740732] fsl_mc_allocator dpmcp.9: Adding to iommu group 0
[    3.747100] fsl_mc_allocator dpmcp.8: Adding to iommu group 0
[    3.753469] fsl_mc_allocator dpmcp.7: Adding to iommu group 0
[    3.759836] fsl_mc_allocator dpmcp.6: Adding to iommu group 0
[    3.766200] fsl_mc_allocator dpmcp.5: Adding to iommu group 0
[    3.772577] fsl_mc_allocator dpmcp.4: Adding to iommu group 0
[    3.778941] fsl_mc_allocator dpmcp.3: Adding to iommu group 0
[    3.785329] fsl_mc_allocator dpmcp.2: Adding to iommu group 0
[    3.791694] fsl_mc_allocator dpmcp.1: Adding to iommu group 0
[    3.797568] fsl_mc_allocator dpcon.72: Adding to iommu group 0
[    3.803522] fsl_mc_allocator dpcon.71: Adding to iommu group 0
[    3.809472] fsl_mc_allocator dpcon.70: Adding to iommu group 0
[    3.815433] fsl_mc_allocator dpcon.69: Adding to iommu group 0
[    3.821385] fsl_mc_allocator dpcon.68: Adding to iommu group 0
[    3.827340] fsl_mc_allocator dpcon.67: Adding to iommu group 0
[    3.833293] fsl_mc_allocator dpcon.66: Adding to iommu group 0
[    3.839248] fsl_mc_allocator dpcon.65: Adding to iommu group 0
[    3.845206] fsl_mc_allocator dpcon.64: Adding to iommu group 0
[    3.851161] fsl_mc_allocator dpcon.63: Adding to iommu group 0
[    3.857112] fsl_mc_allocator dpcon.62: Adding to iommu group 0
[    3.863072] fsl_mc_allocator dpcon.61: Adding to iommu group 0
[    3.869027] fsl_mc_allocator dpcon.60: Adding to iommu group 0
[    3.874995] fsl_mc_allocator dpcon.59: Adding to iommu group 0
[    3.880947] fsl_mc_allocator dpcon.58: Adding to iommu group 0
[    3.886910] fsl_mc_allocator dpcon.57: Adding to iommu group 0
[    3.892861] fsl_mc_allocator dpcon.56: Adding to iommu group 0
[    3.898819] fsl_mc_allocator dpcon.55: Adding to iommu group 0
[    3.904773] fsl_mc_allocator dpcon.54: Adding to iommu group 0
[    3.910736] fsl_mc_allocator dpcon.53: Adding to iommu group 0
[    3.916692] fsl_mc_allocator dpcon.52: Adding to iommu group 0
[    3.922646] fsl_mc_allocator dpcon.51: Adding to iommu group 0
[    3.928607] fsl_mc_allocator dpcon.50: Adding to iommu group 0
[    3.934561] fsl_mc_allocator dpcon.49: Adding to iommu group 0
[    3.940519] fsl_mc_allocator dpcon.48: Adding to iommu group 0
[    3.946474] fsl_mc_allocator dpcon.47: Adding to iommu group 0
[    3.952431] fsl_mc_allocator dpcon.46: Adding to iommu group 0
[    3.958384] fsl_mc_allocator dpcon.45: Adding to iommu group 0
[    3.964345] fsl_mc_allocator dpcon.44: Adding to iommu group 0
[    3.970298] fsl_mc_allocator dpcon.43: Adding to iommu group 0
[    3.976256] fsl_mc_allocator dpcon.42: Adding to iommu group 0
[    3.982213] fsl_mc_allocator dpcon.41: Adding to iommu group 0
[    3.988177] fsl_mc_allocator dpcon.40: Adding to iommu group 0
[    3.994136] fsl_mc_allocator dpcon.39: Adding to iommu group 0
[    4.000090] fsl_mc_allocator dpcon.38: Adding to iommu group 0
[    4.006057] fsl_mc_allocator dpcon.37: Adding to iommu group 0
[    4.012012] fsl_mc_allocator dpcon.36: Adding to iommu group 0
[    4.017979] fsl_mc_allocator dpcon.35: Adding to iommu group 0
[    4.023935] fsl_mc_allocator dpcon.34: Adding to iommu group 0
[    4.029896] fsl_mc_allocator dpcon.33: Adding to iommu group 0
[    4.035851] fsl_mc_allocator dpcon.32: Adding to iommu group 0
[    4.041813] fsl_mc_allocator dpcon.31: Adding to iommu group 0
[    4.047770] fsl_mc_allocator dpcon.30: Adding to iommu group 0
[    4.053735] fsl_mc_allocator dpcon.29: Adding to iommu group 0
[    4.059694] fsl_mc_allocator dpcon.28: Adding to iommu group 0
[    4.065655] fsl_mc_allocator dpcon.27: Adding to iommu group 0
[    4.071612] fsl_mc_allocator dpcon.26: Adding to iommu group 0
[    4.077573] fsl_mc_allocator dpcon.25: Adding to iommu group 0
[    4.083531] fsl_mc_allocator dpcon.24: Adding to iommu group 0
[    4.089491] fsl_mc_allocator dpcon.23: Adding to iommu group 0
[    4.095453] fsl_mc_allocator dpcon.22: Adding to iommu group 0
[    4.101417] fsl_mc_allocator dpcon.21: Adding to iommu group 0
[    4.107377] fsl_mc_allocator dpcon.20: Adding to iommu group 0
[    4.113334] fsl_mc_allocator dpcon.19: Adding to iommu group 0
[    4.119298] fsl_mc_allocator dpcon.18: Adding to iommu group 0
[    4.125256] fsl_mc_allocator dpcon.17: Adding to iommu group 0
[    4.131217] fsl_mc_allocator dpcon.16: Adding to iommu group 0
[    4.137188] fsl_mc_allocator dpcon.15: Adding to iommu group 0
[    4.143150] fsl_mc_allocator dpcon.14: Adding to iommu group 0
[    4.149109] fsl_mc_allocator dpcon.13: Adding to iommu group 0
[    4.155074] fsl_mc_allocator dpcon.12: Adding to iommu group 0
[    4.161033] fsl_mc_allocator dpcon.11: Adding to iommu group 0
[    4.166995] fsl_mc_allocator dpcon.10: Adding to iommu group 0
[    4.172955] fsl_mc_allocator dpcon.9: Adding to iommu group 0
[    4.178839] fsl_mc_allocator dpcon.8: Adding to iommu group 0
[    4.184714] fsl_mc_allocator dpcon.7: Adding to iommu group 0
[    4.190599] fsl_mc_allocator dpcon.6: Adding to iommu group 0
[    4.196485] fsl_mc_allocator dpcon.5: Adding to iommu group 0
[    4.202358] fsl_mc_allocator dpcon.4: Adding to iommu group 0
[    4.208236] fsl_mc_allocator dpcon.3: Adding to iommu group 0
[    4.214109] fsl_mc_allocator dpcon.2: Adding to iommu group 0
[    4.219986] fsl_mc_allocator dpcon.1: Adding to iommu group 0
[    4.225860] fsl_mc_allocator dpcon.0: Adding to iommu group 0
[    4.244320] fsl_mc_dprc dprc.1: DPRC device bound to driver
[    4.250000] pcieport 0000:00:00.0: Adding to iommu group 1
[    4.255763] pcieport 0000:00:00.0: PME: Signaling with IRQ 361
[    4.261750] pcieport 0000:00:00.0: AER: enabled with IRQ 361
[    4.267588] pcieport 0001:00:00.0: Adding to iommu group 2
[    4.273311] pcieport 0001:00:00.0: PME: Signaling with IRQ 362
[    4.279293] pcieport 0001:00:00.0: AER: enabled with IRQ 362
[    4.285102] pcieport 0001:01:00.0: Adding to iommu group 2
[    4.291384] pcieport 0001:02:01.0: Adding to iommu group 2
[    4.297490] pcieport 0001:02:01.0: DPC: enabled with IRQ 365
[    4.303156] pcieport 0001:02:01.0: DPC: error containment capabilities: Int Msg #1, RPExt- PoisonedTLP- SwTrigger- RP PIO Log 0, DL_ActiveErr-
[    4.316346] pcieport 0001:02:02.0: Adding to iommu group 2
[    4.322427] pcieport 0001:02:02.0: DPC: enabled with IRQ 367
[    4.328098] pcieport 0001:02:02.0: DPC: error containment capabilities: Int Msg #1, RPExt- PoisonedTLP- SwTrigger- RP PIO Log 0, DL_ActiveErr-
[    4.341301] pcieport 0002:00:00.0: Adding to iommu group 3
[    4.347013] pcieport 0002:00:00.0: PME: Signaling with IRQ 368
[    4.353007] pcieport 0002:00:00.0: AER: enabled with IRQ 368
[    4.362980] Freeing unused kernel memory: 5440K
[    4.544845] Checked W+X mappings: passed, no W+X pages found
[    4.550525] Run /init as init process
Loading, please wait...
Starting version 247.3-5
[    4.783554] input: buttons as /devices/platform/buttons/input/input0
[    4.790427] imx-i2c 2000000.i2c: can't get pinctrl, bus recovery not supported
[    4.802275] pca953x 0-0076: supply vcc not found, using dummy regulator
[    4.809106] pca953x 0-0076: using no AI
[    4.810572] sdhci: Secure Digital Host Controller Interface driver
[    4.817035] gpio-381 (admin_led_lower): hogged as output/low
[    4.819143] sdhci: Copyright(c) Pierre Ossman
[    4.825802] i2c i2c-0: IMX I2C adapter registered
[    4.834093] sdhci-pltfm: SDHCI platform and OF driver helper
[    4.844937] libphy: Fixed MDIO Bus: probed
[    4.853277] libphy: Freescale XGMAC MDIO Bus: probed
[    4.864088] fsl_mc_dpio dpio.8: Adding to iommu group 0
[    4.866359] imx-i2c 2020000.i2c: can't get pinctrl, bus recovery not supported
[    4.868694] mmc0: SDHCI controller on 2140000.esdhc [2140000.esdhc] using ADMA 64-bit
[    4.875430] fsl_mc_dpio dpio.8: probed
[    4.876868] i2c i2c-1: IMX I2C adapter registered
[    4.885523] fsl_mc_dpio dpio.7: Adding to iommu group 0
[    4.888385] imx-i2c 2030000.i2c: can't get pinctrl, bus recovery not supported
[    4.893226] MACsec IEEE 802.1AE
[    4.898369] i2c i2c-2: IMX I2C adapter registered
[    4.898394] fsl_mc_dpio dpio.7: probed
[    4.899081] fsl_mc_dpio dpio.6: Adding to iommu group 0
[    4.902704] fsl_mc_dpio dpio.6: probed
[    4.903335] fsl_mc_dpio dpio.5: Adding to iommu group 0
[    4.905897] nvme 0002:01:00.0: Adding to iommu group 3
[    4.906716] fsl_mc_dpio dpio.5: probed
[    4.907361] fsl_mc_dpio dpio.4: Adding to iommu group 0
[    4.913285] fsl_mc_dpio dpio.4: probed
[    4.917448] nvme nvme0: pci function 0002:01:00.0
[    4.923198] fsl_mc_dpio dpio.3: Adding to iommu group 0
[    4.924766] usbcore: registered new interface driver usbfs
[    4.924799] usbcore: registered new interface driver hub
[    4.924838] usbcore: registered new device driver usb
[    4.933639] i2c i2c-2: Added multiplexed i2c bus 3
[    4.936637] nvme nvme0: Shutdown timeout set to 10 seconds
[    4.940057] fsl_mc_dpio dpio.3: probed
[    4.940187] i2c i2c-2: Added multiplexed i2c bus 4
[    4.940766] fsl_mc_dpio dpio.2: Adding to iommu group 0
[    4.944325] fsl_mc_dpio dpio.2: probed
[    4.945003] fsl_mc_dpio dpio.1: Adding to iommu group 0
[    4.948648] nvme nvme0: 8/0/0 default/read/poll queues
[    4.948844] fsl_mc_dpio dpio.1: probed
[    4.949082] pca954x 2-0070: registered 2 multiplexed busses for I2C mux pca9540
[    4.959564] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[    4.962029] libphy: Freescale XGMAC MDIO Bus: probed
[    4.966368] fsl_dpaa2_eth dpni.9: Adding to iommu group 0
[    4.967841] libphy: Freescale XGMAC MDIO Bus: probed
[    4.969846] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 1
[    4.973016] libphy: Freescale XGMAC MDIO Bus: probed
[    4.977196]  nvme0n1: p1 p2 p15
[    4.979805] xhci-hcd xhci-hcd.0.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[    4.992021] libphy: Freescale XGMAC MDIO Bus: probed
[    4.993744] xhci-hcd xhci-hcd.0.auto: irq 22, io mem 0x03100000
[    5.081118] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.10
[    5.089398] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    5.096629] usb usb1: Product: xHCI Host Controller
[    5.101505] usb usb1: Manufacturer: Linux 5.10.0-7-arm64 xhci-hcd
[    5.107597] usb usb1: SerialNumber: xhci-hcd.0.auto
[    5.112884] hub 1-0:1.0: USB hub found
[    5.116774] hub 1-0:1.0: 1 port detected
[    5.120958] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[    5.126454] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 2
[    5.134118] xhci-hcd xhci-hcd.0.auto: Host supports USB 3.0 SuperSpeed
[    5.140684] usb usb2: We don't know the algorithms for LPM for this host, disabling LPM.
[    5.148854] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
[    5.157122] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    5.164344] usb usb2: Product: xHCI Host Controller
[    5.169221] usb usb2: Manufacturer: Linux 5.10.0-7-arm64 xhci-hcd
[    5.175311] usb usb2: SerialNumber: xhci-hcd.0.auto
[    5.180491] hub 2-0:1.0: USB hub found
[    5.184264] hub 2-0:1.0: 1 port detected
[    5.188547] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[    5.194046] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 3
[    5.201811] xhci-hcd xhci-hcd.1.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[    5.211241] xhci-hcd xhci-hcd.1.auto: irq 23, io mem 0x03110000
[    5.217356] usb usb3: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.10
[    5.225632] usb usb3: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    5.232856] usb usb3: Product: xHCI Host Controller
[    5.237738] usb usb3: Manufacturer: Linux 5.10.0-7-arm64 xhci-hcd
[    5.243836] usb usb3: SerialNumber: xhci-hcd.1.auto
[    5.249019] hub 3-0:1.0: USB hub found
[    5.252790] hub 3-0:1.0: 1 port detected
[    5.256936] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[    5.262431] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 4
[    5.270093] xhci-hcd xhci-hcd.1.auto: Host supports USB 3.0 SuperSpeed
[    5.276655] usb usb4: We don't know the algorithms for LPM for this host, disabling LPM.
[    5.284816] usb usb4: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.10
[    5.293080] usb usb4: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[    5.300299] usb usb4: Product: xHCI Host Controller
[    5.305175] usb usb4: Manufacturer: Linux 5.10.0-7-arm64 xhci-hcd
[    5.311265] usb usb4: SerialNumber: xhci-hcd.1.auto
[    5.316421] hub 4-0:1.0: USB hub found
[    5.320191] hub 4-0:1.0: 1 port detected
[    5.469839] fsl_dpaa2_eth dpni.9 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1c] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    5.483445] fsl_dpaa2_eth dpni.9: Probed interface eth0
[    5.488842] fsl_dpaa2_eth dpni.8: Adding to iommu group 0
[    5.589150] usb 3-1: new high-speed USB device number 2 using xhci-hcd
[    5.725827] fsl_dpaa2_eth dpni.8 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1d] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    5.739367] fsl_dpaa2_eth dpni.8: Probed interface eth1
[    5.741631] usb 3-1: New USB device found, idVendor=0424, idProduct=2744, bcdDevice= 2.08
[    5.744746] fsl_dpaa2_eth dpni.7: Adding to iommu group 0
[    5.752765] usb 3-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[    5.765291] usb 3-1: Product: USB2744
[    5.768947] usb 3-1: Manufacturer: Microchip Tech
[    5.820793] hub 3-1:1.0: USB hub found
[    5.824580] hub 3-1:1.0: 4 ports detected
[    5.884641] usb 4-1: new SuperSpeed Gen 1 USB device number 2 using xhci-hcd
[    5.909514] usb 4-1: New USB device found, idVendor=0424, idProduct=5744, bcdDevice= 2.08
[    5.917697] usb 4-1: New USB device strings: Mfr=2, Product=3, SerialNumber=0
[    5.924835] usb 4-1: Product: USB5744
[    5.928495] usb 4-1: Manufacturer: Microchip Tech
[    5.964783] hub 4-1:1.0: USB hub found
[    5.968567] hub 4-1:1.0: 4 ports detected
[    6.005849] fsl_dpaa2_eth dpni.7 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1e] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    6.019465] fsl_dpaa2_eth dpni.7: Probed interface eth2
[    6.024856] fsl_dpaa2_eth dpni.6: Adding to iommu group 0
[    6.253834] fsl_dpaa2_eth dpni.6 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:1f] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    6.267409] fsl_dpaa2_eth dpni.6: Probed interface eth3
[    6.272786] fsl_dpaa2_eth dpni.5: Adding to iommu group 0
[    6.765832] fsl_dpaa2_eth dpni.5 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0c] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    6.779375] fsl_dpaa2_eth dpni.5: Probed interface eth4
[    6.784745] fsl_dpaa2_eth dpni.4: Adding to iommu group 0
[    7.037830] fsl_dpaa2_eth dpni.4 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0d] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    7.051375] fsl_dpaa2_eth dpni.4: Probed interface eth5
[    7.056740] fsl_dpaa2_eth dpni.3: Adding to iommu group 0
[    7.309831] fsl_dpaa2_eth dpni.3 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0e] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    7.323368] fsl_dpaa2_eth dpni.3: Probed interface eth6
[    7.328730] fsl_dpaa2_eth dpni.2: Adding to iommu group 0
[    7.581832] fsl_dpaa2_eth dpni.2 (unnamed net_device) (uninitialized): PHY [0x0000000008b96000:0f] driver [Microsemi GE VSC8514 SyncE] (irq=POLL)
[    7.595402] fsl_dpaa2_eth dpni.2: Probed interface eth7
[    7.600763] fsl_dpaa2_eth dpni.1: Adding to iommu group 0
[    7.646302] fsl_dpaa2_eth dpni.1: Probed interface eth8
[    7.651671] fsl_dpaa2_eth dpni.0: Adding to iommu group 0
[    7.697161] fsl_dpaa2_eth dpni.0: Probed interface eth9
Begin: Loading essential drivers ... done.
Begin: Running /scripts/init-premount ... done.
Begin: Mounting root file system ... Begin: Running /scripts/local-top ... done.
Begin: Running /scripts/local-premount ... done.
Begin: Will now check root file system ... fsck from util-linux 2.36.1
[/sbin/fsck.ext4 (1) -- /dev/nvme0n1p1] fsck.ext4 -a -C0 /dev/nvme0n1p1
/dev/nvme0n1p1: clean, 28520/15360000 files, 1174516/61433430 blocks
done.
[    7.866220] random: fast init done
[    7.881203] EXT4-fs (nvme0n1p1): mounted filesystem with ordered data mode. Opts: (null)
done.
Begin: Running /scripts/local-bottom ... GROWROOT: NOCHANGE: partition 1 is size 491467440. it cannot be grown
done.
Begin: Running /scripts/init-bottom ... done.
[    8.044059] Not activating Mandatory Access Control as /sbin/tomoyo-init does not exist.
[    8.100840] systemd[1]: System time before build time, advancing clock.
[    8.118406] systemd[1]: Inserted module 'autofs4'
[    8.185572] systemd[1]: systemd 247.3-5 running in system mode. (+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=unified)
[    8.208417] systemd[1]: Detected architecture arm64.

Welcome to Debian GNU/Linux 11 (bullseye)!

[    8.233861] systemd[1]: Set hostname to <debian>.
[    8.239571] random: systemd: uninitialized urandom read (16 bytes read)
[    8.246200] systemd[1]: Initializing machine ID from random generator.
[    8.252845] systemd[1]: Installed transient /etc/machine-id file.
[    8.579809] systemd[1]: Queued start job for default target Graphical Interface.
[    8.588085] random: systemd: uninitialized urandom read (16 bytes read)
[    8.596397] systemd[1]: Created slice system-getty.slice.
[  OK  ] Created slice system-getty.slice.
[    8.617297] random: systemd: uninitialized urandom read (16 bytes read)
[    8.624600] systemd[1]: Created slice system-modprobe.slice.
[  OK  ] Created slice system-modprobe.slice.
[    8.646063] systemd[1]: Created slice system-serial\x2dgetty.slice.
[  OK  ] Created slice system-serial\x2dgetty.slice.
[    8.669818] systemd[1]: Created slice system-systemd\x2dgrowfs.slice.
[  OK  ] Created slice system-systemd\x2dgrowfs.slice.
[    8.693742] systemd[1]: Created slice User and Session Slice.
[  OK  ] Created slice User and Session Slice.
[    8.713370] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Started Dispatch Password …ts to Console Directory Watch.
[    8.737319] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password R…uests to Wall Directory Watch.
[    8.761626] systemd[1]: Set up automount Arbitrary Executable File Formats File System Automount Point.
[  OK  ] Set up automount Arbitrary…s File System Automount Point.
[    8.789306] systemd[1]: Reached target Local Encrypted Volumes.
[  OK  ] Reached target Local Encrypted Volumes.
[    8.809290] systemd[1]: Reached target Paths.
[  OK  ] Reached target Paths.
[    8.829215] systemd[1]: Reached target Remote File Systems.
[  OK  ] Reached target Remote File Systems.
[    8.849204] systemd[1]: Reached target Slices.
[  OK  ] Reached target Slices.
[    8.869256] systemd[1]: Reached target System Time Set.
[  OK  ] Reached target System Time Set.
[    8.889506] systemd[1]: Listening on Syslog Socket.
[  OK  ] Listening on Syslog Socket.
[    8.909427] systemd[1]: Listening on fsck to fsckd communication Socket.
[  OK  ] Listening on fsck to fsckd communication Socket.
[    8.933322] systemd[1]: Listening on initctl Compatibility Named Pipe.
[  OK  ] Listening on initctl Compatibility Named Pipe.
[    8.957596] systemd[1]: Listening on Journal Audit Socket.
[  OK  ] Listening on Journal Audit Socket.
[    8.977425] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[    8.997506] systemd[1]: Listening on Journal Socket.
[  OK  ] Listening on Journal Socket.
[    9.017552] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[    9.037423] systemd[1]: Listening on udev Kernel Socket.
[  OK  ] Listening on udev Kernel Socket.
[    9.059332] systemd[1]: Mounting Huge Pages File System...
         Mounting Huge Pages File System...
[    9.079194] systemd[1]: Mounting POSIX Message Queue File System...
         Mounting POSIX Message Queue File System...
[    9.103220] systemd[1]: Mounting Kernel Debug File System...
         Mounting Kernel Debug File System...
[    9.123542] systemd[1]: Mounting Kernel Trace File System...
         Mounting Kernel Trace File System...
[    9.147646] systemd[1]: Starting Create list of static device nodes for the current kernel...
         Starting Create list of st…odes for the current kernel...
[    9.175354] systemd[1]: Starting Load Kernel Module configfs...
         Starting Load Kernel Module configfs...
[    9.199376] systemd[1]: Starting Load Kernel Module drm...
         Starting Load Kernel Module drm...
[    9.219387] systemd[1]: Starting Load Kernel Module fuse...
         Starting Load Kernel Module fuse...
[    9.239230] fuse: init (API version 7.32)
[    9.245378] systemd[1]: Started Nameserver information manager.
[  OK  ] Started Nameserver information manager.
[    9.266057] systemd[1]: Condition check resulted in Set Up Additional Binary Formats being skipped.
[    9.275235] systemd[1]: Condition check resulted in File System Check on Root Device being skipped.
[    9.288172] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[    9.312271] systemd[1]: Starting Load Kernel Modules...
         Starting Load Kernel Modules...
[    9.339480] systemd[1]: Starting Remount Root and Kernel File Systems...
         Starting Remount Root and Kernel File Systems...
[    9.363067] EXT4-fs (nvme0n1p1): re-mounted. Opts: discard,errors=remount-ro
[    9.372502] systemd[1]: Starting Coldplug All udev Devices...
         Starting Coldplug All udev Devices...
[    9.398086] systemd[1]: Mounted Huge Pages File System.
[  OK  ] Mounted Huge Pages File System.
[    9.417616] systemd[1]: Mounted POSIX Message Queue File System.
[  OK  ] Mounted POSIX Message Queue File System.
[    9.445601] systemd[1]: Mounted Kernel Debug File System.
[  OK  ] Mounted Kernel Debug File System.
[    9.469676] systemd[1]: Mounted Kernel Trace File System.
[  OK  ] Mounted Kernel Trace File System.
[    9.494305] systemd[1]: Finished Create list of static device nodes for the current kernel.
[  OK  ] Finished Create list of st… nodes for the current kernel.
[    9.526045] systemd[1]: modprobe@configfs.service: Succeeded.
[    9.532672] systemd[1]: Finished Load Kernel Module configfs.
[  OK  ] Finished Load Kernel Module configfs.
[    9.557528] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
[  OK  ] Finished Load Kernel Module drm.
[  OK  ] Finished Load Kernel Module fuse.
[  OK  ] Finished Load Kernel Modules.
[  OK  ] Finished Remount Root and Kernel File Systems.
         Mounting FUSE Control File System...
         Mounting Kernel Configuration File System...
         Starting Initial cloud-init job (pre-networking)...
         Starting Grow File System on /...
[    9.727215] EXT4-fs (nvme0n1p1): resizing filesystem from 61433430 to 61433430 blocks
         Starting Flush Journal to Persistent Storage...
[    9.750599] systemd-journald[304]: Received client request to flush runtime journal.
         Starting Load/Save Random Seed...
         Starting Apply Kernel Variables...
         Starting Create System Users...
[  OK  ] Mounted FUSE Control File System.
[  OK  ] Mounted Kernel Configuration File System.
[  OK  ] Finished Grow File System on /.
[  OK  ] Finished Coldplug All udev Devices.
[  OK  ] Finished Flush Journal to Persistent Storage.
[  OK  ] Finished Apply Kernel Variables.
[  OK  ] Finished Create System Users.
         Starting Helper to synchronize boot up for ifupdown...
         Starting Create Static Device Nodes in /dev...
[  OK  ] Finished Helper to synchronize boot up for ifupdown.
[  OK  ] Finished Create Static Device Nodes in /dev.
[  OK  ] Reached target Local File Systems (Pre).
         Starting Rule-based Manage…for Device Events and Files...
[  OK  ] Started Rule-based Manager for Device Events and Files.
[   10.163911] qoriq-cpufreq qoriq-cpufreq: Freescale QorIQ CPU frequency scaling driver
[   10.176243] optee: probing for conduit method.
[   10.180817] optee: api uid mismatch
[   10.185444] optee: probe of firmware:optee failed with error -22
[  OK  ] Found device /dev/ttyS0.
[   10.229074] caam 8000000.crypto: device ID = 0x0a13030000000000 (Era 8)
[   10.235734] caam 8000000.crypto: job rings = 3, qi = 1
[  OK  ] Found device SAMSUNG MZVL2256HCHQ-00B00 swap.
[   10.305614] dpaa2_caam dpseci.0: Adding to iommu group 0
[   10.313179] dpaa2_caam dpseci.0: dpseci v5.3
[   10.328743] sp805-wdt c000000.wdt: registration successful
[  OK     10.334699] sp805-wdt c010000.wdt: registration successful
0m] Found device[   10.341964] sp805-wdt c020000.wdt: registration successful
 SAMSUN[   10.348611] fsl_mc_dprc dprc.1 (unnamed net_device) (uninitialized): netif_napi_add() called with weight 512
G MZVL2256HCHQ-0[   10.348670] sp805-wdt c030000.wdt: registration successful
0B00 UEFI.
[   10.367667] sp805-wdt c100000.wdt: registration successful
[   10.373712] sp805-wdt c110000.wdt: registration successful
[   10.379731] sp805-wdt c120000.wdt: registration successful
[   10.386277] sp805-wdt c130000.wdt: registration successful
[   10.404024] cryptd: max_cpu_qlen set to 1000
         Activating swap /dev/disk/…2d93-415d-9ba1-35cc79eaa81c...
         Mounting /boot/efi...
[   10.493209] Adding 4194280k swap on /dev/nvme0n1p2.  Priority:-2 extents:1 across:4194280k SSFS
[  OK  ] Activated swap /dev/disk/b…8-2d93-415d-9ba1-35cc79eaa81c.
[  OK  ] Mounted /boot/efi.
[  OK  ] Reached target Local File Systems.
[  OK  ] Reached target Swap.
[   10.594498] dpaa2_caam dpseci.0: FD error: 000000a0
[   10.599448] dpaa2_caam dpseci.0: FD error: 000000a0
         Startin[   10.605121] dpaa2_caam dpseci.0: FD error: 000000a0
g Load [   10.610922] dpaa2_caam dpseci.0: FD error: 000000a0
AppArmor profile[   10.617166] dpaa2_caam dpseci.0: FD error: 000000a0
s...
[   10.623405] dpaa2_caam dpseci.0: FD error: 000000a0
[   10.629094] alg: No test for authenc(hmac(sha224),ecb(cipher_null)) (authenc-hmac-sha224-ecb-cipher_null-caam)
[   10.629144] dpaa2_caam dpseci.0: FD error: 000000a0
         Startin[   10.644195] alg: No test for authenc(hmac(sha256),ecb(cipher_null)) (authenc-hmac-sha256-ecb-cipher_null-caam)
g Creat[   10.650664] alg: No test for authenc(hmac(md5),cbc(aes)) (authenc-hmac-md5-cbc-aes-caam-qi2)
e Volatile Files[   10.655660] alg: No test for authenc(hmac(sha384),ecb(cipher_null)) (authenc-hmac-sha384-ecb-cipher_null-caam)
 and Directories[   10.665455] alg: No test for echainiv(authenc(hmac(md5),cbc(aes))) (echainiv-authenc-hmac-md5-cbc-aes-caam-qi2)
...
[   10.688448] alg: No test for authenc(hmac(sha512),ecb(cipher_null)) (authenc-hmac-sha512-ecb-cipher_null-caam)
[   10.692196] alg: No test for echainiv(authenc(hmac(sha1),cbc(aes))) (echainiv-authenc-hmac-sha1-cbc-aes-caam-qi2)
[   10.709497] alg: No test for authenc(hmac(md5),cbc(aes)) (authenc-hmac-md5-cbc-aes-caam)
[   10.717930] alg: No test for authenc(hmac(sha224),cbc(aes)) (authenc-hmac-sha224-cbc-aes-caam-qi2)
[   10.727327] alg: No test for echainiv(authenc(hmac(md5),cbc(aes))) (echainiv-authenc-hmac-md5-cbc-aes-caam)
[  OK     10.737624] alg: No test for echainiv(authenc(hmac(sha224),cbc(aes))) (echainiv-authenc-hmac-sha224-cbc-aes-caam-qi2)
0m] Finished Create Volatile Files and Directories.[   10.754530] alg: No test for echainiv(authenc(hmac(sha1),cbc(aes))) (echainiv-authenc-hmac-sha1-cbc-aes-caam)

[   10.757290] alg: No test for echainiv(authenc(hmac(sha256),cbc(aes))) (echainiv-authenc-hmac-sha256-cbc-aes-caam-qi2)
[   10.775667] alg: No test for authenc(hmac(sha384),cbc(aes)) (authenc-hmac-sha384-cbc-aes-caam-qi2)
         Startin[   10.786419] alg: No test for echainiv(authenc(hmac(sha384),cbc(aes))) (echainiv-authenc-hmac-sha384-cbc-aes-caam-qi2)
g Update UTMP about System Boot/Shutdow[   10.801567] alg: No test for authenc(hmac(sha224),cbc(aes)) (authenc-hmac-sha224-cbc-aes-caam)
n...
[   10.813320] alg: No test for echainiv(authenc(hmac(sha224),cbc(aes))) (echainiv-authenc-hmac-sha224-cbc-aes-caam)
[   10.829836] alg: No test for echainiv(authenc(hmac(sha256),cbc(aes))) (echainiv-authenc-hmac-sha256-cbc-aes-caam)
[   10.840406] alg: No test for echainiv(authenc(hmac(sha512),cbc(aes))) (echainiv-authenc-hmac-sha512-cbc-aes-caam-qi2)
[   10.840642] alg: No test for authenc(hmac(sha384),cbc(aes)) (authenc-hmac-sha384-cbc-aes-caam)
[   10.856745] audit: type=1400 audit(1618251686.747:2): apparmor="STATUS" operation="profile_load" profile="unconfined" name="nvidia_modprobe" pid=540 comm="apparmor_parser"
[   10.860695] alg: No test for authenc(hmac(md5),cbc(des3_ede)) (authenc-hmac-md5-cbc-des3_ede-caam-qi2)
[   10.875064] audit: type=1400 audit(1618251686.747:3): apparmor="STATUS" operation="profile_load" profile="unconfined" name="nvidia_modprobe//kmod" pid=540 comm="apparmor_parser"
[   10.875076] audit: type=1400 audit(1618251686.755:4): apparmor="STATUS" operation="profile_load" profile="unconfined" name="lsb_release" pid=537 comm="apparmor_parser"
[   10.884583] alg: No test for echainiv(authenc(hmac(md5),cbc(des3_ede))) (echainiv-authenc-hmac-md5-cbc-des3_ede-caam-qi2)
[   10.917166] alg: No test for echainiv(authenc(hmac(sha384),cbc(aes))) (echainiv-authenc-hmac-sha384-cbc-aes-caam)
[   10.927049] alg: No test for echainiv(authenc(hmac(sha1),cbc(des3_ede))) (echainiv-authenc-hmac-sha1-cbc-des3_ede-caam-qi2)
[   10.939891] alg: No test for echainiv(authenc(hmac(sha512),cbc(aes))) (echainiv-authenc-hmac-sha512-cbc-aes-caam)
[   10.948693] alg: No test for echainiv(authenc(hmac(sha224),cbc(des3_ede))) (echainiv-authenc-hmac-sha224-cbc-des3_ede-caam-qi2)
[   10.965218] audit: type=1400 audit(1618251686.859:5): apparmor="STATUS" operation="profile_load" profile="unconfined" name="/usr/bin/man" pid=538 comm="apparmor_parser"
[   10.970489] alg: No test for echainiv(authenc(hmac(sha256),cbc(des3_ede))) (echainiv-authenc-hmac-sha256-cbc-des3_ede-caam-qi2)
[   10.984771] alg: No test for authenc(hmac(md5),cbc(des3_ede)) (authenc-hmac-md5-cbc-des3_ede-caam)
[   10.984783] audit: type=1400 audit(1618251686.859:6): apparmor="STATUS" operation="profile_load" profile="unconfined" name="man_filter" pid=538 comm="apparmor_parser"
[   10.984793] audit: type=1400 audit(1618251686.863:7): apparmor="STATUS" operation="profile_load" profile="unconfined" name="man_groff" pid=538 comm="apparmor_parser"
[   10.997036] alg: No test for echainiv(authenc(hmac(sha384),cbc(des3_ede))) (echainiv-authenc-hmac-sha384-cbc-des3_ede-caam-qi2)
[   11.005320] alg: No test for echainiv(authenc(hmac(md5),cbc(des3_ede))) (echainiv-authenc-hmac-md5-cbc-des3_ede-caam)
[   11.020976] alg: No test for echainiv(authenc(hmac(sha512),cbc(des3_ede))) (echainiv-authenc-hmac-sha512-cbc-des3_ede-caam-qi2)
[   11.035710] alg: No test for echainiv(authenc(hmac(sha1),cbc(des3_ede))) (echainiv-authenc-hmac-sha1-cbc-des3_ede-caam)
[   11.040992] audit: type=1400 audit(1618251686.931:8): apparmor="STATUS" operation="profile_load" profile="unconfined" name="/usr/sbin/chronyd" pid=539 comm="apparmor_parser"
[   11.046466] alg: No test for authenc(hmac(md5),cbc(des)) (authenc-hmac-md5-cbc-des-caam-qi2)
[   11.057617] alg: No test for echainiv(authenc(hmac(sha224),cbc(des3_ede))) (echainiv-authenc-hmac-sha224-cbc-des3_ede-caam)
[   11.068539] alg: No test for echainiv(authenc(hmac(md5),cbc(des))) (echainiv-authenc-hmac-md5-cbc-des-caam-qi2)
[   11.079995] alg: No test for echainiv(authenc(hmac(sha256),cbc(des3_ede))) (echainiv-authenc-hmac-sha256-cbc-des3_ede-caam)
[   11.087597] audit: type=1400 audit(1618251686.983:9): apparmor="STATUS" operation="profile_load" profile="unconfined" name="tcpdump" pid=536 comm="apparmor_parser"
[   11.095577] alg: No test for echainiv(authenc(hmac(sha1),cbc(des))) (echainiv-authenc-hmac-sha1-cbc-des-caam-qi2)
[   11.103841] alg: No test for echainiv(authenc(hmac(sha384),cbc(des3_ede))) (echainiv-authenc-hmac-sha384-cbc-des3_ede-caam)
[   11.116252] alg: No test for echainiv(authenc(hmac(sha224),cbc(des))) (echainiv-authenc-hmac-sha224-cbc-des-caam-qi2)
[   11.125095] alg: No test for echainiv(authenc(hmac(sha512),cbc(des3_ede))) (echainiv-authenc-hmac-sha512-cbc-des3_ede-caam)
[   11.137336] alg: No test for echainiv(authenc(hmac(sha256),cbc(des))) (echainiv-authenc-hmac-sha256-cbc-des-caam-qi2)
[   11.150293] alg: No test for authenc(hmac(md5),cbc(des)) (authenc-hmac-md5-cbc-des-caam)
[   11.162441] alg: No test for echainiv(authenc(hmac(sha384),cbc(des))) (echainiv-authenc-hmac-sha384-cbc-des-caam-qi2)
[   11.171905] alg: No test for echainiv(authenc(hmac(md5),cbc(des))) (echainiv-authenc-hmac-md5-cbc-des-caam)
[   11.184465] alg: No test for echainiv(authenc(hmac(sha512),cbc(des))) (echainiv-authenc-hmac-sha512-cbc-des-caam-qi2)
[   11.194360] alg: No test for echainiv(authenc(hmac(sha1),cbc(des))) (echainiv-authenc-hmac-sha1-cbc-des-caam)
[   11.204360] alg: No test for authenc(hmac(md5),rfc3686(ctr(aes))) (authenc-hmac-md5-rfc3686-ctr-aes-caam-qi2)
[   11.212935] alg: No test for echainiv(authenc(hmac(sha224),cbc(des))) (echainiv-authenc-hmac-sha224-cbc-des-caam)
[   11.223123] alg: No test for seqiv(authenc(hmac(md5),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-md5-rfc3686-ctr-aes-caam-qi2)
[   11.233564] alg: No test for echainiv(authenc(hmac(sha256),cbc(des))) (echainiv-authenc-hmac-sha256-cbc-des-caam)
[   11.243622] alg: No test for seqiv(authenc(hmac(sha1),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha1-rfc3686-ctr-aes-caam-qi2)
[   11.254123] alg: No test for echainiv(authenc(hmac(sha384),cbc(des))) (echainiv-authenc-hmac-sha384-cbc-des-caam)
[   11.263433] alg: No test for authenc(hmac(sha224),rfc3686(ctr(aes))) (authenc-hmac-sha224-rfc3686-ctr-aes-caam-qi2)
[   11.274351] alg: No test for echainiv(authenc(hmac(sha512),cbc(des))) (echainiv-authenc-hmac-sha512-cbc-des-caam)
[   11.284901] alg: No test for seqiv(authenc(hmac(sha224),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha224-rfc3686-ctr-aes-caam-qi2)
[   11.295046] alg: No test for authenc(hmac(md5),rfc3686(ctr(aes))) (authenc-hmac-md5-rfc3686-ctr-aes-caam)
[   11.306638] alg: No test for seqiv(authenc(hmac(sha256),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha256-rfc3686-ctr-aes-caam-qi2)
[   11.316497] alg: No test for seqiv(authenc(hmac(md5),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-md5-rfc3686-ctr-aes-caam)
[   11.327026] alg: No test for seqiv(authenc(hmac(sha384),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha384-rfc3686-ctr-aes-caam-qi2)
[   11.337421] alg: No test for seqiv(authenc(hmac(sha1),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha1-rfc3686-ctr-aes-caam)
[   11.349007] alg: No test for seqiv(authenc(hmac(sha512),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha512-rfc3686-ctr-aes-caam-qi2)
[   11.358693] alg: No test for authenc(hmac(sha224),rfc3686(ctr(aes))) (authenc-hmac-sha224-rfc3686-ctr-aes-caam)
[   11.370036] dpaa2_caam dpseci.0: algorithms registered in /proc/crypto
[   11.380988] alg: No test for seqiv(authenc(hmac(sha224),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha224-rfc3686-ctr-aes-caam)
[  OK     11.443004] alg: No test for seqiv(authenc(hmac(sha256),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha256-rfc3686-ctr-aes-caam)
0m] Finished    11.455676] alg: No test for seqiv(authenc(hmac(sha384),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha384-rfc3686-ctr-aes-caam)
;1;39mUpdate UTM[   11.468281] alg: No test for seqiv(authenc(hmac(sha512),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha512-rfc3686-ctr-aes-caam)
P about System B[   11.480592] caam algorithms registered in /proc/crypto
oot/Shutdown.
[   10.996345] cloud-init[606]: Cloud-init v. 20.4.1 running 'init-local' at Mon, 12 Apr 2021 18:21:26 +0000. Up 10.90 seconds.
[  OK  ] Finished Load AppArmor profiles.
[   11.598417] dpaa2_caam dpseci.0: hash algorithms registered in /proc/crypto
[  OK  ] Finished Initial cloud-init job (pre-networking).
[  OK  ] Reached target Network (Pre).
         Starting Raise network interfaces...
[   11.737177] random: crng init done
[   11.740583] random: 7 urandom warning(s) missed due to ratelimiting
[  OK  ] Finished Load/Save Random Seed.
[   11.779075] caam 8000000.crypto: caam pkc algorithms registered in /proc/crypto
[   11.786520] caam 8000000.crypto: registering rng-caam
         Starting Commit a transient machine-id on disk...
[   11.819734] fsl_dpaa2_eth dpni.3 eth6: configuring for inband/qsgmii link mode
[  OK  ] Finished Commit a transient machine-id on disk.
[**    ] A start job is running for Raise network interfaces (6s / 5min 3s)
[   14.891142] fsl_dpaa2_eth dpni.3 eth6: Link is Up - 1Gbps/Full - flow control rx/tx
[  OK  ] Finished Raise network interfaces.
[  OK  ] Reached target Network.
         Starting Initial cloud-ini… (metadata service crawler)...
[   31.232856] cloud-init[870]: Cloud-init v. 20.4.1 running 'init' at Mon, 12 Apr 2021 18:21:47 +0000. Up 31.14 seconds.
[   31.279238] cloud-init[870]: ci-info: +++++++++++++++++++++++++++++++++++++++Net device info+++++++++++++++++++++++++++++++++++++++
[   31.300623] cloud-init[870]: ci-info: +--------+-------+-----------------------------+---------------+--------+-------------------+
[   31.320469] cloud-init[870]: ci-info: | Device |   Up  |           Address           |      Mask     | Scope  |     Hw-Address    |
[   31.340516] cloud-init[870]: ci-info: +--------+-------+-----------------------------+---------------+--------+-------------------+
[   31.360472] cloud-init[870]: ci-info: |  eth0  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:95 |
[   31.380598] cloud-init[870]: ci-info: |  eth1  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:96 |
[   31.400515] cloud-init[870]: ci-info: |  eth2  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:97 |
[   31.420517] cloud-init[870]: ci-info: |  eth3  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:98 |
[   31.440516] cloud-init[870]: ci-info: |  eth4  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:99 |
[   31.460538] cloud-init[870]: ci-info: |  eth5  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:9a |
[   31.480552] cloud-init[870]: ci-info: |  eth6  |  True |        192.168.0.104        | 255.255.255.0 | global | 00:0a:fa:24:2d:9b |
[   31.500542] cloud-init[870]: ci-info: |  eth6  |  True | fe80::20a:faff:fe24:2d9b/64 |       .       |  link  | 00:0a:fa:24:2d:9b |
[   31.520568] cloud-init[870]: ci-info: |  eth7  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:9c |
[   31.540538] cloud-init[870]: ci-info: |  eth8  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:9e |
[   31.560543] cloud-init[870]: ci-info: |  eth9  | False |              .              |       .       |   .    | 00:0a:fa:24:2d:9d |
[   31.580568] cloud-init[870]: ci-info: |   lo   |  True |          127.0.0.1          |   255.0.0.0   |  host  |         .         |
[   31.604468] cloud-init[870]: ci-info: |   lo   |  True |           ::1/128           |       .       |  host  |         .         |
[   31.624541] cloud-init[870]: ci-info: +--------+-------+-----------------------------+---------------+--------+-------------------+
[   31.644465] cloud-init[870]: ci-info: +++++++++++++++++++++++++++++Route IPv4 info+++++++++++++++++++++++++++++
[   31.664303] cloud-init[870]: ci-info: +-------+-------------+-------------+---------------+-----------+-------+
[   31.688593] cloud-init[870]: ci-info: | Route | Destination |   Gateway   |    Genmask    | Interface | Flags |
[   31.708489] cloud-init[870]: ci-info: +-------+-------------+-------------+---------------+-----------+-------+
[   31.728531] cloud-init[870]: ci-info: |   0   |   0.0.0.0   | 192.168.0.1 |    0.0.0.0    |    eth6   |   UG  |
[   31.748551] cloud-init[870]: ci-info: |   1   | 192.168.0.0 |   0.0.0.0   | 255.255.255.0 |    eth6   |   U   |
[   31.768534] cloud-init[870]: ci-info: +-------+-------------+-------------+---------------+-----------+-------+
[   31.788487] cloud-init[870]: ci-info: +++++++++++++++++++Route IPv6 info+++++++++++++++++++
[   31.804584] cloud-init[870]: ci-info: +-------+-------------+---------+-----------+-------+
[   31.820571] cloud-init[870]: ci-info: | Route | Destination | Gateway | Interface | Flags |
[   31.836485] cloud-init[870]: ci-info: +-------+-------------+---------+-----------+-------+
[   31.852636] cloud-init[870]: ci-info: |   1   |  fe80::/64  |    ::   |    eth6   |   U   |
[   31.868554] cloud-init[870]: ci-info: |   3   |    local    |    ::   |    eth6   |   U   |
[   31.884296] cloud-init[870]: ci-info: |   4   |  multicast  |    ::   |    eth6   |   U   |
[   31.900279] cloud-init[870]: ci-info: +-------+-------------+---------+-----------+-------+
[   36.736714] cloud-init[870]: Generating public/private rsa key pair.
[   36.752619] cloud-init[870]: Your identification has been saved in /etc/ssh/ssh_host_rsa_key
[   36.776457] cloud-init[870]: Your public key has been saved in /etc/ssh/ssh_host_rsa_key.pub
[   36.800535] cloud-init[870]: The key fingerprint is:
[   36.820533] cloud-init[870]: SHA256:SBl0GdfHGjAPHphfo107ZADZeKfs51mkQQc3qnay+KU root@ten64-sid
[   36.840554] cloud-init[870]: The key's randomart image is:
[   36.860503] cloud-init[870]: +---[RSA 3072]----+
[   36.880318] cloud-init[870]: |     .o o=BB.o.o.|
[   36.896301] cloud-init[870]: |       ++oo=B X.o|
[   36.916345] cloud-init[870]: |      o  ..*.& o |
[  OK  ] Finished Initial cloud-ini…ob (metadata service crawler).
[   36.932724] cloud-init[870]: |     . .  o * + .|
[  OK  ] Reached target Cloud-config availability.
[   36.964676] cloud-init[870]: |      . S  = . = |
[  OK  ] Reached target Network is Online.
[   36.996612] cloud-init[870]: |          o = o .|
[  OK  ] Reached target System Initialization.
[   37.028493] cloud-init[870]: |         . . + o |
[  OK  ] Started Daily Cleanup of Temporary Directories.
[   37.060569] cloud-init[870]: |          . o o  |
[  OK  ] Listening on D-Bus System Message Bus Socket.
[   37.092605] cloud-init[870]: |           E     |
[  OK  ] Listening on UUID daemon activation socket.
[   37.124497] cloud-init[870]: +----[SHA256]-----+
[  OK  ] Reached target Sockets.
[   37.156559] cloud-init[870]: Generating public/private dsa key pair.
[  OK  ] Reached target Basic System.
[   37.188597] cloud-init[870]: Your identification has been saved in /etc/ssh/ssh_host_dsa_key
         Starting chrony, an NTP client/server...
[   37.224320] cloud-init[870]: Your public key has been saved in /etc/ssh/ssh_host_dsa_key.pub
         Starting Apply the settings specified in cloud-config...
[   37.260354] cloud-init[870]: The key fingerprint is:
[  OK  ] Started Regular background program processing daemon.
[   37.292319] cloud-init[870]: SHA256:2oh7YmtiOtYPXp71mG0jHz/vNxAokK6rdAsd4I/pEC0 root@ten64-sid
[  OK  ] Started D-Bus System Message Bus.
[   37.332364] cloud-init[870]: The key's randomart image is:
         Starting Remove Stale Onli…t4 Metadata Check Snapshots...
[   37.364293] cloud-init[870]: +---[DSA 1024]----+
         Starting System Logging Service...
[   37.396375] cloud-init[870]: |        .        |
         Starting OpenBSD Secure Shell server...
[   37.428436] cloud-init[870]: |       o         |
         Starting User Login Management...
[   37.460345] cloud-init[870]: |   .  . .   .    |
         Starting Permit User Sessions...
[   37.492369] cloud-init[870]: |  o .  . . . .   |
[  OK  ] Started chrony, an NTP client/server.
[   37.528286] cloud-init[870]: | E o .. S .   .  |
[  OK  ] Finished Remove Stale Onli…ext4 Metadata Check Snapshots.
[   37.560331] cloud-init[870]: |  o =o.+     .   |
[  OK  ] Finished Permit User Sessions.
[   37.592394] cloud-init[870]: | ..*.=+...    .  |
[  OK  ] Reached target System Time Synchronized.
[   37.628366] cloud-init[870]: |..B.Xo=.=oo.   o |
[  OK  ] Started Daily apt download activities.
[   37.660332] cloud-init[870]: |o+ B=O o++..+o. .|
[  OK  ] Started Daily apt upgrade and clean activities.
[   37.692315] cloud-init[870]: +----[SHA256]-----+
[  OK  ] Started Periodic ext4 Onli…ata Check for All Filesystems.
[   37.728315] cloud-init[870]: Generating public/private ecdsa key pair.
[  OK  ] Started Discard unused blocks once a week.
[   37.760596] cloud-init[870]: Your identification has been saved in /etc/ssh/ssh_host_ecdsa_key
[  OK  ] Started Daily rotation of log files.
[   37.796361] cloud-init[870]: Your public key has been saved in /etc/ssh/ssh_host_ecdsa_key.pub
[  OK  ] Started Daily man-db regeneration.
[   37.836358] cloud-init[870]: The key fingerprint is:
[  OK  ] Reached target Timers.
[   37.868322] cloud-init[870]: SHA256:qf9P6ngy0C4/ovLsoC/A3SAtv3T0cQINiBtgPGD4d1M root@ten64-sid
[  OK  ] Started Getty on tty1.
[   37.908303] cloud-init[870]: The key's randomart image is:
[  OK  ] Started Serial Getty on ttyS0.
[   37.932332] cloud-init[870]: +---[ECDSA 256]---+
[  OK  ] Reached target Login Prompts.
[   37.956386] cloud-init[870]: |Bo. ..o          |
[  OK  ] Started OpenBSD Secure Shell server.
[   37.988351] cloud-init[870]: |+= . . .E        |
[  OK  ] Started User Login Management.
[   38.020355] cloud-init[870]: | .=   ..         |
[  OK  ] Started Unattended Upgrades Shutdown.
[   38.056326] cloud-init[870]: | +.o..oo o       |
[  OK  ] Started System Logging Service.
[   38.096390] cloud-init[870]: |. =.+..oS        |
[  OK  ] Reached target Multi-User System.
[   38.128416] cloud-init[870]: |.. + o.o.        |
[  OK  ] Reached target Graphical Interface.
[   38.164395] cloud-init[870]: |. o o .o    .    |
         Starting Update UTMP about System Runlevel Changes...
[   38.204393] cloud-init[870]: |...+  o.=..o     |
[  OK  ] Finished Update UTMP about System Runlevel Changes.
[   38.240420] cloud-init[870]: |.o.+=. ++B+..    |
[   38.276365] cloud-init[870]: +----[SHA256]-----+
[   38.296315] cloud-init[870]: Generating public/private ed25519 key pair.
[   38.316326] cloud-init[870]: Your identification has been saved in /etc/ssh/ssh_host_ed25519_key
[   38.340249] cloud-init[870]: Your public key has been saved in /etc/ssh/ssh_host_ed25519_key.pub
[   38.360275] cloud-init[870]: The key fingerprint is:
[   38.380345] cloud-init[870]: SHA256:/vPpWzkDJtf1TPG1TsZuhZ0Aj3CAaULHl8+rpZxj0Yg root@ten64-sid
[   38.404816] cloud-init[870]: The key's randomart image is:
[   38.420523] cloud-init[870]: +--[ED25519 256]--+
[   38.436605] cloud-init[870]: |    ....oooo.. ..|
[   38.452561] cloud-init[870]: |     ..= oo o oo*|
[   38.468572] cloud-init[870]: |      o . o. ..**|
[   38.484624] cloud-init[870]: |           o .=+o|
[   38.500374] cloud-init[870]: |        S + = .+o|
[   38.520649] cloud-init[870]: |       E o B ... |
[   38.536267] cloud-init[870]: |        o *   =  |
[   38.552467] cloud-init[870]: |         O.  o o |
[   38.568565] cloud-init[870]: |        . o+=.   |
[   38.584564] cloud-init[870]: +----[SHA256]-----+
[   38.620861] cloud-init[949]: Cloud-init v. 20.4.1 running 'modules:config' at Mon, 12 Apr 2021 18:21:54 +0000. Up 38.30 seconds.
[   38.646985] cloud-init[949]: Generating locales (this might take a while)...
         Starting Online ext4 Metad…a Check for All Filesystems...
         Starting Rotate log files...
         Starting Daily man-db regeneration...
[  OK  ] Finished Online ext4 Metadata Check for All Filesystems.

Debian GNU/Linux 11 ten64-sid ttyS0

ten64-sid login: [   49.381443] cloud-init[949]:   de_DE.UTF-8... done
[   51.562274] cloud-init[949]:   de_DE.ISO-8859-1... done
[   51.563075] cloud-init[949]: Generation complete.
[   52.054629] device-mapper: uevent: version 1.0.3
[   52.059458] device-mapper: ioctl: 4.43.0-ioctl (2020-10-01) initialised: dm-devel@redhat.com
[   53.892022] cloud-init[1059]: Cloud-init v. 20.4.1 running 'modules:final' at Tue, 22 Jun 2021 07:03:31 +0000. Up 53.68 seconds.
ci-info: no authorized SSH keys fingerprints found for user debian.
[   53.973248] cloud-init[1059]: ci-info: no authorized SSH keys fingerprints found for user debian.
<14>Jun 22 07:03:31 ec2:
<14>Jun 22 07:03:31 ec2: #############################################################
<14>Jun 22 07:03:31 ec2: -----BEGIN SSH HOST KEY FINGERPRINTS-----
<14>Jun 22 07:03:31 ec2: 1024 SHA256:2oh7YmtiOtYPXp71mG0jHz/vNxAokK6rdAsd4I/pEC0 root@ten64-sid (DSA)
<14>Jun 22 07:03:31 ec2: 256 SHA256:qf9P6ngy0C4/ovLsoC/A3SAtv3T0cQINiBtgPGD4d1M root@ten64-sid (ECDSA)
<14>Jun 22 07:03:31 ec2: 256 SHA256:/vPpWzkDJtf1TPG1TsZuhZ0Aj3CAaULHl8+rpZxj0Yg root@ten64-sid (ED25519)
<14>Jun 22 07:03:31 ec2: 3072 SHA256:SBl0GdfHGjAPHphfo107ZADZeKfs51mkQQc3qnay+KU root@ten64-sid (RSA)
<14>Jun 22 07:03:31 ec2: -----END SSH HOST KEY FINGERPRINTS-----
<14>Jun 22 07:03:31 ec2: #############################################################
-----BEGIN SSH HOST KEY KEYS-----
ecdsa-sha2-nistp256 [snip] root@ten64-sid
-----END SSH HOST KEY KEYS-----
[   54.064144] cloud-init[1059]: Cloud-init v. 20.4.1 finished at Tue, 22 Jun 2021 07:03:31 +0000. Datasource DataSourceNoCloud [seed=/dev/nvme0n1p15][dsmode=net].  Up 54.04 seconds

ten64-sid login:
```

## Firmware v0.8.5 + openSUSE Leap 15.3
This release only works in [legacy network mode](/network/managementmodes.md) so
there will be some differences in enumeration between the other logs on this page.

* By default, openSUSE will boot in 'silent' mode - these logs have been
captured with this disabled in `/etc/default/grub`

```
crc32+  fsl-mc: ten64: setting SFP to legacy (unmanaged) mode
        fsl-mc: ten64: setting GBE ports to legacy (unmanaged) mode

fsl-mc: Booting Management Complex ... SUCCESS
fsl-mc: Management Complex booted (version: 10.20.4, boot status: 0x1)
Hit any key to stop autoboot:  0

  *** U-Boot Boot Menu ***

     Normal boot
     Built-in recovery
     Boot from NVMe
     Boot from USB
     Boot from SD
     Boot OpenWrt from NAND
     Boot from network
     Boot from SATA
     U-Boot console


  Press UP/DOWN to move, ENTER to select

Device 0: Vendor: 0x144d Rev: GXA7301Q Prod: S674NE0NC02476
            Type: Hard Disk
            Capacity: 244198.3 MB = 238.4 GB (500118192 x 512)
... is now current device
Scanning nvme 0:1...
** Unable to read file / **
Found EFI removable media binary efi/boot/bootaa64.efi
device 0 offset 0x580000, size 0x40000
SF: 262144 bytes @ 0x580000 Read: OK
device 0 offset 0x600000, size 0x40000
SF: 262144 bytes @ 0x600000 Read: OK
TEN64 ft_board_setup start, blob 0000000090000000
INFO:    RNG Desc SUCCESS with status 0
INFO:    result 71171a86d57fc4b2
TEN64 ft_board_setup end
Card did not respond to voltage select!
mmc_init: -95, time 18
Scanning disk esdhc@2140000.blk...
Disk esdhc@2140000.blk not ready
Scanning disk nvme#0.blk#0...
** Unrecognized filesystem type **
** Unrecognized filesystem type **
Found 4 disks
TEN64 ft_board_setup start, blob 0000000087f00000
TEN64 ft_board_setup already run, not doing anything
BootOrder not defined
EFI boot manager: Cannot load any image
1439744 bytes read in 2 ms (686.5 MiB/s)
TEN64 ft_board_setup start, blob 0000000087ee7000
TEN64 ft_board_setup already run, not doing anything
Welcome to GRUB!

DPMAC7@qsgmii Waiting for PHY auto negotiation to complete.......user interrupt!
DPMAC7@qsgmii: Could not initialize
DPMAC8@qsgmii Waiting for PHY auto negotiation to complete.....user interrupt!
DPMAC8@qsgmii: Could not initialize
DPMAC9@qsgmii Waiting for PHY auto negotiation to complete.user interrupt!
DPMAC9@qsgmii: Could not initialize
DPMAC10@qsgmii Waiting for PHY auto negotiation to complete.user interrupt!
DPMAC10@qsgmii: Could not initialize
Please press 't' to show the boot menu on this console
error: ../../grub-core/video/video.c:762:no suitable video mode found.

                             GNU GRUB  version 2.04

 ┌────────────────────────────────────────────────────────────────────────────┐
 │*openSUSE Leap 15.3                                                         │
 │ Advanced options for openSUSE Leap 15.3                                    │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 │                                                                            │
 └────────────────────────────────────────────────────────────────────────────┘

      Use the ▲ and ▼ keys to select which entry is highlighted.
      Press enter to boot the selected OS, `e' to edit the commands
      before booting or `c' for a command-line. ESC to return previous
      menu.
Loading Linux 5.3.18-57-default ...
Loading initial ramdisk ...
EFI stub: Booting Linux Kernel...
EFI stub: EFI_RNG_PROTOCOL unavailable, KASLR will be disabled
EFI stub: ERROR: Could not determine UEFI Secure Boot status.
EFI stub: Using DTB from configuration table
EFI stub: Exiting boot services and installing virtual address map...
fsl-mc: Deploying data path layout ... SUCCESS
[    0.000000] Booting Linux on physical CPU 0x0000000000 [0x410fd034]
[    0.000000] Linux version 5.3.18-57-default (geeko@buildhost) (gcc version 7.5.0 (SUSE Linux)) #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9)
[    0.000000] Machine model: Traverse Ten64
[    0.000000] efi: EFI v2.80 by Das U-Boot
[    0.000000] efi: RTPROP=0x83dfff7040 SMBIOS=0xf69c6000 MEMRESERVE=0x83dfe85040
[    0.000000] NUMA: No NUMA configuration found
[    0.000000] NUMA: Faking a node at [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000] NUMA: NODE_DATA [mem 0x83de0dd300-0x83de0e0fff]
[    0.000000] Zone ranges:
[    0.000000]   DMA      [mem 0x0000000080000000-0x00000000ffffffff]
[    0.000000]   DMA32    empty
[    0.000000]   Normal   [mem 0x0000000100000000-0x00000083dfffffff]
[    0.000000] Movable zone start for each node
[    0.000000] Early memory node ranges
[    0.000000]   node   0: [mem 0x0000000080000000-0x00000000f69c5fff]
[    0.000000]   node   0: [mem 0x00000000f69c6000-0x00000000f69c6fff]
[    0.000000]   node   0: [mem 0x00000000f69c7000-0x00000000fbcbffff]
[    0.000000]   node   0: [mem 0x00000000fbcc0000-0x00000000fbccffff]
[    0.000000]   node   0: [mem 0x00000000fbcd0000-0x00000000fbdfffff]
[    0.000000]   node   0: [mem 0x0000008080000000-0x00000083dffeafff]
[    0.000000]   node   0: [mem 0x00000083dffeb000-0x00000083dffeffff]
[    0.000000]   node   0: [mem 0x00000083dfff0000-0x00000083dfff2fff]
[    0.000000]   node   0: [mem 0x00000083dfff3000-0x00000083dfff3fff]
[    0.000000]   node   0: [mem 0x00000083dfff4000-0x00000083dfff6fff]
[    0.000000]   node   0: [mem 0x00000083dfff7000-0x00000083dfffffff]
[    0.000000] Zeroed struct page in unavailable ranges: 32 pages
[    0.000000] Initmem setup node 0 [mem 0x0000000080000000-0x00000083dfffffff]
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] psci: SMC Calling Convention v1.1
[    0.000000] percpu: Embedded 32 pages/cpu s93592 r8192 d29288 u131072
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: detected: ARM erratum 845719
[    0.000000] CPU features: detected: GIC system register CPU interface
[    0.000000] CPU features: kernel page table isolation forced ON by KASLR
[    0.000000] CPU features: detected: Kernel page table isolation (KPTI)
[    0.000000] Speculative Store Bypass Disable mitigation not required
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 3982848
[    0.000000] Policy zone: Normal
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/Image-5.3.18-57-default root=UUID=1e4c6eed-b95c-4d4b-aeb8-45fd94a1db83 plymouth.enable=0
[    0.000000] Dentry cache hash table entries: 2097152 (order: 12, 16777216 bytes, linear)
[    0.000000] Inode-cache hash table entries: 1048576 (order: 11, 8388608 bytes, linear)
[    0.000000] mem auto-init: stack:off, heap alloc:off, heap free:off
[    0.000000] software IO TLB: mapped [mem 0xf7cc0000-0xfbcc0000] (64MB)
[    0.000000] Memory: 2980540K/16185344K available (11456K kernel code, 2082K rwdata, 5544K rodata, 6400K init, 9645K bss, 454192K reserved, 0K cma-reserved)
[    0.000000] random: get_random_u64 called from __kmem_cache_create+0x50/0x5f8 with crng_init=0
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=8, Nodes=1
[    0.000000] ftrace: allocating 40173 entries in 157 pages
[    0.000000] rcu: Hierarchical RCU implementation.
[    0.000000] rcu:     RCU event tracing is enabled.
[    0.000000] rcu:     RCU restricting CPUs from NR_CPUS=768 to nr_cpu_ids=8.
[    0.000000]  Tasks RCU enabled.
[    0.000000] rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
[    0.000000] rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=8
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GICv3: GIC: Using split EOI/Deactivate mode
[    0.000000] GICv3: 256 SPIs implemented
[    0.000000] GICv3: 0 Extended SPIs implemented
[    0.000000] GICv3: Distributor has no Range Selector support
[    0.000000] GICv3: 16 PPIs implemented
[    0.000000] GICv3: CPU0: found redistributor 0 region 0:0x0000000006100000
[    0.000000] ITS [mem 0x06020000-0x0603ffff]
[    0.000000] ITS@0x0000000006020000: allocated 65536 Devices @80bfd80000 (flat, esz 8, psz 64K, shr 0)
[    0.000000] ITS: using cache flushing for cmd queue
[    0.000000] GICv3: using LPI property table @0x00000080bfd70000
[    0.000000] GIC: using cache flushing for LPI property table
[    0.000000] GICv3: CPU0: using allocated LPI pending table @0x00000080bfe10000
[    0.000000] arch_timer: cp15 timer(s) running at 25.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x5c409fb33, max_idle_ns: 440795203156 ns
[    0.000002] sched_clock: 56 bits at 25MHz, resolution 39ns, wraps every 4398046511103ns
[    0.000425] Console: colour dummy device 80x25
[    0.000780] printk: console [tty0] enabled
[    0.000838] Calibrating delay loop (skipped), value calculated using timer frequency.. 50.00 BogoMIPS (lpj=250000)
[    0.000851] pid_max: default: 32768 minimum: 301
[    0.000941] LSM: Security Framework initializing
[    0.001007] AppArmor: AppArmor initialized
[    0.001129] Mount-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.001175] Mountpoint-cache hash table entries: 32768 (order: 6, 262144 bytes, linear)
[    0.002126] ASID allocator initialised with 32768 entries
[    0.002183] rcu: Hierarchical SRCU implementation.
[    0.002942] Platform MSI: gic-its@6020000 domain created
[    0.003011] PCI/MSI: /interrupt-controller@6000000/gic-its@6020000 domain created
[    0.003061] fsl-mc MSI: gic-its@6020000 domain created
[    0.003815] Remapping and enabling EFI services.
[    0.004100] smp: Bringing up secondary CPUs ...
[    0.004422] Detected VIPT I-cache on CPU1
[    0.004440] GICv3: CPU1: found redistributor 1 region 0:0x0000000006120000
[    0.004465] GICv3: CPU1: using allocated LPI pending table @0x00000080bfe20000
[    0.004490] CPU1: Booted secondary processor 0x0000000001 [0x410fd034]
[    0.004814] Detected VIPT I-cache on CPU2
[    0.004827] GICv3: CPU2: found redistributor 2 region 0:0x0000000006140000
[    0.004847] GICv3: CPU2: using allocated LPI pending table @0x00000080bfe30000
[    0.004861] CPU2: Booted secondary processor 0x0000000002 [0x410fd034]
[    0.005179] Detected VIPT I-cache on CPU3
[    0.005192] GICv3: CPU3: found redistributor 3 region 0:0x0000000006160000
[    0.005213] GICv3: CPU3: using allocated LPI pending table @0x00000080bfe40000
[    0.005227] CPU3: Booted secondary processor 0x0000000003 [0x410fd034]
[    0.005545] Detected VIPT I-cache on CPU4
[    0.005564] GICv3: CPU4: found redistributor 100 region 0:0x0000000006180000
[    0.005597] GICv3: CPU4: using allocated LPI pending table @0x00000080bfe50000
[    0.005620] CPU4: Booted secondary processor 0x0000000100 [0x410fd034]
[    0.005980] Detected VIPT I-cache on CPU5
[    0.005995] GICv3: CPU5: found redistributor 101 region 0:0x00000000061a0000
[    0.006020] GICv3: CPU5: using allocated LPI pending table @0x00000080bfe60000
[    0.006035] CPU5: Booted secondary processor 0x0000000101 [0x410fd034]
[    0.006364] Detected VIPT I-cache on CPU6
[    0.006379] GICv3: CPU6: found redistributor 102 region 0:0x00000000061c0000
[    0.006403] GICv3: CPU6: using allocated LPI pending table @0x00000080bfe70000
[    0.006418] CPU6: Booted secondary processor 0x0000000102 [0x410fd034]
[    0.006752] Detected VIPT I-cache on CPU7
[    0.006768] GICv3: CPU7: found redistributor 103 region 0:0x00000000061e0000
[    0.006790] GICv3: CPU7: using allocated LPI pending table @0x00000080bfe80000
[    0.006804] CPU7: Booted secondary processor 0x0000000103 [0x410fd034]
[    0.006867] smp: Brought up 1 node, 8 CPUs
[    0.007017] SMP: Total of 8 processors activated.
[    0.007024] CPU features: detected: 32-bit EL0 Support
[    0.007032] CPU features: detected: CRC32 instructions
[    0.007891] CPU: All CPU(s) started at EL2
[    0.007918] alternatives: patching kernel code
[    0.433642] node 0 initialised, 3187653 pages in 430ms
[    0.434363] devtmpfs: initialized
[    0.437347] Registered cp15_barrier emulation handler
[    0.437360] Registered setend emulation handler
[    0.437371] KASLR enabled
[    0.437576] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
[    0.437594] futex hash table entries: 2048 (order: 5, 131072 bytes, linear)
[    0.437793] pinctrl core: initialized pinctrl subsystem
[    0.438397] SMBIOS 3.0 present.
[    0.438412] DMI: traverse ten64/ten64, BIOS 2020.07-rc1-gb47b96d4 06/22/2021
[    0.438766] NET: Registered protocol family 16
[    0.439311] DMA: preallocated 2048 KiB GFP_KERNEL pool for atomic allocations
[    0.439648] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA pool for atomic allocations
[    0.439983] DMA: preallocated 2048 KiB GFP_KERNEL|GFP_DMA32 pool for atomic allocations
[    0.440034] audit: initializing netlink subsys (disabled)
[    0.440146] audit: type=2000 audit(0.440:1): state=initialized audit_enabled=0 res=1
[    0.440782] cpuidle: using governor ladder
[    0.440857] cpuidle: using governor menu
[    0.440927] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    0.441605] Serial: AMBA PL011 UART driver
[    0.442577] Machine: Traverse Ten64
[    0.442583] SoC family: QorIQ LS1088A
[    0.442588] SoC ID: svr:0x87030010, Revision: 1.0
[    0.449545] fsl_mc_bus 80c000000.fsl-mc: MC firmware version: 10.20.4
[    0.452106] fsl_mc_dprc dprc.1: DMA mask not set
[    0.456155] HugeTLB registered 1.00 GiB page size, pre-allocated 0 pages
[    0.456164] HugeTLB registered 32.0 MiB page size, pre-allocated 0 pages
[    0.456173] HugeTLB registered 2.00 MiB page size, pre-allocated 0 pages
[    0.456181] HugeTLB registered 64.0 KiB page size, pre-allocated 0 pages
[    0.883121] alg: No test for lzo-rle (lzo-rle-generic)
[    0.883194] alg: No test for lzo-rle (lzo-rle-scomp)
[    0.892519] ACPI: Interpreter disabled.
[    0.892954] iommu: Default domain type: Passthrough
[    0.893102] vgaarb: loaded
[    0.893342] pps_core: LinuxPPS API ver. 1 registered
[    0.893349] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    0.893365] PTP clock support registered
[    0.893426] EDAC MC: Ver: 3.0.0
[    0.893675] ima: secureboot mode unknown, no efi
[    0.894124] NetLabel: Initializing
[    0.894131] NetLabel:  domain hash size = 128
[    0.894137] NetLabel:  protocols = UNLABELED CIPSOv4 CALIPSO
[    0.894175] NetLabel:  unlabeled traffic allowed by default
[    0.894359] clocksource: Switched to clocksource arch_sys_counter
[    0.935849] VFS: Disk quotas dquot_6.6.0
[    0.935895] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    0.936227] AppArmor: AppArmor Filesystem Enabled
[    0.936261] pnp: PnP ACPI: disabled
[    0.939954] thermal_sys: Registered thermal governor 'fair_share'
[    0.939956] thermal_sys: Registered thermal governor 'bang_bang'
[    0.939964] thermal_sys: Registered thermal governor 'step_wise'
[    0.939972] thermal_sys: Registered thermal governor 'user_space'
[    0.940252] NET: Registered protocol family 2
[    0.940581] tcp_listen_portaddr_hash hash table entries: 8192 (order: 5, 131072 bytes, linear)
[    0.940701] TCP established hash table entries: 131072 (order: 8, 1048576 bytes, linear)
[    0.941302] TCP bind hash table entries: 65536 (order: 8, 1048576 bytes, linear)
[    0.941882] TCP: Hash tables configured (established 131072 bind 65536)
[    0.941962] UDP hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    0.942171] UDP-Lite hash table entries: 8192 (order: 6, 262144 bytes, linear)
[    0.942478] NET: Registered protocol family 1
[    0.942496] NET: Registered protocol family 44
[    0.942506] PCI: CLS 0 bytes, default 64
[    0.942616] Trying to unpack rootfs image as initramfs...
[    5.159501] Freeing initrd memory: 31996K
[    5.160120] hw perfevents: enabled with armv8_pmuv3 PMU driver, 7 counters available
[    5.160297] kvm [1]: IPA Size Limit: 40bits
[    5.161274] kvm [1]: vgic-v2@c0e0000
[    5.161289] kvm [1]: GIC system register CPU interface enabled
[    5.161386] kvm [1]: vgic interrupt IRQ1
[    5.161513] kvm [1]: Hyp mode initialized successfully
[    5.162523] Initialise system trusted keyrings
[    5.162543] Key type blacklist registered
[    5.162639] workingset: timestamp_bits=40 max_order=22 bucket_order=0
[    5.166636] zbud: loaded
[    5.167313] Platform Keyring initialized
[    5.176707] Key type asymmetric registered
[    5.176715] Asymmetric key parser 'x509' registered
[    5.176737] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 246)
[    5.176836] io scheduler mq-deadline registered
[    5.176845] io scheduler kyber registered
[    5.176919] io scheduler bfq registered
[    5.178783] shpchp: Standard Hot Plug PCI Controller Driver version: 0.4
[    5.179497] layerscape-pcie 3400000.pcie: host bridge /soc/pcie@3400000 ranges:
[    5.179521] layerscape-pcie 3400000.pcie:       IO 0x2000010000..0x200001ffff -> 0x0000000000
[    5.179539] layerscape-pcie 3400000.pcie:      MEM 0x2040000000..0x207fffffff -> 0x0040000000
[    5.179827] layerscape-pcie 3400000.pcie: iATU unroll: disabled
[    5.179836] layerscape-pcie 3400000.pcie: Detected iATU regions: 256 outbound, 24 inbound
[    6.180077] layerscape-pcie 3400000.pcie: Phy link never came up
[    6.180160] layerscape-pcie 3400000.pcie: PCI host bridge to bus 0000:00
[    6.180172] pci_bus 0000:00: root bus resource [bus 00-ff]
[    6.180180] pci_bus 0000:00: root bus resource [io  0x0000-0xffff]
[    6.180191] pci_bus 0000:00: root bus resource [mem 0x2040000000-0x207fffffff] (bus address [0x40000000-0x7fffffff])
[    6.180218] pci 0000:00:00.0: [1957:80c0] type 01 class 0x060400
[    6.180281] pci 0000:00:00.0: supports D1 D2
[    6.180289] pci 0000:00:00.0: PME# supported from D0 D1 D2 D3hot
[    6.182289] pci 0000:00:00.0: PCI bridge to [bus 01-ff]
[    6.182406] layerscape-pcie 3500000.pcie: host bridge /soc/pcie@3500000 ranges:
[    6.182431] layerscape-pcie 3500000.pcie:       IO 0x2800010000..0x280001ffff -> 0x0000000000
[    6.182448] layerscape-pcie 3500000.pcie:      MEM 0x2840000000..0x287fffffff -> 0x0040000000
[    6.182485] layerscape-pcie 3500000.pcie: iATU unroll: disabled
[    6.182494] layerscape-pcie 3500000.pcie: Detected iATU regions: 8 outbound, 6 inbound
[    6.282527] layerscape-pcie 3500000.pcie: Link up
[    6.282596] layerscape-pcie 3500000.pcie: PCI host bridge to bus 0001:00
[    6.282606] pci_bus 0001:00: root bus resource [bus 00-ff]
[    6.282616] pci_bus 0001:00: root bus resource [io  0x10000-0x1ffff] (bus address [0x0000-0xffff])
[    6.282628] pci_bus 0001:00: root bus resource [mem 0x2840000000-0x287fffffff] (bus address [0x40000000-0x7fffffff])
[    6.282648] pci 0001:00:00.0: [1957:80c0] type 01 class 0x060400
[    6.282694] pci 0001:00:00.0: supports D1 D2
[    6.282702] pci 0001:00:00.0: PME# supported from D0 D1 D2 D3hot
[    6.283799] pci 0001:01:00.0: [12d8:b304] type 01 class 0x060400
[    6.284208] pci 0001:01:00.0: supports D1 D2
[    6.284216] pci 0001:01:00.0: PME# supported from D0 D1 D2 D3hot D3cold
[    6.284381] pci 0001:01:00.0: PTM enabled (root), unknown granularity
[    6.284446] pci 0001:01:00.0: 4.000 Gb/s available PCIe bandwidth, limited by 5 GT/s x1 link at 0001:00:00.0 (capable of 8.000 Gb/s with 5 GT/s x2 link)
[    6.315501] pci 0001:02:01.0: [12d8:b304] type 01 class 0x060400
[    6.315919] pci 0001:02:01.0: supports D1 D2
[    6.315927] pci 0001:02:01.0: PME# supported from D0 D1 D2 D3hot D3cold
[    6.316263] pci 0001:02:02.0: [12d8:b304] type 01 class 0x060400
[    6.316681] pci 0001:02:02.0: supports D1 D2
[    6.316689] pci 0001:02:02.0: PME# supported from D0 D1 D2 D3hot D3cold
[    6.320000] pci 0001:02:01.0: PCI bridge to [bus 03]
[    6.320040] pci 0001:02:02.0: PCI bridge to [bus 04]
[    6.320078] pci 0001:01:00.0: PCI bridge to [bus 02-04]
[    6.320117] pci 0001:00:00.0: PCI bridge to [bus 01-ff]
[    6.320307] layerscape-pcie 3600000.pcie: host bridge /soc/pcie@3600000 ranges:
[    6.320329] layerscape-pcie 3600000.pcie:       IO 0x3000010000..0x300001ffff -> 0x0000000000
[    6.320346] layerscape-pcie 3600000.pcie:      MEM 0x3040000000..0x307fffffff -> 0x0040000000
[    6.320384] layerscape-pcie 3600000.pcie: iATU unroll: disabled
[    6.320393] layerscape-pcie 3600000.pcie: Detected iATU regions: 8 outbound, 6 inbound
[    6.420425] layerscape-pcie 3600000.pcie: Link up
[    6.420495] layerscape-pcie 3600000.pcie: PCI host bridge to bus 0002:00
[    6.420506] pci_bus 0002:00: root bus resource [bus 00-ff]
[    6.420515] pci_bus 0002:00: root bus resource [io  0x20000-0x2ffff] (bus address [0x0000-0xffff])
[    6.420527] pci_bus 0002:00: root bus resource [mem 0x3040000000-0x307fffffff] (bus address [0x40000000-0x7fffffff])
[    6.420547] pci 0002:00:00.0: [1957:80c0] type 01 class 0x060400
[    6.420591] pci 0002:00:00.0: supports D1 D2
[    6.420599] pci 0002:00:00.0: PME# supported from D0 D1 D2 D3hot
[    6.421687] pci 0002:01:00.0: [144d:a80a] type 00 class 0x010802
[    6.421788] pci 0002:01:00.0: reg 0x10: [mem 0x3040000000-0x3040003fff 64bit]
[    6.422308] pci 0002:01:00.0: 15.752 Gb/s available PCIe bandwidth, limited by 8 GT/s x2 link at 0002:00:00.0 (capable of 63.012 Gb/s with 16 GT/s x4 link)
[    6.423356] pci 0002:00:00.0: BAR 14: assigned [mem 0x3040000000-0x30400fffff]
[    6.423370] pci 0002:01:00.0: BAR 0: assigned [mem 0x3040000000-0x3040003fff 64bit]
[    6.423404] pci 0002:00:00.0: PCI bridge to [bus 01-ff]
[    6.423413] pci 0002:00:00.0:   bridge window [mem 0x3040000000-0x30400fffff]
[    6.427926] Serial: 8250/16550 driver, 4 ports, IRQ sharing enabled
[    6.429022] 21c0500.serial: ttyS0 at MMIO 0x21c0500 (irq = 8, base_baud = 21875000) is a 16550A
[    8.026576] printk: console [ttyS0] enabled
[    8.031043] 21c0600.serial: ttyS1 at MMIO 0x21c0600 (irq = 8, base_baud = 21875000) is a 16550A
[    8.039973] Serial: AMBA driver
[    8.043392] msm_serial: driver initialized
[    8.048180] arm-smmu 5000000.iommu: probing hardware configuration...
[    8.054627] arm-smmu 5000000.iommu: SMMUv2 with:
[    8.059241] arm-smmu 5000000.iommu:  stage 1 translation
[    8.064557] arm-smmu 5000000.iommu:  stage 2 translation
[    8.069866] arm-smmu 5000000.iommu:  nested translation
[    8.075093] arm-smmu 5000000.iommu:  stream matching with 128 register groups
[    8.082229] arm-smmu 5000000.iommu:  64 context banks (0 stage-2 only)
[    8.088762] arm-smmu 5000000.iommu:  Supported page sizes: 0x61311000
[    8.095205] arm-smmu 5000000.iommu:  Stage-1: 48-bit VA -> 48-bit IPA
[    8.101643] arm-smmu 5000000.iommu:  Stage-2: 48-bit IPA -> 48-bit PA
[    8.108832] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    8.115437] libphy: Fixed MDIO Bus: probed
[    8.119806] libphy: Freescale XGMAC MDIO Bus: probed
[    8.171012] libphy: Freescale XGMAC MDIO Bus: probed
[    8.181600] libphy: Freescale XGMAC MDIO Bus: probed
[    8.192018] libphy: Freescale XGMAC MDIO Bus: probed
[    8.218803] libphy: Freescale XGMAC MDIO Bus: probed
[    8.245652] mousedev: PS/2 mouse device common for all mice
[    8.252453] ledtrig-cpu: registered to indicate activity on CPUs
[    8.258783] hidraw: raw HID events driver (C) Jiri Kosina
[    8.265320] drop_monitor: Initializing network drop monitor service
[    8.272096] NET: Registered protocol family 10
[    8.288477] Segment Routing with IPv6
[    8.292144] RPL Segment Routing with IPv6
[    8.296653] registered taskstats version 1
[    8.300755] Loading compiled-in X.509 certificates
[    8.305684] Loaded X.509 cert 'SUSE Linux Enterprise Secure Boot Signkey: 5a240449d29fd0d8a7a187e6fc0e26b95d1aa87b'
[    8.316190] zswap: loaded using pool lzo/zbud
[    8.320581] page_owner is disabled
[    8.335399] Key type encrypted registered
[    8.339418] AppArmor: AppArmor sha1 policy hashing enabled
[    8.344928] ima: No TPM chip found, activating TPM-bypass!
[    8.350424] ima: Allocated hash algorithm: sha256
[    8.355156] ima: No architecture policies found
[    8.359704] evm: Initialising EVM extended attributes:
[    8.364843] evm: security.selinux
[    8.368151] evm: security.apparmor
[    8.371546] evm: security.ima
[    8.374511] evm: security.capability
[    8.378080] evm: HMAC attrs: 0x1
[    8.382415] fsl_mc_dprc dprc.1: Adding to iommu group 0
[    8.468959] fsl_mc_allocator dpbp.9: Adding to iommu group 0
[    8.474699] fsl_mc_allocator dpbp.8: Adding to iommu group 0
[    8.480422] fsl_mc_allocator dpbp.7: Adding to iommu group 0
[    8.486158] fsl_mc_allocator dpbp.6: Adding to iommu group 0
[    8.491881] fsl_mc_allocator dpbp.5: Adding to iommu group 0
[    8.497614] fsl_mc_allocator dpbp.4: Adding to iommu group 0
[    8.503338] fsl_mc_allocator dpbp.3: Adding to iommu group 0
[    8.509065] fsl_mc_allocator dpbp.2: Adding to iommu group 0
[    8.514799] fsl_mc_allocator dpbp.1: Adding to iommu group 0
[    8.520523] fsl_mc_allocator dpbp.0: Adding to iommu group 0
[    8.527262] fsl_mc_allocator dpmcp.34: Adding to iommu group 0
[    8.533663] fsl_mc_allocator dpmcp.33: Adding to iommu group 0
[    8.540070] fsl_mc_allocator dpmcp.32: Adding to iommu group 0
[    8.546476] fsl_mc_allocator dpmcp.31: Adding to iommu group 0
[    8.552879] fsl_mc_allocator dpmcp.30: Adding to iommu group 0
[    8.559290] fsl_mc_allocator dpmcp.29: Adding to iommu group 0
[    8.565695] fsl_mc_allocator dpmcp.28: Adding to iommu group 0
[    8.572096] fsl_mc_allocator dpmcp.27: Adding to iommu group 0
[    8.578511] fsl_mc_allocator dpmcp.26: Adding to iommu group 0
[    8.584919] fsl_mc_allocator dpmcp.25: Adding to iommu group 0
[    8.591324] fsl_mc_allocator dpmcp.24: Adding to iommu group 0
[    8.597733] fsl_mc_allocator dpmcp.23: Adding to iommu group 0
[    8.604135] fsl_mc_allocator dpmcp.22: Adding to iommu group 0
[    8.610544] fsl_mc_allocator dpmcp.21: Adding to iommu group 0
[    8.616953] fsl_mc_allocator dpmcp.20: Adding to iommu group 0
[    8.623358] fsl_mc_allocator dpmcp.19: Adding to iommu group 0
[    8.629768] fsl_mc_allocator dpmcp.18: Adding to iommu group 0
[    8.636178] fsl_mc_allocator dpmcp.17: Adding to iommu group 0
[    8.642585] fsl_mc_allocator dpmcp.16: Adding to iommu group 0
[    8.648992] fsl_mc_allocator dpmcp.15: Adding to iommu group 0
[    8.655414] fsl_mc_allocator dpmcp.14: Adding to iommu group 0
[    8.661819] fsl_mc_allocator dpmcp.13: Adding to iommu group 0
[    8.668226] fsl_mc_allocator dpmcp.12: Adding to iommu group 0
[    8.674638] fsl_mc_allocator dpmcp.11: Adding to iommu group 0
[    8.681044] fsl_mc_allocator dpmcp.10: Adding to iommu group 0
[    8.687458] fsl_mc_allocator dpmcp.9: Adding to iommu group 0
[    8.693776] fsl_mc_allocator dpmcp.8: Adding to iommu group 0
[    8.700105] fsl_mc_allocator dpmcp.7: Adding to iommu group 0
[    8.706432] fsl_mc_allocator dpmcp.6: Adding to iommu group 0
[    8.712748] fsl_mc_allocator dpmcp.5: Adding to iommu group 0
[    8.719075] fsl_mc_allocator dpmcp.4: Adding to iommu group 0
[    8.725401] fsl_mc_allocator dpmcp.3: Adding to iommu group 0
[    8.731723] fsl_mc_allocator dpmcp.2: Adding to iommu group 0
[    8.738050] fsl_mc_allocator dpmcp.1: Adding to iommu group 0
[    8.743869] fsl_mc_allocator dpcon.72: Adding to iommu group 0
[    8.749779] fsl_mc_allocator dpcon.71: Adding to iommu group 0
[    8.755698] fsl_mc_allocator dpcon.70: Adding to iommu group 0
[    8.761602] fsl_mc_allocator dpcon.69: Adding to iommu group 0
[    8.767513] fsl_mc_allocator dpcon.68: Adding to iommu group 0
[    8.773417] fsl_mc_allocator dpcon.67: Adding to iommu group 0
[    8.779331] fsl_mc_allocator dpcon.66: Adding to iommu group 0
[    8.785240] fsl_mc_allocator dpcon.65: Adding to iommu group 0
[    8.791146] fsl_mc_allocator dpcon.64: Adding to iommu group 0
[    8.797060] fsl_mc_allocator dpcon.63: Adding to iommu group 0
[    8.802966] fsl_mc_allocator dpcon.62: Adding to iommu group 0
[    8.808875] fsl_mc_allocator dpcon.61: Adding to iommu group 0
[    8.814793] fsl_mc_allocator dpcon.60: Adding to iommu group 0
[    8.820699] fsl_mc_allocator dpcon.59: Adding to iommu group 0
[    8.826616] fsl_mc_allocator dpcon.58: Adding to iommu group 0
[    8.832523] fsl_mc_allocator dpcon.57: Adding to iommu group 0
[    8.838438] fsl_mc_allocator dpcon.56: Adding to iommu group 0
[    8.844351] fsl_mc_allocator dpcon.55: Adding to iommu group 0
[    8.850257] fsl_mc_allocator dpcon.54: Adding to iommu group 0
[    8.856171] fsl_mc_allocator dpcon.53: Adding to iommu group 0
[    8.862078] fsl_mc_allocator dpcon.52: Adding to iommu group 0
[    8.867991] fsl_mc_allocator dpcon.51: Adding to iommu group 0
[    8.873900] fsl_mc_allocator dpcon.50: Adding to iommu group 0
[    8.879814] fsl_mc_allocator dpcon.49: Adding to iommu group 0
[    8.885729] fsl_mc_allocator dpcon.48: Adding to iommu group 0
[    8.891639] fsl_mc_allocator dpcon.47: Adding to iommu group 0
[    8.897553] fsl_mc_allocator dpcon.46: Adding to iommu group 0
[    8.903458] fsl_mc_allocator dpcon.45: Adding to iommu group 0
[    8.909375] fsl_mc_allocator dpcon.44: Adding to iommu group 0
[    8.915290] fsl_mc_allocator dpcon.43: Adding to iommu group 0
[    8.921201] fsl_mc_allocator dpcon.42: Adding to iommu group 0
[    8.927116] fsl_mc_allocator dpcon.41: Adding to iommu group 0
[    8.933026] fsl_mc_allocator dpcon.40: Adding to iommu group 0
[    8.938945] fsl_mc_allocator dpcon.39: Adding to iommu group 0
[    8.944861] fsl_mc_allocator dpcon.38: Adding to iommu group 0
[    8.950771] fsl_mc_allocator dpcon.37: Adding to iommu group 0
[    8.956688] fsl_mc_allocator dpcon.36: Adding to iommu group 0
[    8.962597] fsl_mc_allocator dpcon.35: Adding to iommu group 0
[    8.968517] fsl_mc_allocator dpcon.34: Adding to iommu group 0
[    8.974431] fsl_mc_allocator dpcon.33: Adding to iommu group 0
[    8.980343] fsl_mc_allocator dpcon.32: Adding to iommu group 0
[    8.986261] fsl_mc_allocator dpcon.31: Adding to iommu group 0
[    8.992172] fsl_mc_allocator dpcon.30: Adding to iommu group 0
[    8.998087] fsl_mc_allocator dpcon.29: Adding to iommu group 0
[    9.004000] fsl_mc_allocator dpcon.28: Adding to iommu group 0
[    9.009923] fsl_mc_allocator dpcon.27: Adding to iommu group 0
[    9.015843] fsl_mc_allocator dpcon.26: Adding to iommu group 0
[    9.021755] fsl_mc_allocator dpcon.25: Adding to iommu group 0
[    9.027674] fsl_mc_allocator dpcon.24: Adding to iommu group 0
[    9.033586] fsl_mc_allocator dpcon.23: Adding to iommu group 0
[    9.039505] fsl_mc_allocator dpcon.22: Adding to iommu group 0
[    9.045424] fsl_mc_allocator dpcon.21: Adding to iommu group 0
[    9.051336] fsl_mc_allocator dpcon.20: Adding to iommu group 0
[    9.057258] fsl_mc_allocator dpcon.19: Adding to iommu group 0
[    9.063175] fsl_mc_allocator dpcon.18: Adding to iommu group 0
[    9.069095] fsl_mc_allocator dpcon.17: Adding to iommu group 0
[    9.075015] fsl_mc_allocator dpcon.16: Adding to iommu group 0
[    9.080930] fsl_mc_allocator dpcon.15: Adding to iommu group 0
[    9.086851] fsl_mc_allocator dpcon.14: Adding to iommu group 0
[    9.092762] fsl_mc_allocator dpcon.13: Adding to iommu group 0
[    9.098685] fsl_mc_allocator dpcon.12: Adding to iommu group 0
[    9.104605] fsl_mc_allocator dpcon.11: Adding to iommu group 0
[    9.110521] fsl_mc_allocator dpcon.10: Adding to iommu group 0
[    9.116440] fsl_mc_allocator dpcon.9: Adding to iommu group 0
[    9.122267] fsl_mc_allocator dpcon.8: Adding to iommu group 0
[    9.128103] fsl_mc_allocator dpcon.7: Adding to iommu group 0
[    9.133932] fsl_mc_allocator dpcon.6: Adding to iommu group 0
[    9.139767] fsl_mc_allocator dpcon.5: Adding to iommu group 0
[    9.145602] fsl_mc_allocator dpcon.4: Adding to iommu group 0
[    9.151430] fsl_mc_allocator dpcon.3: Adding to iommu group 0
[    9.157266] fsl_mc_allocator dpcon.2: Adding to iommu group 0
[    9.163092] fsl_mc_allocator dpcon.1: Adding to iommu group 0
[    9.168928] fsl_mc_allocator dpcon.0: Adding to iommu group 0
[    9.187288] fsl_mc_dprc dprc.1: DPRC device bound to driver
[    9.192921] pcieport 0000:00:00.0: Adding to iommu group 1
[    9.198551] pcieport 0000:00:00.0: PME: Signaling with IRQ 353
[    9.204513] pcieport 0000:00:00.0: AER: enabled with IRQ 353
[    9.210364] pcieport 0001:00:00.0: Adding to iommu group 2
[    9.215958] pcieport 0001:00:00.0: PME: Signaling with IRQ 354
[    9.221925] pcieport 0001:00:00.0: AER: enabled with IRQ 354
[    9.227760] pcieport 0001:01:00.0: Adding to iommu group 2
[    9.233948] pcieport 0001:02:01.0: Adding to iommu group 2
[    9.240014] pcieport 0001:02:01.0: DPC: error containment capabilities: Int Msg #1, RPExt- PoisonedTLP- SwTrigger- RP PIO Log 0, DL_ActiveErr-
[    9.253241] pcieport 0001:02:02.0: Adding to iommu group 2
[    9.259311] pcieport 0001:02:02.0: DPC: error containment capabilities: Int Msg #1, RPExt- PoisonedTLP- SwTrigger- RP PIO Log 0, DL_ActiveErr-
[    9.272576] pcieport 0002:00:00.0: Adding to iommu group 3
[    9.278171] pcieport 0002:00:00.0: PME: Signaling with IRQ 360
[    9.284118] pcieport 0002:00:00.0: AER: enabled with IRQ 360
[    9.289991] hctosys: unable to open rtc device (rtc0)
[    9.299185] Freeing unused kernel memory: 6400K
[    9.314388] Run /init as init process
[    9.333789] systemd[1]: System time before build time, advancing clock.
[    9.536667] systemd[1]: systemd 246.13+suse.127.g60e71ffa7e running in system mode. (+PAM +AUDIT +SELINUX -IMA +APPARMOR -SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=hybrid)
[    9.561142] systemd[1]: Detected architecture arm64.
[    9.566153] systemd[1]: Running in initial RAM disk.

Welcome to openSUSE Leap 15.3 dracut-049.1+suse.188.gbf445638-3.30.1 (Initramfs)!

[    9.634458] systemd[1]: No hostname configured.
[    9.638997] systemd[1]: Set hostname to <localhost>.
[    9.644046] random: systemd: uninitialized urandom read (16 bytes read)
[    9.650685] systemd[1]: Initializing machine ID from random generator.
[    9.824920] systemd[1]: Queued start job for default target Initrd Default Target.
[    9.832821] random: systemd: uninitialized urandom read (16 bytes read)
[    9.839588] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Started Dispatch Password …ts to Console Directory Watch.
[    9.884448] random: systemd: uninitialized urandom read (16 bytes read)
[    9.891077] systemd[1]: Reached target Local Encrypted Volumes.
[  OK  ] Reached target Local Encrypted Volumes.
[    9.924441] systemd[1]: Reached target Local File Systems.
[  OK  ] Reached target Local File Systems.
[    9.964407] systemd[1]: Reached target Paths.
[  OK  ] Reached target Paths.
[    9.994440] systemd[1]: Reached target Slices.
[  OK  ] Reached target Slices.
[   10.024422] systemd[1]: Reached target Swap.
[  OK  ] Reached target Swap.
[   10.054426] systemd[1]: Reached target Timers.
[  OK  ] Reached target Timers.
[   10.084636] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[   10.124607] systemd[1]: Listening on Journal Socket.
[  OK  ] Listening on Journal Socket.
[   10.154620] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[   10.194560] systemd[1]: Listening on udev Kernel Socket.
[  OK  ] Listening on udev Kernel Socket.
[   10.234416] systemd[1]: Reached target Sockets.
[  OK  ] Reached target Sockets.
[   10.267644] systemd[1]: Starting Create list of static device nodes for the current kernel...
         Starting Create list of st…odes for the current kernel...
[   10.307520] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[   10.346759] systemd[1]: Starting Load Kernel Modules...
         Starting Load Kernel Modules...
[   10.386906] systemd[1]: Starting Setup Virtual Console...
         Starting Setup Virtual Console...
[   10.400801] SCSI subsystem initialized
[   10.426441] systemd[1]: Finished Create list of static device nodes for the current kernel.
[  OK  ] Finished Create list of st… nodes for the current kernel.
[   10.465773] systemd[1]: Finished Load Kernel Modules.
[  OK  ] Finished Load Kernel Modules.
[   10.505726] systemd[1]: Finished Setup Virtual Console.
[  OK  ] Finished Setup Virtual Console.
[   10.544832] systemd[1]: Condition check resulted in dracut ask for additional cmdline parameters being skipped.
[   10.557347] systemd[1]: Starting dracut cmdline hook...
         Starting dracut cmdline hook...
[   10.606667] systemd[1]: Starting Apply Kernel Variables...
         Starting Apply Kernel Variables...
[   10.646786] systemd[1]: Starting Create Static Device Nodes in /dev...
         Starting Create Static Device Nodes in /dev...
[   10.685990] systemd[1]: Finished Apply Kernel Variables.
[  OK  ] Finished Apply Kernel Variables.
[   10.725996] systemd[1]: Finished Create Static Device Nodes in /dev.
[  OK  ] Finished Create Static Device Nodes in /dev.
[   10.775945] systemd[1]: Finished dracut cmdline hook.
[  OK  ] Finished dracut cmdline hook.
[   10.826885] systemd[1]: Starting dracut pre-udev hook...
         Starting dracut pre-udev hook...
[   10.864758] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
[   10.891981] device-mapper: uevent: version 1.0.3
[   10.896792] device-mapper: ioctl: 4.42.0-ioctl (2020-02-27) initialised: dm-devel@redhat.com
         Starting Create Volatile Files and Directories...
[  OK  ] Finished Create Volatile Files and Directories.
[  OK  ] Finished dracut pre-udev hook.
         Starting Rule-based Manage…for Device Events and Files...
[  OK  ] Started Rule-based Manager for Device Events and Files.
         Starting dracut pre-trigger hook...
[  OK  ] Finished dracut pre-trigger hook.
         Starting Coldplug All udev Devices...
[   11.893842] input: buttons as /devices/platform/buttons/input/input0
[   11.902171] imx-i2c 2000000.i2c: can't get pinctrl, bus recovery not supported
[  OK     11.910082] i2c i2c-0: IMX I2C adapter registered
0m] Finished    11.915909] imx-i2c 2020000.i2c: can't get pinctrl, bus recovery not supported
;1;39mColdplug A[   11.924504] i2c i2c-1: IMX I2C adapter registered
ll udev Devices imx-i2c 2030000.i2c: can't get pinctrl, bus recovery not supported
[0m.
[   11.939296] i2c i2c-2: IMX I2C adapter registered
[   11.939624] sdhci: Secure Digital Host Controller Interface driver
[   11.950511] sdhci: Copyright(c) Pierre Ossman
[   11.955657] caam 8000000.crypto: device ID = 0x0a13030000000000 (Era 8)
[   11.962303] caam 8000000.crypto: job rings = 3, qi = 1
[   11.962538] sdhci-pltfm: SDHCI platform and OF driver helper
[   11.968660] dwc3 3100000.usb: Failed to get clk 'ref': -2
[   11.978644] dwc3 3100000.usb: IRQ host not found
[   11.983279] dwc3 3100000.usb: IRQ dwc_usb3 not found
[   11.983797] dwc3 3110000.usb: Failed to get clk 'ref': -2
[  OK     11.993738] dwc3 3110000.usb: IRQ host not found
m] Reached targe[   11.999813] dwc3 3110000.usb: IRQ dwc_usb3 not found
t Syste[   12.004411] mmc0: SDHCI controller on 2140000.esdhc [2140000.esdhc] using ADMA 64-bit
m Initialization.
[   12.017226] nvme 0002:01:00.0: Adding to iommu group 3
[  OK     12.023027] nvme nvme0: pci function 0002:01:00.0
0m] Reached target Basic System.
[   12.032789] fsl_mc_dpio dpio.8: Adding to iommu group 0
[   12.041654] nvme nvme0: Shutdown timeout set to 10 seconds
[   12.043057] fsl_mc_dpio dpio.8: probed
         [   12.051652] fsl_mc_dpio dpio.7: Adding to iommu group 0
Starting    12.053449] nvme nvme0: 8/0/0 default/read/poll queues
9mdracut initque[   12.060924] fsl_mc_dpio dpio.7: probed
ue hook...
[   12.067288] random: fast init done
[   12.069451] fsl_mc_dpio dpio.6: Adding to iommu group 0
[   12.080131]  nvme0n1: p1 p2 p3
[   12.082617] fsl_mc_dpio dpio.6: probed
[   12.088700] fsl_mc_dpio dpio.5: Adding to iommu group 0
[   12.098903] fsl_mc_dpio dpio.5: probed
[   12.103737] fsl_mc_dpio dpio.4: Adding to iommu group 0
[   12.112937] fsl_mc_dpio dpio.4: probed
[   12.118808] fsl_mc_dpio dpio.3: Adding to iommu group 0
[   12.130125] fsl_mc_dpio dpio.3: probed
[   12.134314] pca953x 0-0076: 0-0076 supply vcc not found, using dummy regulator
[   12.135417] fsl_mc_dpio dpio.2: Adding to iommu group 0
[   12.141731] pca953x 0-0076: using no AI
[   12.151802] fsl_mc_dpio dpio.2: probed
[   12.153654] usbcore: registered new interface driver usbfs
[   12.153663] GPIO line 381 (admin_led_lower) hogged as output/low
[   12.156238] fsl_mc_dpio dpio.1: Adding to iommu group 0
[   12.161188] usbcore: registered new interface driver hub
[   12.170606] fsl_mc_dpio dpio.1: probed
[   12.172422] usbcore: registered new device driver usb
[   12.189518] dpaa2_caam dpseci.0: Adding to iommu group 0
[   12.196446] dpaa2_caam dpseci.0: dpseci v5.3
[   12.213693] fsl_mc_dprc dprc.1 (unnamed net_device) (uninitialized): netif_napi_add() called with weight 512
[   12.228796] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[   12.234300] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 1
[   12.242080] xhci-hcd xhci-hcd.0.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[   12.251522] xhci-hcd xhci-hcd.0.auto: irq 14, io mem 0x03100000
[   12.257656] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.03
[   12.265146] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.265933] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   12.270819] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.278030] usb usb1: Product: xHCI Host Controller
[   12.282937] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.287790] usb usb1: Manufacturer: Linux 5.3.18-57-default xhci-hcd
[   12.292693] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.299023] usb usb1: SerialNumber: xhci-hcd.0.auto
[   12.303935] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.309098] hub 1-0:1.0: USB hub found
[   12.313708] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.317438] hub 1-0:1.0: 1 port detected
[   12.322330] dpaa2_caam dpseci.0: FD error: 000000a0
[   12.326466] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[   12.334047] alg: No test for authenc(hmac(md5),cbc(aes)) (authenc-hmac-md5-cbc-aes-caam-qi2)
[   12.336597] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 2
[   12.336606] xhci-hcd xhci-hcd.0.auto: Host supports USB 3.0 SuperSpeed
[   12.345121] alg: No test for echainiv(authenc(hmac(md5),cbc(aes))) (echainiv-authenc-hmac-md5-cbc-aes-caam-qi2)
[   12.352749] usb usb2: We don't know the algorithms for LPM for this host, disabling LPM.
[   12.359246] raid6: neonx8   gen()  3142 MB/s
[   12.361126] alg: No test for echainiv(authenc(hmac(sha1),cbc(aes))) (echainiv-authenc-hmac-sha1-cbc-aes-caam-qi2)
[   12.369418] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.03
[   12.377528] alg: No test for authenc(hmac(sha224),cbc(aes)) (authenc-hmac-sha224-cbc-aes-caam-qi2)
[   12.381729] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   12.381732] usb usb2: Product: xHCI Host Controller
[   12.381735] usb usb2: Manufacturer: Linux 5.3.18-57-default xhci-hcd
[   12.381738] usb usb2: SerialNumber: xhci-hcd.0.auto
[   12.392073] alg: No test for echainiv(authenc(hmac(sha224),cbc(aes))) (echainiv-authenc-hmac-sha224-cbc-aes-caam-qi2)
[   12.400542] hub 2-0:1.0: USB hub found
[   12.411099] alg: No test for echainiv(authenc(hmac(sha256),cbc(aes))) (echainiv-authenc-hmac-sha256-cbc-aes-caam-qi2)
[   12.416503] hub 2-0:1.0: 1 port detected
[   12.421441] alg: No test for authenc(hmac(sha384),cbc(aes)) (authenc-hmac-sha384-cbc-aes-caam-qi2)
[   12.428000] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[   12.432688] alg: No test for echainiv(authenc(hmac(sha384),cbc(aes))) (echainiv-authenc-hmac-sha384-cbc-aes-caam-qi2)
[   12.443251] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 3
[   12.448969] alg: No test for echainiv(authenc(hmac(sha512),cbc(aes))) (echainiv-authenc-hmac-sha512-cbc-aes-caam-qi2)
[   12.457707] xhci-hcd xhci-hcd.1.auto: hcc params 0x0220f66d hci version 0x100 quirks 0x0000000002010010
[   12.461634] alg: No test for authenc(hmac(md5),cbc(des3_ede)) (authenc-hmac-md5-cbc-des3_ede-caam-qi2)
[   12.470552] xhci-hcd xhci-hcd.1.auto: irq 15, io mem 0x03110000
[   12.476095] alg: No test for echainiv(authenc(hmac(md5),cbc(des3_ede))) (echainiv-authenc-hmac-md5-cbc-des3_ede-caam-qi2)
[   12.486873] usb usb3: New USB device found, idVendor=1d6b, idProduct=0002, bcdDevice= 5.03
[   12.494740] alg: No test for echainiv(authenc(hmac(sha1),cbc(des3_ede))) (echainiv-authenc-hmac-sha1-cbc-des3_ede-caam-qi2)
[   12.504941] usb usb3: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   12.514727] alg: No test for echainiv(authenc(hmac(sha224),cbc(des3_ede))) (echainiv-authenc-hmac-sha224-cbc-des3_ede-caam-qi2)
[   12.523648] usb usb3: Product: xHCI Host Controller
[   12.529561] raid6: neonx8   xor()  2234 MB/s
[   12.529961] alg: No test for echainiv(authenc(hmac(sha256),cbc(des3_ede))) (echainiv-authenc-hmac-sha256-cbc-des3_ede-caam-qi2)
[   12.540536] usb usb3: Manufacturer: Linux 5.3.18-57-default xhci-hcd
[   12.549204] alg: No test for echainiv(authenc(hmac(sha384),cbc(des3_ede))) (echainiv-authenc-hmac-sha384-cbc-des3_ede-caam-qi2)
[   12.559954] usb usb3: SerialNumber: xhci-hcd.1.auto
[   12.567569] alg: No test for echainiv(authenc(hmac(sha512),cbc(des3_ede))) (echainiv-authenc-hmac-sha512-cbc-des3_ede-caam-qi2)
[   12.578946] hub 3-0:1.0: USB hub found
[   12.583618] alg: No test for authenc(hmac(md5),cbc(des)) (authenc-hmac-md5-cbc-des-caam-qi2)
[   12.587842] hub 3-0:1.0: 1 port detected
[   12.599378] alg: No test for echainiv(authenc(hmac(md5),cbc(des))) (echainiv-authenc-hmac-md5-cbc-des-caam-qi2)
[   12.605856] xhci-hcd xhci-hcd.1.auto: xHCI Host Controller
[   12.617554] alg: No test for echainiv(authenc(hmac(sha1),cbc(des))) (echainiv-authenc-hmac-sha1-cbc-des-caam-qi2)
[   12.622037] xhci-hcd xhci-hcd.1.auto: new USB bus registered, assigned bus number 4
[   12.633898] alg: No test for echainiv(authenc(hmac(sha224),cbc(des))) (echainiv-authenc-hmac-sha224-cbc-des-caam-qi2)
[   12.637278] xhci-hcd xhci-hcd.1.auto: Host supports USB 3.0 SuperSpeed
[   12.646115] alg: No test for echainiv(authenc(hmac(sha256),cbc(des))) (echainiv-authenc-hmac-sha256-cbc-des-caam-qi2)
[   12.649686] usb usb4: We don't know the algorithms for LPM for this host, disabling LPM.
[   12.660128] alg: No test for echainiv(authenc(hmac(sha384),cbc(des))) (echainiv-authenc-hmac-sha384-cbc-des-caam-qi2)
[   12.665302] usb usb4: New USB device found, idVendor=1d6b, idProduct=0003, bcdDevice= 5.03
[   12.675891] alg: No test for echainiv(authenc(hmac(sha512),cbc(des))) (echainiv-authenc-hmac-sha512-cbc-des-caam-qi2)
[   12.683160] usb usb4: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   12.693836] alg: No test for authenc(hmac(md5),rfc3686(ctr(aes))) (authenc-hmac-md5-rfc3686-ctr-aes-caam-qi2)
[   12.700308] usb usb4: Product: xHCI Host Controller
[   12.700310] raid6: neonx4   gen()  3016 MB/s
[   12.700315] usb usb4: Manufacturer: Linux 5.3.18-57-default xhci-hcd
[   12.710992] alg: No test for seqiv(authenc(hmac(md5),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-md5-rfc3686-ctr-aes-caam-qi2)
[   12.719030] usb usb4: SerialNumber: xhci-hcd.1.auto
[   12.729757] alg: No test for seqiv(authenc(hmac(sha1),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha1-rfc3686-ctr-aes-caam-qi2)
[   12.738186] hub 4-0:1.0: USB hub found
[   12.748598] alg: No test for authenc(hmac(sha224),rfc3686(ctr(aes))) (authenc-hmac-sha224-rfc3686-ctr-aes-caam-qi2)
[   12.755786] hub 4-0:1.0: 1 port detected
[   12.765763] alg: No test for seqiv(authenc(hmac(sha224),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha224-rfc3686-ctr-aes-caam-qi2)
[   12.838165] alg: No test for seqiv(authenc(hmac(sha256),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha256-rfc3686-ctr-aes-caam-qi2)
[   12.849874] alg: No test for seqiv(authenc(hmac(sha384),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha384-rfc3686-ctr-aes-caam-qi2)
[   12.858374] alg: No test for authenc(hmac(sha224),ecb(cipher_null)) (authenc-hmac-sha224-ecb-cipher_null-caam)
[   12.861562] alg: No test for seqiv(authenc(hmac(sha512),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha512-rfc3686-ctr-aes-caam-qi2)
[   12.871452] raid6: neonx4   xor()  2162 MB/s
[   12.871534] alg: No test for authenc(hmac(sha256),ecb(cipher_null)) (authenc-hmac-sha256-ecb-cipher_null-caam)
[   12.883040] dpaa2_caam dpseci.0: algorithms registered in /proc/crypto
[   12.898094] alg: No test for authenc(hmac(sha384),ecb(cipher_null)) (authenc-hmac-sha384-ecb-cipher_null-caam)
[   12.913993] alg: No test for authenc(hmac(sha512),ecb(cipher_null)) (authenc-hmac-sha512-ecb-cipher_null-caam)
[   12.924139] alg: No test for authenc(hmac(md5),cbc(aes)) (authenc-hmac-md5-cbc-aes-caam)
[   12.932382] alg: No test for echainiv(authenc(hmac(md5),cbc(aes))) (echainiv-authenc-hmac-md5-cbc-aes-caam)
[   12.981455] alg: No test for echainiv(authenc(hmac(sha1),cbc(aes))) (echainiv-authenc-hmac-sha1-cbc-aes-caam)
[   12.991522] alg: No test for authenc(hmac(sha224),cbc(aes)) (authenc-hmac-sha224-cbc-aes-caam)
[   12.994364] usb 3-1: new high-speed USB device number 2 using xhci-hcd
[   13.000300] alg: No test for echainiv(authenc(hmac(sha224),cbc(aes))) (echainiv-authenc-hmac-sha224-cbc-aes-caam)
[   13.034355] raid6: neonx2   gen()  2783 MB/s
[   13.056433] alg: No test for echainiv(authenc(hmac(sha256),cbc(aes))) (echainiv-authenc-hmac-sha256-cbc-aes-caam)
[   13.066844] alg: No test for authenc(hmac(sha384),cbc(aes)) (authenc-hmac-sha384-cbc-aes-caam)
[   13.075629] alg: No test for echainiv(authenc(hmac(sha384),cbc(aes))) (echainiv-authenc-hmac-sha384-cbc-aes-caam)
[   13.114755] dpaa2_caam dpseci.0: hash algorithms registered in /proc/crypto
[   13.132222] alg: No test for echainiv(authenc(hmac(sha512),cbc(aes))) (echainiv-authenc-hmac-sha512-cbc-aes-caam)
[   13.142573] alg: No test for authenc(hmac(md5),cbc(des3_ede)) (authenc-hmac-md5-cbc-des3_ede-caam)
[   13.151746] alg: No test for echainiv(authenc(hmac(md5),cbc(des3_ede))) (echainiv-authenc-hmac-md5-cbc-des3_ede-caam)
[   13.168166] alg: No test for echainiv(authenc(hmac(sha1),cbc(des3_ede))) (echainiv-authenc-hmac-sha1-cbc-des3_ede-caam)
[   13.184676] alg: No test for echainiv(authenc(hmac(sha224),cbc(des3_ede))) (echainiv-authenc-hmac-sha224-cbc-des3_ede-caam)
[   13.196254] usb 3-1: New USB device found, idVendor=0424, idProduct=2744, bcdDevice= 2.08
[   13.201525] alg: No test for echainiv(authenc(hmac(sha256),cbc(des3_ede))) (echainiv-authenc-hmac-sha256-cbc-des3_ede-caam)
[   13.204437] raid6: neonx2   xor()  2006 MB/s
[   13.204444] usb 3-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[   13.221274] alg: No test for echainiv(authenc(hmac(sha384),cbc(des3_ede))) (echainiv-authenc-hmac-sha384-cbc-des3_ede-caam)
[   13.226987] usb 3-1: Product: USB2744
[   13.226991] usb 3-1: Manufacturer: Microchip Tech
[   13.243834] alg: No test for echainiv(authenc(hmac(sha512),cbc(des3_ede))) (echainiv-authenc-hmac-sha512-cbc-des3_ede-caam)
[   13.257711] alg: No test for authenc(hmac(md5),cbc(des)) (authenc-hmac-md5-cbc-des-caam)
[   13.265929] alg: No test for echainiv(authenc(hmac(md5),cbc(des))) (echainiv-authenc-hmac-md5-cbc-des-caam)
[   13.281413] alg: No test for echainiv(authenc(hmac(sha1),cbc(des))) (echainiv-authenc-hmac-sha1-cbc-des-caam)
[   13.291473] hub 3-1:1.0: USB hub found
[   13.295262] hub 3-1:1.0: 4 ports detected
[   13.297082] alg: No test for echainiv(authenc(hmac(sha224),cbc(des))) (echainiv-authenc-hmac-sha224-cbc-des-caam)
[   13.315255] alg: No test for echainiv(authenc(hmac(sha256),cbc(des))) (echainiv-authenc-hmac-sha256-cbc-des-caam)
[   13.331232] alg: No test for echainiv(authenc(hmac(sha384),cbc(des))) (echainiv-authenc-hmac-sha384-cbc-des-caam)
[   13.347291] alg: No test for echainiv(authenc(hmac(sha512),cbc(des))) (echainiv-authenc-hmac-sha512-cbc-des-caam)
[   13.357574] usb 4-1: new SuperSpeed Gen 1 USB device number 2 using xhci-hcd
[   13.357640] alg: No test for authenc(hmac(md5),rfc3686(ctr(aes))) (authenc-hmac-md5-rfc3686-ctr-aes-caam)
[   13.374280] alg: No test for seqiv(authenc(hmac(md5),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-md5-rfc3686-ctr-aes-caam)
[   13.374351] raid6: neonx1   gen()  2282 MB/s
[   13.385139] alg: No test for seqiv(authenc(hmac(sha1),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha1-rfc3686-ctr-aes-caam)
[   13.400293] alg: No test for authenc(hmac(sha224),rfc3686(ctr(aes))) (authenc-hmac-sha224-rfc3686-ctr-aes-caam)
[   13.410490] alg: No test for seqiv(authenc(hmac(sha224),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha224-rfc3686-ctr-aes-caam)
[   13.410568] usb 4-1: New USB device found, idVendor=0424, idProduct=5744, bcdDevice= 2.08
[   13.421870] alg: No test for seqiv(authenc(hmac(sha256),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha256-rfc3686-ctr-aes-caam)
[   13.429902] usb 4-1: New USB device strings: Mfr=2, Product=3, SerialNumber=0
[   13.441262] alg: No test for seqiv(authenc(hmac(sha384),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha384-rfc3686-ctr-aes-caam)
[   13.448270] usb 4-1: Product: USB5744
[   13.459628] alg: No test for seqiv(authenc(hmac(sha512),rfc3686(ctr(aes)))) (seqiv-authenc-hmac-sha512-rfc3686-ctr-aes-caam)
[   13.463155] usb 4-1: Manufacturer: Microchip Tech
[   13.474394] caam algorithms registered in /proc/crypto
[   13.497780] hub 4-1:1.0: USB hub found
[   13.501569] hub 4-1:1.0: 4 ports detected
[   13.554353] raid6: neonx1   xor()  1631 MB/s
[   13.724358] raid6: int64x8  gen()  1609 MB/s
[   13.894347] raid6: int64x8  xor()  1009 MB/s
[   14.064348] raid6: int64x4  gen()  2255 MB/s
[   14.142269] caam 8000000.crypto: caam pkc algorithms registered in /proc/crypto
[   14.149593] caam 8000000.crypto: registering rng-caam
[   14.200914] random: crng init done
[   14.204318] random: 7 urandom warning(s) missed due to ratelimiting
[   14.234355] raid6: int64x4  xor()  1143 MB/s
[   14.404352] raid6: int64x2  gen()  1891 MB/s
[   14.574348] raid6: int64x2  xor()  1014 MB/s
[   14.744358] raid6: int64x1  gen()  1327 MB/s
[   14.914350] raid6: int64x1  xor()   731 MB/s
[   14.918615] raid6: using algorithm neonx8 gen() 3142 MB/s
[   14.924010] raid6: .... xor() 2234 MB/s, rmw enabled
[   14.928977] raid6: using neon recovery algorithm
[   14.992148] xor: measuring software checksum speed
[   15.094347]    8regs     :  3055.200 MB/sec
[   15.194347]    32regs    :  3747.200 MB/sec
[   15.294347]    arm64_neon:  3306.000 MB/sec
[   15.298526] xor: using function: 32regs (3747.200 MB/sec)
[   15.456900] Btrfs loaded, crc32c=crc32c-generic, assert=on
[   15.471755] BTRFS: device label ROOT devid 1 transid 45 /dev/nvme0n1p3
[  OK  ] Found device SAMSUNG MZVL2256HCHQ-00B00 ROOT.
[  OK  ] Finished dracut initqueue hook.
[  OK  ] Reached target Initrd Root Device.
[  OK  ] Reached target Remote File Systems (Pre).
[  OK  ] Reached target Remote File Systems.
         Starting dracut pre-mount hook...
[   15.778411] dracut-pre-mount[684]: Created symlink /etc/systemd/system/systemd-fsck-root.service → /dev/null.
[   16.666917]  nvme0n1: p1 p2 p3
[   16.693706] dracut-pre-mount[1012]: Removed /etc/systemd/system/systemd-fsck-root.service.
[  OK  ] Finished dracut pre-mount hook.
         Starting File System Check…b95c-4d4b-aeb8-45fd94a1db83...
[  OK  ] Finished File System Check…d-b95c-4d4b-aeb8-45fd94a1db83.
         Mounting /sysroot...
[   17.159237] BTRFS info (device nvme0n1p3): disk space caching is enabled
[   17.166036] BTRFS info (device nvme0n1p3): has skinny extents
[   17.176106] BTRFS info (device nvme0n1p3): enabling ssd optimizations
[  OK  ] Mounted /sysroot.
[  OK  ] Reached target Initrd Root File System.
         Starting Reload Configuration from the Real Root...
[  OK  ] Finished Reload Configuration from the Real Root.
[  OK  ] Reached target Initrd File Systems.
[  OK  ] Reached target Initrd Default Target.
         Starting dracut pre-pivot and cleanup hook...
[  OK  ] Finished dracut pre-pivot and cleanup hook.
         Starting Cleaning Up and Shutting Down Daemons...
[  OK  ] Stopped target Timers.
[  OK  ] Stopped dracut pre-pivot and cleanup hook.
[  OK  ] Stopped target Initrd Default Target.
[  OK  ] Stopped target Basic System.
[  OK  ] Stopped target Initrd Root Device.
[  OK  ] Stopped target Paths.
[  OK  ] Stopped target Remote File Systems.
[  OK  ] Stopped target Remote File Systems (Pre).
[  OK  ] Stopped target Slices.
[  OK  ] Stopped target Sockets.
[  OK  ] Stopped target System Initialization.
[  OK  ] Stopped target Swap.
[  OK  ] Stopped dracut pre-mount hook.
[  OK  ] Stopped target Local Encrypted Volumes.
[  OK  ] Stopped Dispatch Password …ts to Console Directory Watch.
[  OK  ] Stopped dracut initqueue hook.
[  OK  ] Stopped Apply Kernel Variables.
[  OK  ] Stopped Load Kernel Modules.
[  OK  ] Stopped Create Volatile Files and Directories.
[  OK  ] Stopped target Local File Systems.
[  OK  ] Stopped Coldplug All udev Devices.
[  OK  ] Stopped dracut pre-trigger hook.
         Stopping Rule-based Manage…for Device Events and Files...
[  OK  ] Stopped Setup Virtual Console.
[  OK  ] Stopped Rule-based Manager for Device Events and Files.
[  OK  ] Closed udev Control Socket.
[  OK  ] Closed udev Kernel Socket.
[  OK  ] Stopped dracut pre-udev hook.
[  OK  ] Stopped dracut cmdline hook.
         Starting Cleanup udev Database...
[  OK  ] Stopped Create Static Device Nodes in /dev.
[  OK  ] Stopped Create list of sta… nodes for the current kernel.
[  OK  ] Finished Cleaning Up and Shutting Down Daemons.
[  OK  ] Finished Cleanup udev Database.
[  OK  ] Reached target Switch Root.
         Starting Switch Root...
[   19.172144] systemd-journald[206]: Received SIGTERM from PID 1 (systemd).
[   19.508770] systemd[1]: systemd 246.13+suse.127.g60e71ffa7e running in system mode. (+PAM +AUDIT +SELINUX -IMA +APPARMOR -SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=hybrid)
[   19.533209] systemd[1]: Detected architecture arm64.

Welcome to openSUSE Leap 15.3!

[   19.948716] systemd[1]: initrd-switch-root.service: Succeeded.
[   19.955182] systemd[1]: Stopped Switch Root.
[  OK  ] Stopped Switch Root.
[   19.985099] systemd[1]: systemd-journald.service: Scheduled restart job, restart counter is at 1.
[   19.995756] systemd[1]: Created slice system-getty.slice.
[  OK  ] Created slice system-getty.slice.
[   20.034889] systemd[1]: Created slice system-modprobe.slice.
[  OK  ] Created slice system-modprobe.slice.
[   20.075679] systemd[1]: Created slice system-serial\x2dgetty.slice.
[  OK  ] Created slice system-serial\x2dgetty.slice.
[   20.114842] systemd[1]: Created slice User and Session Slice.
[  OK  ] Created slice User and Session Slice.
[   20.156165] systemd[1]: Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Started Dispatch Password …ts to Console Directory Watch.
[   20.194969] systemd[1]: Set up automount Arbitrary Executable File Formats File System Automount Point.
[  OK  ] Set up automount Arbitrary…s File System Automount Point.
[   20.234439] systemd[1]: Reached target Local Encrypted Volumes.
[  OK  ] Reached target Local Encrypted Volumes.
[   20.274437] systemd[1]: Stopped target Switch Root.
[  OK  ] Stopped target Switch Root.
[   20.304417] systemd[1]: Stopped target Initrd File Systems.
[  OK  ] Stopped target Initrd File Systems.
[   20.344399] systemd[1]: Stopped target Initrd Root File System.
[  OK  ] Stopped target Initrd Root File System.
[   20.384457] systemd[1]: Reached target Remote File Systems.
[  OK  ] Reached target Remote File Systems.
[   20.424411] systemd[1]: Reached target Slices.
[  OK  ] Reached target Slices.
[   20.454430] systemd[1]: Reached target System Time Set.
[  OK  ] Reached target System Time Set.
[   20.494570] systemd[1]: Listening on Device-mapper event daemon FIFOs.
[  OK  ] Listening on Device-mapper event daemon FIFOs.
[   20.534636] systemd[1]: Listening on LVM2 poll daemon socket.
[  OK  ] Listening on LVM2 poll daemon socket.
[   20.574486] systemd[1]: Listening on initctl Compatibility Named Pipe.
[  OK  ] Listening on initctl Compatibility Named Pipe.
[   20.614672] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[   20.654577] systemd[1]: Listening on udev Kernel Socket.
[  OK  ] Listening on udev Kernel Socket.
[   20.696245] systemd[1]: Activating swap /dev/disk/by-uuid/9aff7a2c-5ce2-4deb-88a9-59cd58ebb210...
         Activating swap /dev/disk/…5ce2-4deb-88a9-59cd58ebb210...
[   20.736549] systemd[1]: Mounting Huge Pages File System...
         Mounting Huge Pages File System...
[   20.754394] Adding 511996k swap on /dev/nvme0n1p2.  Priority:-2 extents:1 across:511996k SSFS
[   20.776437] systemd[1]: Mounting POSIX Message Queue File System...
         Mounting POSIX Message Queue File System...
[   20.816514] systemd[1]: Mounting Kernel Debug File System...
         Mounting Kernel Debug File System...
[   20.856551] systemd[1]: Mounting Kernel Trace File System...
         Mounting Kernel Trace File System...
[   20.896787] systemd[1]: Starting Create list of static device nodes for the current kernel...
         Starting Create list of st…odes for the current kernel...
[   20.936258] systemd[1]: Starting Monitoring of LVM2 mirrors, snapshots etc. using dmeventd or progress polling...
         Starting Monitoring of LVM…meventd or progress polling...
[   20.976441] systemd[1]: Starting Load Kernel Module configfs...
         Starting Load Kernel Module configfs...
[   21.016412] systemd[1]: Starting Load Kernel Module drm...
         Starting Load Kernel Module drm...
[   21.056425] systemd[1]: Starting Load Kernel Module fuse...
         Starting Load Kernel Module fuse...
[   21.085607] fuse: init (API version 7.31)
[   21.095134] systemd[1]: Condition check resulted in Set Up Additional Binary Formats being skipped.
[   21.104672] systemd[1]: systemd-fsck-root.service: Succeeded.
[   21.111105] systemd[1]: Stopped File System Check on Root Device.
[  OK  ] Stopped File System Check on Root Device.
[   21.144580] systemd[1]: Stopped Journal Service.
[  OK  ] Stopped Journal Service.
[   21.177667] systemd[1]: Starting Journal Service...
         Starting Journal Service...
[   21.217016] systemd[1]: Starting Load Kernel Modules...
         Starting Load Kernel Modules...
[   21.256670] systemd[1]: Starting Remount Root and Kernel File Systems...
         Starting Remount Root and Kernel File Systems...
[   21.275455] BTRFS info (device nvme0n1p3): use lzo compression, level 0
[   21.282077] BTRFS info (device nvme0n1p3): disk space caching is enabled
[   21.296723] systemd[1]: Starting Coldplug All udev Devices...
         Starting Coldplug All udev Devices...
[   21.336872] systemd[1]: sysroot.mount: Succeeded.
[   21.347184] systemd[1]: Activated swap /dev/disk/by-uuid/9aff7a2c-5ce2-4deb-88a9-59cd58ebb210.
[  OK  ] Activated swap /dev/disk/b…c-5ce2-4deb-88a9-59cd58ebb210.
[   21.385008] systemd[1]: Mounted Huge Pages File System.
[  OK  ] Mounted Huge Pages File System.
[   21.424737] systemd[1]: Mounted POSIX Message Queue File System.
[  OK  ] Mounted POSIX Message Queue File System.
[   21.464784] systemd[1]: Mounted Kernel Debug File System.
[  OK  ] Mounted Kernel Debug File System.
[   21.504694] systemd[1]: Mounted Kernel Trace File System.
[  OK  ] Mounted Kernel Trace File System.
[   21.545311] systemd[1]: Finished Create list of static device nodes for the current kernel.
[  OK  ] Finished Create list of st… nodes for the current kernel.
[   21.585078] systemd[1]: modprobe@configfs.service: Succeeded.
[   21.591371] systemd[1]: Finished Load Kernel Module configfs.
[  OK  ] Finished Load Kernel Module configfs.
[   21.624736] systemd[1]: Started Journal Service.
[  OK  ] Started Journal Service.
[  OK  ] Finished Load Kernel Module drm.
[  OK  ] Finished Load Kernel Module fuse.
[  OK  ] Finished Load Kernel Modules.
[  OK  ] Finished Remount Root and Kernel File Systems.
[  OK  ] Reached target Swap.
         Mounting FUSE Control File System...
         Mounting Kernel Configuration File System...
         Starting Apply Kernel Variables...
         Starting Create Static Device Nodes in /dev...
[  OK  ] Finished Coldplug All udev Devices.
[  OK  ] Mounted FUSE Control File System.
[  OK  ] Mounted Kernel Configuration File System.
[  OK  ] Finished Create Static Device Nodes in /dev.
[  OK  ] Finished Apply Kernel Variables.
         Starting Wait for udev To …plete Device Initialization...
         Starting Rule-based Manage…for Device Events and Files...
[  OK  ] Started Rule-based Manager for Device Events and Files.
         Starting Load Kernel Module configfs...
[   22.682393] qoriq-cpufreq qoriq-cpufreq: Freescale QorIQ CPU frequency scaling driver
[  OK  ] Finished Load Kernel Module configfs.
[   22.716841] optee: probing for conduit method.
[   22.721299] optee: api uid mismatch
[   22.724841] optee: probe of firmware:optee failed with error -22
[   22.752165] i2c i2c-2: Added multiplexed i2c bus 3
[   22.757099] i2c i2c-2: Added multiplexed i2c bus 4
[   22.761898] pca954x 2-0070: registered 2 multiplexed busses for I2C mux pca9540
[   22.788572] tpm tpm0: starting up the TPM manually
[   22.840443] sp805-wdt c000000.wdt: registration successful
[   22.846878] fsl_dpaa2_eth dpni.9: Adding to iommu group 0
[   22.849146] sp805-wdt c010000.wdt: registration successful
[   22.858259] sp805-wdt c020000.wdt: registration successful
[   22.863997] sp805-wdt c030000.wdt: registration successful
[   22.869734] sp805-wdt c100000.wdt: registration successful
[   22.875451] sp805-wdt c110000.wdt: registration successful
[   22.881197] sp805-wdt c120000.wdt: registration successful
[   22.887029] sp805-wdt c130000.wdt: registration successful
[   22.892738] fsl_dpaa2_eth dpni.9: Probed interface eth0
[   22.898130] fsl_dpaa2_eth dpni.8: Adding to iommu group 0
[   22.935109] cryptd: max_cpu_qlen set to 1000
[   22.944047] fsl_dpaa2_eth dpni.8: Probed interface eth1
[   22.949473] fsl_dpaa2_eth dpni.7: Adding to iommu group 0
[   22.994973] fsl_dpaa2_eth dpni.7: Probed interface eth2
[   23.000347] fsl_dpaa2_eth dpni.6: Adding to iommu group 0
[   23.045775] fsl_dpaa2_eth dpni.6: Probed interface eth3
[   23.051104] fsl_dpaa2_eth dpni.5: Adding to iommu group 0
[  OK  ] Finished Monitoring of LVM… dmeventd or progress polling.
[   23.097468] fsl_dpaa2_eth dpni.5: Probed interface eth4
[   23.102781] fsl_dpaa2_eth dpni.4: Adding to iommu group 0
[  OK  ] Reached target Local File Systems (Pre).
[   23.148093] fsl_dpaa2_eth dpni.4: Probed interface eth5
[   23.153398] fsl_dpaa2_eth dpni.3: Adding to iommu group 0
         Mounting /.snapshots...
[   23.198757] fsl_dpaa2_eth dpni.3: Probed interface eth6
[   23.204071] fsl_dpaa2_eth dpni.2: Adding to iommu group 0
         Mounting /boot/efi...
         Mounting /boot/grub2/arm64-efi...
[   23.250551] fsl_dpaa2_eth dpni.2: Probed interface eth7
[   23.257314] fsl_dpaa2_eth dpni.1: Adding to iommu group 0
         Mounting /home...
[   23.302727] fsl_dpaa2_eth dpni.1: Probed interface eth8
[   23.308070] fsl_dpaa2_eth dpni.0: Adding to iommu group 0
         Mounting /opt...
         Mounting /root...
[   23.353407] fsl_dpaa2_eth dpni.0: Probed interface eth9
[   23.364914] fsl_dpaa2_eth dpni.8 rename3: renamed from eth1
         Mounting /srv...
         Mounting /tmp...
         Mounting /usr/local...
[   23.467445] fsl_dpaa2_eth dpni.2 rename9: renamed from eth7
         Mounting /var...
[  OK  ] Mounted /.snapshots.
[   23.545333] fsl_dpaa2_eth dpni.1 rename10: renamed from eth8
[  OK  ] Mounted /boot/efi.
[  OK  ] Mounted /boot/grub2/arm64-efi.
[  OK  ] Mounted /home.
[   23.624844] fsl_dpaa2_eth dpni.0 eth1: renamed from eth9
[  OK  ] Mounted /opt.
[  OK  ] Mounted /root.
[  OK  ] Mounted /srv.
[   23.715089] fsl_dpaa2_eth dpni.9 eth7: renamed from eth0
[  OK  ] Mounted /tmp.
[  OK  ] Mounted /usr/local.
[   23.784604] fsl_dpaa2_eth dpni.2 eth8: renamed from rename9
[  OK  ] Mounted /var.
[   23.854663] fsl_dpaa2_eth dpni.8 eth9: renamed from rename3
[  OK  ] Reached target Local File Systems.
[   23.914665] fsl_dpaa2_eth dpni.1 eth0: renamed from rename10
         Starting Restore /run/initramfs on shutdown...
         Starting Flush Journal to Persistent Storage...
[   23.976403] systemd-journald[1146]: Received client request to flush runtime journal.
[   23.987454] systemd-journald[1146]: File /var/log/journal/826c9d7a747c44dc9bf29e8c9c7129a0/system.journal corrupted or uncleanly shut down, renaming and replacing.
         Starting Load/Save Random Seed...
[  OK  ] Finished Restore /run/initramfs on shutdown.
[  OK  ] Finished Load/Save Random Seed.
[  OK  ] Finished Wait for udev To Complete Device Initialization.
[  OK  ] Finished Flush Journal to Persistent Storage.
         Starting Create Volatile Files and Directories...
[  OK  ] Finished Create Volatile Files and Directories.
         Starting Update UTMP about System Boot/Shutdown...
[  OK  ] Finished Update UTMP about System Boot/Shutdown.
[  OK  ] Reached target System Initialization.
[  OK  ] Started Watch /etc/sysconfig/btrfsmaintenance.
[  OK  ] Started Watch for changes in CA certificates.
[  OK  ] Started Watch for changes in issue snippets.
[  OK  ] Started Daily Cleanup of Snapper Snapshots.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Reached target Paths.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Reached target Sockets.
[  OK  ] Reached target Basic System.
[  OK  ] Started D-Bus System Message Bus.
[  OK  ] Started Detect if the system suffers from bsc#1089761.
[  OK  ] Started irqbalance daemon.
         Starting Generate issue file for login session...
         Starting Apply settings from /etc/sysconfig/keyboard...
         Starting User Login Management...
         Starting wicked AutoIPv4 supplicant service...
         Starting wicked DHCPv4 supplicant service...
         Starting wicked DHCPv6 supplicant service...
[  OK  ] Started wicked AutoIPv4 supplicant service.
[  OK  ] Started wicked DHCPv4 supplicant service.
[  OK  ] Started wicked DHCPv6 supplicant service.
[  OK  ] Finished Generate issue file for login session.
[  OK  ] Finished Apply settings from /etc/sysconfig/keyboard.
         Starting wicked network management service daemon...
[  OK  ] Listening on Load/Save RF …itch Status /dev/rfkill Watch.
[  OK  ] Started User Login Management.
[  OK  ] Started wicked network management service daemon.
         Starting wicked network nanny service...
[  OK  ] Started wicked network nanny service.
         Starting wicked managed network interfaces...
[   25.823623] No iBFT detected.
[  OK  ] Finished wicked managed network interfaces.
[  OK  ] Reached target Network.
         Starting NTP client/server...
         Starting OpenSSH Daemon...
         Starting Permit User Sessions...
[  OK  ] Finished Permit User Sessions.
[  OK  ] Started Getty on tty1.
[  OK  ] Started Serial Getty on ttyS0.
[  OK  ] Reached target Login Prompts.
[  OK  ] Started OpenSSH Daemon.
[  OK  ] Started NTP client/server.
[  OK  ] Reached target Multi-User System.
[  OK  ] Reached target Graphical Interface.
[  OK  ] Reached target System Time Synchronized.
[  OK  ] Started Backup of RPM database.
[  OK  ] Started Backup of /etc/sysconfig.
[  OK  ] Started Balance block groups on a btrfs filesystem.
[  OK  ] Started Defragment file data and/or directory metadata.
[  OK  ] Started Scrub btrfs filesystem, verify block checksums.
[  OK  ] Started Discard unused blocks on a mounted filesystem.
[  OK  ] Started Check if mainboard battery is Ok.
[  OK  ] Started Discard unused blocks once a week.
[  OK  ] Started Daily rotation of log files.
[  OK  ] Started Timeline of Snapper Snapshots.
[  OK  ] Reached target Timers.
         Starting Update UTMP about System Runlevel Changes...
[  OK  ] Finished Update UTMP about System Runlevel Changes.

Welcome to openSUSE Leap 15.3 - Kernel 5.3.18-57-default (ttyS0).

eth0:
eth1:
eth2:
eth3:
eth4:
eth5:
eth6:
eth7:
eth8:
eth9:


localhost login:
```