# Architecture / Block Diagram

Note: This depicts the default Ten64 retail/developer board configuration. Traverse offers customized SKUs for volume customers, which may differ
from the system depicted below.

![Ten64 block diagram](/images/ten64-architecture.png)
