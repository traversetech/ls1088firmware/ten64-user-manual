# Hardware Changes and Errata

### Errata
| Affected board revisions | Issue                                                                                              | Impact                                                  | Workarounds                                                 | Proposed Resolution                                                                                                                |
|--------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------|-------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| Ten64 Rev B,C      | RTC (Epson RX-8035) interrupt is not functional due to inversion/missing pullup                    | Low (no effect found on any Linux distribution to date) | Ensure there is no IRQ reference for the RTC in device tree | This will be fixed in a future Ten64 board respin.                                                                                 |
| Ten64 Rev B,C            | It may not be possible to install an ATX (UNC) mounting screw for the mounting hole next to the fan connector | Low                                                     | Use a smaller mounting screw (e.g M3) or do not install     | A future Ten64 revision may reposition the fan connector, the current position was a compromise due to layout requirements |
| All Ten64 boards built prior to 2022 | Design error with tamper detect circuit prevents NXP verified/secure boot from working | Low (secure boot requires permanent changes to the board, e.g eFuses, so is not a "out of the box feature") | Contact Traverse to exchange for a modified board, or modify board with instructions below | All Ten64 boards built 2022 onwards will have the fix for this issue |

#### Secure Boot fix
A design error with the Trust Architecture tamper input prevents Trust Architecture secure boot from working. To fix this issue, remove R264 (0 ohm 0402) and install R265 (10K ohm 0402). These two resistor positions are on the underside of the board near the DDR4 tracks.

See [this document](/hardware/schematics/ten64-secure-boot-fix.pdf) for details of the modification.

## Changes between Ten64 Rev C (developer / retail version) and Ten64 Rev D (5G optimized / OEM low cost)

* Removed USB hub and PCIe switch due to supply and cost issues
    * Reduced to 1 external USB port (no internal USB port or USB port to slots other than M.2/B)
    * 2 x MiniPCIe slots replaced with 1 x M.2 Key E with PCIe and 4 wire UART (for Bluetooth)
* Moved M.2 Key B slot to better position to improve radio performance / reduce interference effects.
* Added "graceful shutdown" feature (backed by large capacitors) for the Telit FN980 cards.
* Added mode select jumper (PCIe or USB) for cards that use M.2 pin 20 (Sierra EM9191, FN980 rev 2.0)

## Ten64 Rev C2
* This revision can ship with a EMC2302 (two channel) fan controller in order to mitigate part shortages.
  There is still only one fan connector on the board.
* Note: Only firmware builds after 2022-09-08 are supported on these boards (specifically Firmware Builder commit [26614bfd](26614bfd7377ada1190e09c3b9311b655d3cd2ab))

## From Ten64 Rev B to Ten64 Rev C (first production version)
* Incorporation of 'wire mod' rework fixes
* Improve clearances around mounting holes and power connector
* De-feature I2C4 access to M.2 cards due to this not being adhered to by some Key B modems. It can restored by adding the missing parts.
* Add I2C control to USB Hub, to allow the USB3 lanes to be turned off when a PCIe only card (e.g SSD) is installed in Key B (fixes "cannot connect" warnings in the kernel)
* Adjusted heights of M.2 slots to better facilitate use of 42mm cards inside the 52mm/80mm footprints.
* The microcontroller (LPC804) was redeveloped between RevB and RevC to fix I2C interoperability issues with U-Boot

## From Ten64 Rev A to Ten64 Rev B
* Incorporation of 'wire mod' rework fixes
* Re-allocate one PCIe 3.0 lane from SoC from MiniPCIe WiFi to M.2/B
* Tweak layout so mounting hole for 5G cards can be on the board.
* Add mounting hole at 42mm for M.2/M
* SIM only position on SIM tray is now allocated to SIM1 on M.2/B, SIM2 is shared with microSD or eSIM (build time option).
* The microSD pins on the board are now electrically isolated when the tray is in the eject position.
* Add 4-pin fan header for CPU fan + EMC2301 PWM controller.
* The SPI-NAND flash size was increased to 256MB (2Gbit).
* DDR SPD and RTC moved to I2C3 bus, TPM moved to I2C1. (prevents interference by other I2C1 devices in boot + allows EFI RTC protocol implementation in the future)
* Added a 'power button' line from the microcontroller to LS1088 which can be used for ACPI-style power button events.
* The single 'admin' button on the rear panel was replaced with a stacked dual-LED/button combination to better accommodate enclosures without ATX front panel LEDs/buttons.
* SFP cage was reduced from four LEDs to two LEDs (one for each slot).
