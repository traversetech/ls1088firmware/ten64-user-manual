# Cryptographic acceleration on the LS1088

There are two cryptographic accelerators on the LS1088:

* The QorIQ ``SEC`` engine (also known as CAAM)

    This supports many operations used for networking/communications use cases such
   as IPSec and 802.11.

    Interaction is via the drivers in the Linux kernel (``drivers/crypto/caam``) as well
   as some networking toolkits such as [DPDK](https://doc.dpdk.org/guides/cryptodevs/dpaa2_sec.html).

* The ARMv8 Cryptographic extensions

    Similar in concept to AES-NI on x86, including functions for AES and SHA-1,224 and 256.

    Not as expansive as the SEC engine but easier to use from userspace applications.

## Driver support
### SEC engine
Support for the DPAA2 accelerator engine was added to the mainline Linux kernel in v5.0 - while the SEC engine has been supported for a long
time (across QorIQ PPC and DPAA1-LS) the access method for DPAA2 family SoC's (DPSECI) is different.

If you need the accelerator driver for an earlier kernel release, you might want to use NXP's LSDK kernel tree (for 4.14 and 4.19).

At the minimum you will need to enable:
```
CONFIG_CRYPTO_DEV_FSL_CAAM_COMMON=y
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API_DESC=y
CONFIG_CRYPTO_DEV_FSL_CAAM_AHASH_API_DESC=y
CONFIG_CRYPTO_DEV_FSL_CAAM=m
# CONFIG_CRYPTO_DEV_FSL_CAAM_DEBUG is not set
CONFIG_CRYPTO_DEV_FSL_CAAM_JR=m
CONFIG_CRYPTO_DEV_FSL_CAAM_RINGSIZE=9
# CONFIG_CRYPTO_DEV_FSL_CAAM_INTC is not set
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API_QI=y
CONFIG_CRYPTO_DEV_FSL_CAAM_AHASH_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_PKC_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_RNG_API=y
CONFIG_CRYPTO_DEV_FSL_DPAA2_CAAM=y
```

You can find this in the kernel menuconfig under ``Cryptographic API->Hardware Crypto devices->`` Freescale CAAM and QorIQ DPAA2 CAAM.

### ARM Cryptographic extensions
In the kernel:
```
CONFIG_CRYPTO_SHA1_ARM64_CE=y
CONFIG_CRYPTO_SHA2_ARM64_CE=y
CONFIG_CRYPTO_SHA512_ARM64_CE=y
CONFIG_CRYPTO_SM3_ARM64_CE=y
CONFIG_CRYPTO_SM4_ARM64_CE=y
CONFIG_CRYPTO_GHASH_ARM64_CE=y
CONFIG_CRYPTO_CRCT10DIF_ARM64_CE=m
CONFIG_CRYPTO_AES_ARM64_CE=y
CONFIG_CRYPTO_AES_ARM64_CE_CCM=y
CONFIG_CRYPTO_AES_ARM64_CE_BLK=y
```

In menuconfig, this is at the top level ``ARM64 Accelerated Cryptographic Algorithms``

### List of available operations
These operations are available as of v5.5:
#### SEC/CAAM
```
cat /proc/crypto  | grep caam | grep driver
driver       : rsa-caam
driver       : cmac-aes-caam
driver       : xcbc-aes-caam
driver       : md5-caam
driver       : hmac-md5-caam
driver       : sha512-caam
driver       : hmac-sha512-caam
driver       : sha384-caam
driver       : hmac-sha384-caam
driver       : sha256-caam
driver       : hmac-sha256-caam
driver       : sha224-caam
driver       : hmac-sha224-caam
driver       : sha1-caam
driver       : hmac-sha1-caam
driver       : seqiv-authenc-hmac-sha512-rfc3686-ctr-aes-caam
driver       : authenc-hmac-sha512-rfc3686-ctr-aes-caam
driver       : seqiv-authenc-hmac-sha384-rfc3686-ctr-aes-caam
driver       : authenc-hmac-sha384-rfc3686-ctr-aes-caam
driver       : seqiv-authenc-hmac-sha256-rfc3686-ctr-aes-caam
driver       : authenc-hmac-sha256-rfc3686-ctr-aes-caam
driver       : seqiv-authenc-hmac-sha224-rfc3686-ctr-aes-caam
driver       : authenc-hmac-sha224-rfc3686-ctr-aes-caam
driver       : seqiv-authenc-hmac-sha1-rfc3686-ctr-aes-caam
driver       : authenc-hmac-sha1-rfc3686-ctr-aes-caam
driver       : seqiv-authenc-hmac-md5-rfc3686-ctr-aes-caam
driver       : authenc-hmac-md5-rfc3686-ctr-aes-caam
driver       : echainiv-authenc-hmac-sha512-cbc-des-caam
driver       : authenc-hmac-sha512-cbc-des-caam
driver       : echainiv-authenc-hmac-sha384-cbc-des-caam
driver       : authenc-hmac-sha384-cbc-des-caam
driver       : echainiv-authenc-hmac-sha256-cbc-des-caam
driver       : authenc-hmac-sha256-cbc-des-caam
driver       : echainiv-authenc-hmac-sha224-cbc-des-caam
driver       : authenc-hmac-sha224-cbc-des-caam
driver       : echainiv-authenc-hmac-sha1-cbc-des-caam
driver       : authenc-hmac-sha1-cbc-des-caam
driver       : echainiv-authenc-hmac-md5-cbc-des-caam
driver       : authenc-hmac-md5-cbc-des-caam
driver       : echainiv-authenc-hmac-sha512-cbc-des3_ede-caam
driver       : authenc-hmac-sha512-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-sha384-cbc-des3_ede-caam
driver       : authenc-hmac-sha384-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-sha256-cbc-des3_ede-caam
driver       : authenc-hmac-sha256-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-sha224-cbc-des3_ede-caam
driver       : authenc-hmac-sha224-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-sha1-cbc-des3_ede-caam
driver       : authenc-hmac-sha1-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-md5-cbc-des3_ede-caam
driver       : authenc-hmac-md5-cbc-des3_ede-caam
driver       : echainiv-authenc-hmac-sha512-cbc-aes-caam
driver       : authenc-hmac-sha512-cbc-aes-caam
driver       : echainiv-authenc-hmac-sha384-cbc-aes-caam
driver       : authenc-hmac-sha384-cbc-aes-caam
driver       : echainiv-authenc-hmac-sha256-cbc-aes-caam
driver       : authenc-hmac-sha256-cbc-aes-caam
driver       : echainiv-authenc-hmac-sha224-cbc-aes-caam
driver       : authenc-hmac-sha224-cbc-aes-caam
driver       : echainiv-authenc-hmac-sha1-cbc-aes-caam
driver       : authenc-hmac-sha1-cbc-aes-caam
driver       : echainiv-authenc-hmac-md5-cbc-aes-caam
driver       : authenc-hmac-md5-cbc-aes-caam
driver       : authenc-hmac-sha512-ecb-cipher_null-caam
driver       : authenc-hmac-sha384-ecb-cipher_null-caam
driver       : authenc-hmac-sha256-ecb-cipher_null-caam
driver       : authenc-hmac-sha224-ecb-cipher_null-caam
driver       : authenc-hmac-sha1-ecb-cipher_null-caam
driver       : authenc-hmac-md5-ecb-cipher_null-caam
driver       : gcm-aes-caam
driver       : rfc4543-gcm-aes-caam
driver       : rfc4106-gcm-aes-caam
driver       : ecb-des3-caam
driver       : ecb-aes-caam
driver       : ecb-des-caam
driver       : xts-aes-caam
driver       : rfc3686-ctr-aes-caam
driver       : ctr-aes-caam
driver       : cbc-des-caam
driver       : cbc-3des-caam
driver       : cbc-aes-caam
```
#### ARM Cryptographic extensions
```
cat /proc/crypto  | grep driver | grep ce
driver       : crct10dif-arm64-ce
driver       : essiv-cbc-aes-sha256-ce
driver       : cts-cbc-aes-ce
driver       : xts-aes-ce
driver       : ctr-aes-ce
driver       : cbc-aes-ce
driver       : ecb-aes-ce
driver       : cbcmac-aes-ce
driver       : xcbc-aes-ce
driver       : cmac-aes-ce
driver       : __essiv-cbc-aes-sha256-ce
driver       : __cts-cbc-aes-ce
driver       : __xts-aes-ce
driver       : ctr-aes-ce
driver       : __ctr-aes-ce
driver       : __cbc-aes-ce
driver       : __ecb-aes-ce
driver       : ccm-aes-ce
driver       : aes-ce
driver       : gcm-aes-ce
driver       : ghash-ce
driver       : sha256-ce
driver       : sha224-ce
driver       : sha1-ce
```

## Further reading
* [Layerscape SDK User Guide](https://www.nxp.com/docs/en/user-guide/LSDKUG_Rev19.09.pdf), section 7.2.14 "Security Engine (SEC)"
* [Presentation: Overview of QorIQ Security Features](https://community.nxp.com/docs/DOC-104265) - older, but a good overview of the SEC engine. The LS1088 is a SEC 5.0 family device.
* [ARM Cortex-A53 Cryptography Extension Technical Reference Manual](https://developer.arm.com/docs/ddi0501/latest/introduction/about-the-cortex-a53-processor-cryptography-extension)
