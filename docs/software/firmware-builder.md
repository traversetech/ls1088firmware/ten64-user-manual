---
layout: page
title: Firmware Builder
permalink: /software/firmware-builder/
---

[Firmware Builder](https://gitlab.com/traversetech/ls1088firmware/firmware-builder.git) is how firmware binaries
for the Ten64 board are built - this includes:

- Reset Configuration Word
- ARM Trusted Firmware (ATF)
- U-Boot
- DPAA2 configuration
- Device Tree (DTB)
- SD card for [reflash](/software/reflash/)

Some of the steps above have one or more previous stages as inputs, ATF includes the RCW and U-Boot, for example.

We ship firmware built on public CI instances to ensure integrity and traceability of firmware.

You can download firmware artifacts from our archive server - [https://archive.traverse.com.au/pub/traverse/ls1088firmware/firmware-builds/latest/](https://archive.traverse.com.au/pub/traverse/ls1088firmware/firmware-builds/latest/).
