# Using the hardware watchdog

The LS1088 integrates several ARM SP805 hardware watchdog's, which can be used to
restart the system automatically in cases where the kernel is no longer servicing the
watchdog (e.g under kernel panic).

_NOTE_ Other Layerscape family members (such as the LS1043) used the iMX watchdog hardware block (imx2-wdt). You should ensure your kernel is configured correctly.

## OpenWrt based systems

OpenWrt will enable the watchdog by default. You can test that it works by issuing the following commands:

```
root@OpenWrt:/# ubus call system watchdog '{"stop": true}'
{
        "status": "stopped",
        "timeout": 30,
        "frequency": 5,
        "magicclose": false
}

```

See this [article](http://kernelreloaded.com/manually-controlling-openwrt-hardware-watchdog/) from kernelreloaded.com for more information.

## systemd based systems

Ensure you have ```RuntimeWatchdogSec=X``` set in ```/etc/systemd/system.conf```
- by default it is disabled.

You can check to see if it is enabled by looking at the system log:
```
# journalctl -xn 500 | grep watchdog
Oct 23 04:56:58 ten64-debian systemd[1]: Hardware watchdog 'sp805-wdt', version 0
Oct 23 04:56:58 ten64-debian systemd[1]: Set hardware watchdog to 10s.
```
and/or seeing if systemd has ```/dev/watchdog``` open:
```
root@ten64-debian:~# lsof | grep watchdog
systemd      1                            root    4w      CHR             10,130      0t0        167 /dev/watchdog
```

A crude way to check if the watchdog works is to cause a kernel panic:
```
root@ten64-debian:~# echo c > /proc/sysrq-trigger
[  185.412985] sysrq: SysRq : Trigger a crash

...

[  185.675732] Memory Limit: none
[  185.678784] ---[ end Kernel panic - not syncing: Fatal exception ]---

(some time later - the watchdog resets the system)
INFO:    RCW BOOT SRC is QSPI
INFO:    RCW BOOT SRC is QSPI
..
U-Boot 2019.10-00681-g8aade676ba (Oct 18 2019 - 02:53:58 +0000)
```
