
# Macros using the mkdocs-macros-plugin
def define_env(env):
  "Hook function"

  @env.macro
  def checkmark():
      return "<span style=\"font-size:x-large;\">&#x2705;</span><br/>YES"

  @env.macro
  def crossmark():
      return "<span style=\"font-size:x-large;\">&#10060;</span><br/>NO"